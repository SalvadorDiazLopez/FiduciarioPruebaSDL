package IfiduContabilidad;

import fiduEstrucContables.DatosFiso;
import fiduEstrucContables.DatosMovimiento;
import fiduEstrucContables.DetMovimiento;
import fiduEstrucContables.SubFiso;
import fiduEstrucGlobales.Cliente;

/*
VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsContabilizador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'*************************************************************'
'*           Derechos Reservados EFISOFT S.A. 2002           *'
'*************************************************************'
'*  ARCHIVO     : IfiduContabilidad.vbp                      *'
'*  CLASES      : clsContabilizador.cls                      *'
'*                                                           *'
'*  DESCRIPCION : Interfaz de Servicios para el Modulo       *'
'*                de CONTABILIDAD.                           *'
'*                                                           *'
'*  AUTOR       : OAP - Septiembre 2002                      *'
'*************************************************************'
Option Explicit
*/
public interface IContabilizador {

	//'AcumulaSaldos
	public boolean AcumulaSaldos(GuiaDet vGuiaDet, boolean bMesAnt, String sMsgError) ;

	//'EscalaSaldosAbono
	public boolean EscalaSaldosAbono(GuiaDet vGuiaDet, boolean bMesAnt, String sMsgError) ;
	
	//'EscalaSaldosCargo
	public boolean EscalaSaldosCargo(GuiaDet vGuiaDet, boolean bMesAnt, String sMsgError) ;
	
	//'GenAplicaOperacionSrv
	public boolean GenAplicaOperacionSrv(DatosFiso vFiso, SubFiso vSFiso, DetMovimiento vDetMov, DatosMovimiento vDatosMov[], 
			long lNumFolioCont, Cliente vCliente, String sMsgServ, String sMsgError, /*Optional*/ boolean bConSuger) ;
	
	//'GeneraDetMov
	public boolean GeneraDetMov(DetMovimiento vDetMov, DatosMovimiento vDatosMov[], String sMsgError) ;
	
	//'GrabaMovimiento
	public boolean GrabaMovimiento(DatosFiso vDatosFiso, SubFiso vSubFiso, DetMovimiento vDetMov, DatosMovimiento vDatosMov[], 
			Transaccion vTransacc[], Cliente vCliente, int iNumTransacc, boolean _bMesAnt, String sMsgError) ; 
	
	//'InsertaAsiento
	public boolean InsertaAsiento(Transaccion vTransacc[], DetMovimiento vDetMov, GuiaDet vGuiaDet, int iNumTran, 
	                                boolean bMesAnt, String sMsgError) ;

	//'SolicitaFolioPoliza
	public boolean SolicitaFolioPoliza(DatosFiso vDatosFiso, DetMovimiento vDetMov, String sMsgError) ;
	
	//'GeneraEjes
	public void GeneraEjes(long lNumFisoEje, boolean bMesAnt, String sMsgError);
	
}
