package IfiduContabilidad;

import java.text.SimpleDateFormat;
import java.util.Date;

import fiduEstrucContables.DatosFiso;
import fiduEstrucContables.DatosMovimiento;
import fiduEstrucContables.DetMovimiento;
import fiduEstrucContables.SubFiso;
import fiduEstrucGlobales.Cliente;
/*
VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Contabilizador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'*************************************************************'
'*           Derechos Reservados EFISOFT S.A. 2002           *'
'*************************************************************'
'*  ARCHIVO     : fiduContabilidad.vbp                       *'
'*  CLASES      : Contabilizador.cls                         *'
'*                                                           *'
'*  DESCRIPCION : Implementacion de Servicios para el Modulo *'
'*                de CONTABILIDAD.                           *'
'*                                                           *'
'*  AUTOR       : OAP - Octubre 2002                         *'
'*************************************************************'
Option Explicit
*/
public class Contabilizador implements IContabilizador {

	private final String m_sObjectName = "fiduContabilidad" ;
	private final int m_iCONTABILIZAR = 2 ;
	private final int m_iSUGERIR = 3 ;
	private final int m_iINVERSIONES = 2 ;
	fiduConstContab.Constantes oConsConta = new fiduConstContab.Constantes() ;
	private long lngCuentaConBanco ;                      //'*PAP-CAREJ01/550-0001
	private long lngCuentaSinBanco ;                      //'*PAP-CAREJ01/550-0002
	private final int cntTres = 3/*&H3*/ ;                     //'*PAP-CAREJ01/550-0003
	private final int cntCero = 0/*&H0*/ ;                     //'*PAP-CAREJ01/550-0004
	private final int cntMenosUno = -1 ;                  //'*PAP-CAREJ01/550-0005
	
	@Override
	public boolean AcumulaSaldos(GuiaDet vGuiaDet, boolean bMesAnt, String sMsgError) {
		// TODO Auto-generated method stub
		
		boolean resultado = false ;
		
		GuiaDet oGuiaDet = new GuiaDet() ;
		
		try {
			
			oGuiaDet = vGuiaDet ;
			//oGuiaDet. -> son acceso a get y set que aun no estan hechos.
		    if (oGuiaDet.iNumCta < cntCero) {                                  //'*PAP-CAREJ01/550-0006
		        oGuiaDet.iNumCta = oGuiaDet.iNumCta * cntMenosUno; 			   //'*PAP-CAREJ01/550-0007
		        lngCuentaConBanco = cntTres + oGuiaDet.iNumCta;              //'*PAP-CAREJ01/550-0008
		    }
		    else {                                                                //'*PAP-CAREJ01/550-0009
		        lngCuentaConBanco = oGuiaDet.iNumCta ;                        //'*PAP-CAREJ01/550-0010
		    }
		    
		    if (oGuiaDet.sTipoAsiento == "A" ) {
	            if ((oGuiaDet.lAux2 = 0) && (oGuiaDet.sAux3 == 0)) {//       '* DSA-FII/454-00015
	                if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) {
	                	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
	                }
	            }                                                  //'* DSA-FII/454-00016
		    
	            if ((oGuiaDet.sAux3 > 0) && (oGuiaDet.lAux2 == 0)){       //'* DSA-FII/454-00017
	            	//' oGuiaDet.sAux3 = 0                                 '* DSA-FII/454-00018
	            	if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) {
	            		if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
	            	}
	            	oGuiaDet.sAux3 = 0 ;                                //'* DSA-FII/454-00019
	            } 
	            
	            if ((oGuiaDet.lAux2 > 0) && (oGuiaDet.sAux3 == 0)){       //'* DSA-FII/454-00021
                    //'oGuiaDet.lAux2 = 0                                 //'* DSA-FII/454-00022
                    if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) {
                    	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
                    }
	            }
	            
	            if ((oGuiaDet.sAux3 > 0) && (oGuiaDet.lAux2 > 0)) {       //'* DSA-FII/454-00023
		            //'oGuiaDet.sAux3 = 0                                 '* DSA-FII/454-00024
		            if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) { //'* DSA-FII/454-00025
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;  
		            }
		            oGuiaDet.sAux3 = 0 ;                                 //'* DSA-FII/454-00026
	            }
	            
	            
	            if (oGuiaDet.iNum4SubCta > 0) {
		            oGuiaDet.iNum4SubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
	            }
	            
	            if (oGuiaDet.iNum3SubCta > 0) {
		            oGuiaDet.iNum3SubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
	            }
	            
	            if (oGuiaDet.iNum2SubCta > 0) {
		            oGuiaDet.iNum2SubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
	            }
	            
	            if (oGuiaDet.iNumSubCta > 0) {
		            oGuiaDet.iNumSubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosAbono(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
	            }
		    }
		    else {
		    	
		    	if ((oGuiaDet.sAux3 = 0) && (oGuiaDet.lAux2 == 0)) {               //'* DSA-FII/454-00027
	                if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
	                	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
	                }
		    	} 
		    	
		    	if ((oGuiaDet.sAux3 > 0) && (oGuiaDet.lAux2 == 0)) {               //'* DSA-FII/454-00029
	                //'oGuiaDet.sAux3 = 0                                          //'* DSA-FII/454-00030
	                if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
	                	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
	                }
	                oGuiaDet.sAux3 = 0;                                          //'* DSA-FII/454-00031
		    	}
		    	
		    	if ((oGuiaDet.lAux2 > 0) && (oGuiaDet.sAux3 == 0)) {               //'* DSA-FII/454-00032
	                //'oGuiaDet.lAux2 = 0                                         '* DSA-FII/454-00033
	                if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
	                	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
	                }
		    	}
		    	
		    	if ((oGuiaDet.lAux2 > 0) && (oGuiaDet.sAux3 > 0)) {               //'* DSA-FII/454-00034
		            //'oGuiaDet.lAux3 = 0                                         '* DSA-FII/454-00035
		            if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
		            oGuiaDet.sAux3 = 0 ;                                          //'* DSA-FII/454-00037
		    	}
		    	
		    	
		    	if (oGuiaDet.iNum4SubCta > 0) {
		            oGuiaDet.iNum4SubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
		    	}
		    	
		    	if (oGuiaDet.iNum3SubCta > 0) {
		            oGuiaDet.iNum3SubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
		    	}
		    	
		    	if (oGuiaDet.iNum2SubCta > 0) {
		            oGuiaDet.iNum2SubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
		    	}
		    	
		    	if (oGuiaDet.iNumSubCta > 0) {
		            oGuiaDet.iNumSubCta = 0 ;
		            if (!clsContabilizador_EscalaSaldosCargo(oGuiaDet, bMesAnt, sMsgError)) {
		            	if (oGuiaDet != null) { oGuiaDet = null ; }
	        			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
	        			return resultado ;
		            }
		    	}
		    }
		    
		    resultado = true ;
    	    if (oGuiaDet != null) { oGuiaDet = null ; }
		    
		}catch(Exception ex) {
			
			if (oGuiaDet != null) { oGuiaDet = null ; }
			sMsgError += "\n Fuente : " + m_sObjectName + ".AcumulaSaldos" ;
			return resultado ;
		}
		
		return resultado ;
	}

	@Override
	public boolean EscalaSaldosAbono(GuiaDet vGuiaDet, boolean bMesAnt, String sMsgError) {
		// TODO Auto-generated method stub
		
		boolean resultado = false ;
		
		IfiduBaseDatos.IBaseDatos oAccesoBD ;
		String sSql ;
		ADODB.Recordset rsSaldos = new ADODB.Recordset() ;
		String sMesAnt ;
	    boolean bIndicador ;
	    boolean bCorrecto ;
	    
	    long  PA_SALD_AX2 ;               //'* DSA-FII/454-00039
	    double PA_SALD_AX3 ;             //'* DSA-FII/454-00040
		
	    try {
	    	
	    	if (sMsgError.equals("Mes13")) {
		      sMesAnt = "H" ;
		      sMsgError = "" ;
	    	}
		    else {
		      if (bMesAnt) { sMesAnt = "MSA" ; }
		      
		      else { sMesAnt = "" ; }
		    }
	    	
	    	bIndicador = true ;
	    	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	    	while(bIndicador) {
	    		sSql = " SELECT CCON_CTA" + " FROM   FDSALDOS" + sMesAnt ;
		        //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta)      '*PAP-CAREJ01/550-0012
		        sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;     //'*PAP-CAREJ01/550-0013
		        sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;
		        sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ; //.iNumSubCta son gets del objeto vGuiaDet 
		        sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ; //que aun no esta hecho.
		        sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;
		        sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;
		        sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;
		        sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;
		        sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;
		        
		        if (sMesAnt.equals("H")) {
	                sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ; //tenemos que recuperar el a�o del get
	                sSql = sSql + " AND  SALD_MES = 13 " ;
		        }
		        
		        oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
                //		DoEvents
                rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
                //		DoEvents
                if (sMsgError.length() > 0) {
        		    if (oAccesoBD != null) { oAccesoBD = null ; }
        		    if (rsSaldos != null) { rsSaldos = null ; }
        		    sMsgError = sMsgError + "\n" + 
        		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                    return resultado  ;
                }
                if (oAccesoBD != null) { oAccesoBD = null ; }
		        
                if (rsSaldos.EOF) {
	                sSql = " INSERT INTO FDSALDOS" + sMesAnt ;
			        sSql = sSql + " VALUES(" ;
			        //'sSql = sSql + vGuiaDet.iNumCta + "," ;       '*PAP-CAREJ01/550-0014
			        sSql = sSql + lngCuentaConBanco + "," ;      //'*PAP-CAREJ01/550-0015
			        sSql = sSql + vGuiaDet.iNumSubCta + "," ;
			        sSql = sSql + vGuiaDet.iNum2SubCta + "," ;
			        sSql = sSql + vGuiaDet.iNum3SubCta + "," ;
			        sSql = sSql + vGuiaDet.iNum4SubCta + "," ;
			        sSql = sSql + vGuiaDet.iNumMoneda + "," ;
			        sSql = sSql + vGuiaDet.lAux1 + "," ;
			        sSql = sSql + vGuiaDet.lAux2 + "," ;
			        sSql = sSql + vGuiaDet.sAux3 + "," + "0," + "0," ;
			        sSql = sSql + vGuiaDet.dValor) + "," ;
			        
			        if (sMesAnt != "H") {
		              sSql = sSql + "0," + "0," + vGuiaDet.dValor + "," ;
			        }

			        sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + "," ;
	                sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ;
	                sSql = sSql + "0," + "1," ;
	                if (sMesAnt != "H") {
	                  sSql = sSql + "0," ;
	                  sSql = sSql + "1)" ;
	                }
	                else {
	                  sSql = sSql + Year(vGuiaDet.dtFechaMov) + "," ; //tenemos que recuperar el a�o del get
	                  sSql = sSql + "13)" ;
	                }
			       
                }
                else {
                	
                	sSql = " UPDATE FDSALDOS" + sMesAnt ;
					sSql = sSql + " SET    SALD_ABONOS_MES   = SALD_ABONOS_MES + " + vGuiaDet.dValor + "," ;
					sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + "," ;
					sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ;
					sSql = sSql + "        SALD_NABONOS_MES  = SALD_NABONOS_MES + 1" ;
                	
					if (sMesAnt != "H") {
		              sSql = sSql + "        , SALD_ABONOS_EJER  = SALD_ABONOS_EJER + " + vGuiaDet.dValor + "," ;
		              sSql = sSql + "        SALD_NABONOS_EJER = SALD_NABONOS_EJER + 1" ;
					}
					
					//'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta)       '*PAP-CAREJ01/550-0016
		            sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;      //'*PAP-CAREJ01/550-0017
		            sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;
		            sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;
		            sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;
		            sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;
		            sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;
		            sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;
		            sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;
		            sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;
					
		            if (sMesAnt == "H") {
						sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;//tenemos que recuperar el a�o del get
						sSql = sSql + " AND  SALD_MES = 13 " ;
		            }
                }
                
                oAccesoBD =new IfiduBaseDatos.BaseDatos() ;
                //		DoEvents
                bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
                //		DoEvents
                if (oAccesoBD != null) { oAccesoBD = null ; }
                if (!bCorrecto) {
        		    if (oAccesoBD != null) { oAccesoBD = null ; }
        		    if (rsSaldos != null) { rsSaldos = null ; }
        		    sMsgError = sMsgError + "\n" + 
        		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                    return resultado ;
                }
                
                if (vGuiaDet.lAux2 > 0) { //'* DSA-FII/454-00042
	                PA_SALD_AX2 = vGuiaDet.lAux2 ;//                                        '* DSA-FII/454-00043
	                vGuiaDet.lAux2 = 0 ;//                                                  '* DSA-FII/454-00044
	                sSql = " SELECT CCON_CTA" ;                                           //'* DSA-FII/454-00045
	                sSql = sSql + " FROM   FDSALDOS" + sMesAnt ;                          //'* DSA-FII/454-00046
	                //'sSql = sSql + " WHERE  CCON_CTA   = " vGuiaDet.iNumCta ;      //'* DSA-FII/454-00047  '*PAP-CAREJ01/550-0018
	                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;     //'*PAP-CAREJ01/550-0019
	                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;   //'* DSA-FII/454-00048
	                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;  //'* DSA-FII/454-00049
	                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;  //'* DSA-FII/454-00050
	                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;  //'* DSA-FII/454-00051
	                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;    //'* DSA-FII/454-00052
	                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;          //'* DSA-FII/454-00053
	                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;          //'* DSA-FII/454-00054
	                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;          //'* DSA-FII/454-00055
	                if (sMesAnt == "H") { //'* DSA-FII/454-00056
	                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;    //'* DSA-FII/454-00056
	                    sSql = sSql + " AND  SALD_MES = 13 " ;                           //'* DSA-FII/454-00057
	                }                                                              //'* DSA-FII/454-00058
	                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;             //'* DSA-FII/454-00059
                    //		DoEvents                                                            '* DSA-FII/454-00060
                    rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;       //'* DSA-FII/454-00061
                    //		DoEvents                                                            '* DSA-FII/454-00062
                    if (sMsgError.length() > 0) {//GoTo EscalaSaldosAbonoErr                '* DSA-FII/454-00063
            		    if (oAccesoBD != null) { oAccesoBD = null ; }
            		    if (rsSaldos != null) { rsSaldos = null ; }
            		    sMsgError = sMsgError + "\n" + 
            		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                        return resultado ;
                    }
                    if (oAccesoBD != null) { oAccesoBD = null ; }           //'* DSA-FII/454-00064
                    vGuiaDet.lAux2 = PA_SALD_AX2 ;                                       //'* DSA-FII/454-00065
                    
                    if (rsSaldos.EOF) {                                        //'* DSA-FII/454-00066
	                    sSql = " INSERT INTO FDSALDOS" + sMesAnt ;             //'* DSA-FII/454-00067
	                    sSql = sSql + " VALUES(" ;                             //'* DSA-FII/454-00068
	                    //'sSql = sSql + vGuiaDet.iNumCta + "," ;        	  //'* DSA-FII/454-00069  '*PAP-CAREJ01/550-0020
	                    sSql = sSql + lngCuentaConBanco + "," ;               //'*PAP-CAREJ01/550-0021
	                    sSql = sSql + vGuiaDet.iNumSubCta + "," ;             //'* DSA-FII/454-00070
	                    sSql = sSql + vGuiaDet.iNum2SubCta + "," ;            //'* DSA-FII/454-00071
	                    sSql = sSql + vGuiaDet.iNum3SubCta + "," ;            //'* DSA-FII/454-00072
	                    sSql = sSql + vGuiaDet.iNum4SubCta + "," ;            //'* DSA-FII/454-00073
	                    sSql = sSql + vGuiaDet.iNumMoneda + "," ;             //'* DSA-FII/454-00074
	                    sSql = sSql + vGuiaDet.lAux1 + "," ;                  //'* DSA-FII/454-00075
	                    sSql = sSql + "0," ;                                  //'* DSA-FII/454-00076
	                    sSql = sSql + vGuiaDet.sAux3 + "," ;                  //'* DSA-FII/454-00077
	                    sSql = sSql + "0," ;                                  //'* DSA-FII/454-00078
	                    sSql = sSql + "0," ;                                  //'* DSA-FII/454-00079
	                    sSql = sSql + vGuiaDet.dValor + "," ;                 //'* DSA-FII/454-00080
	                    
	                    if (sMesAnt != "H") {                                          //'* DSA-FII/454-00081
		                    sSql = sSql + "0," ;                                         //'* DSA-FII/454-00082
		                    sSql = sSql + "0," ;                                         //'* DSA-FII/454-00083
		                    sSql = sSql + vGuiaDet.dValor + "," ;                    //'* DSA-FII/454-00084
	                    }                                                          //'* DSA-FII/454-00085
	                    
	                    sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + "," ;                  //'* DSA-FII/454-00086
                        sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ;  //'* DSA-FII/454-00087
                        sSql = sSql + "0," ;                                              //'* DSA-FII/454-00088
                        sSql = sSql + "1," ;                                             //'* DSA-FII/454-00089
	                    
                        if (sMesAnt != "H") {                                          //'* DSA-FII/454-00090
	                        sSql = sSql + "0," ;                                         //'* DSA-FII/454-00091
	                        sSql = sSql + "1)" ;                                         //'* DSA-FII/454-00092
                        }
                        else {                                                            //'* DSA-FII/454-00093
	                        sSql = sSql + Year(vGuiaDet.dtFechaMov) + "," ;              // '* DSA-FII/454-00094
	                        sSql = sSql + "13)" ;                                         //'* DSA-FII/454-00095
                        }                                                          //'* DSA-FII/454-00096 
                    }
                    else {
                    	sSql = " UPDATE FDSALDOS" + sMesAnt ;                             //'* DSA-FII/454-00098
                        sSql = sSql + " SET    SALD_ABONOS_MES   = SALD_ABONOS_MES + " + vGuiaDet.dValor + "," ; //'* DSA-FII/454-00099
                        sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + "," ;  //'* DSA-FII/454-00100
                        sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ; //'* DSA-FII/454-00101
                        sSql = sSql + "        SALD_NABONOS_MES  = SALD_NABONOS_MES + 1" ;    //'* DSA-FII/454-00102
                        if (sMesAnt != "H") {          //'* DSA-FII/454-00103
	                        sSql = sSql + "        , SALD_ABONOS_EJER  = SALD_ABONOS_EJER + " + vGuiaDet.dValor + "," ;  //'* DSA-FII/454-00104
	                        sSql = sSql + "        SALD_NABONOS_EJER = SALD_NABONOS_EJER + 1" ;   //'* DSA-FII/454-00105
                        }  //'* DSA-FII/454-00106
                        
                        //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta ;   '* DSA-FII/454-00107   '*PAP-CAREJ01/550-0022
                        sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;      //'*PAP-CAREJ01/550-0023
                        sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;    //'* DSA-FII/454-00108
                        sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;   //'* DSA-FII/454-00109
                        sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;   //'* DSA-FII/454-00110
                        sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;  //'* DSA-FII/454-00111
                        sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;    //'* DSA-FII/454-00111
                        sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;           //'* DSA-FII/454-00112
                        sSql = sSql + " AND    SALD_AX2 = " + 0 ;                             //'* DSA-FII/454-00113
                        sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;           //'* DSA-FII/454-00114
                        
                        if (sMesAnt == "H") {                                               //'* DSA-FII/454-00115
                            sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ; ;//'* DSA-FII/454-00116
                            sSql = sSql + " AND  SALD_MES = 13 " ;                            //'* DSA-FII/454-00117
                        }                                                              //'* DSA-FII/454-00118 
                    }
                    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;                 //'* DSA-FII/454-00120
                    //		DoEvents                                                                //'* DSA-FII/454-00121
                    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;              //'* DSA-FII/454-00122
                    //		DoEvents                                                                //'* DSA-FII/454-00123
                    if (oAccesoBD != null) { oAccesoBD = null ; }                //'* DSA-FII/454-00124
                    if (!bCorrecto) {//GoTo EscalaSaldosAbonoErr                         //'* DSA-FII/454-00125
            		    if (oAccesoBD != null) { oAccesoBD = null ; }
            		    if (rsSaldos != null) { rsSaldos = null ; }
            		    sMsgError = sMsgError + "\n" + 
            		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                        return resultado ;
                    }
                }
                
       //Linea 350'****************************FIN SUCONTRATO**************************       '* DSA-FII/454-00127
                //'****************************INICIO AUXILIAR 3**************************    '* DSA-FII/454-00128
                if (vGuiaDet.sAux3 > 0) {                                                  //'* DSA-FII/454-00129
	                PA_SALD_AX3 = vGuiaDet.sAux3 ;                                           //'* DSA-FII/454-00130
	                vGuiaDet.sAux3 = 0;                                                      //'* DSA-FII/454-00131
	                sSql = " SELECT CCON_CTA" ;                                              //'* DSA-FII/454-00132
	                sSql = sSql + " FROM   FDSALDOS" + sMesAnt ;                              //'* DSA-FII/454-00133
            		//'sSql = sSql + " WHERE  CCON_CTA = " + vGuiaDet.iNumCta ;//'* DSA-FII/454-00134  '*PAP-CAREJ01/550-0024
	                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;         //'*PAP-CAREJ01/550-0025
	                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;       //'* DSA-FII/454-00135
	                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;      //'* DSA-FII/454-00136
	                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;      //'* DSA-FII/454-00137
	                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;      //'* DSA-FII/454-00138
	                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;    //'* DSA-FII/454-00139
	                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;              //'* DSA-FII/454-00140
	                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;              //'* DSA-FII/454-00141
	                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3;              //'* DSA-FII/454-00142
	                
	                if (sMesAnt == "H") {                                                   //'* DSA-FII/454-00143
	                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov); //'* DSA-FII/454-00144
	                    sSql = sSql + " AND  SALD_MES = 13 ";                                //'* DSA-FII/454-00145
	                }                                                                  //'* DSA-FII/454-00146
	                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;                 //'* DSA-FII/454-00147
	                //		DoEvents                                                                //'* DSA-FII/454-00148
	                rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError);           //'* DSA-FII/454-00149
	                //		DoEvents                                                                //'* DSA-FII/454-00150
	                if (sMsgError.length() > 0) {//GoTo EscalaSaldosAbonoErr                    //'* DSA-FII/454-00151
	        		    if (oAccesoBD != null) { oAccesoBD = null ; }
	        		    if (rsSaldos != null) { rsSaldos = null ; }
	        		    sMsgError = sMsgError + "\n" + 
	        		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
	                    return resultado ;
	                }
	                if (oAccesoBD != null) { oAccesoBD = null ; }                //'* DSA-FII/454-00152
	                vGuiaDet.sAux3 = PA_SALD_AX3 ;                                           //'* DSA-FII/454-00153
	                
	                if (rsSaldos.EOF) {                                                     //'* DSA-FII/454-00154
		                sSql = " INSERT INTO FDSALDOS" + sMesAnt;                            //'* DSA-FII/454-00155
		                sSql = sSql + " VALUES(";                                            //'* DSA-FII/454-00156
                		//'sSql = sSql + vGuiaDet.iNumCta + "," ;       //'* DSA-FII/454-00157   '*PAP-CAREJ01/550-0026
		                sSql = sSql + lngCuentaConBanco + ",";                          //'*PAP-CAREJ01/550-0027
		                sSql = sSql + vGuiaDet.iNumSubCta + ",";                        //'* DSA-FII/454-00158
		                sSql = sSql + vGuiaDet.iNum2SubCta + "," ;                      //'* DSA-FII/454-00159
		                sSql = sSql + vGuiaDet.iNum3SubCta + "," ;                      //'* DSA-FII/454-00160
		                sSql = sSql + vGuiaDet.iNum4SubCta + "," ;                      //'* DSA-FII/454-00161
		                sSql = sSql + vGuiaDet.iNumMoneda + "," ;                       //'* DSA-FII/454-00162
		                sSql = sSql + vGuiaDet.lAux1 + "," ;                           //'* DSA-FII/454-00163
		                sSql = sSql + vGuiaDet.lAux2 + "," ;                            //'* DSA-FII/454-00164
		                sSql = sSql + "0,";                                                  //'* DSA-FII/454-00165
		                sSql = sSql + "0,";                                                  //'* DSA-FII/454-00166
		                sSql = sSql + "0,";                                                  //'* DSA-FII/454-00167
		                sSql = sSql + vGuiaDet.dValor + ",";                            //'* DSA-FII/454-00168
		                
		                if (sMesAnt != "H") {                                              //'* DSA-FII/454-00169
		                    sSql = sSql + "0," ;                                             //'* DSA-FII/454-00170
		                    sSql = sSql + "0,";                                              //'* DSA-FII/454-00171
		                    sSql = sSql + vGuiaDet.dValor + ",";                        //'* DSA-FII/454-00172
		                }                                                              //'* DSA-FII/454-00173
		                sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ",";                   //'* DSA-FII/454-00174
		                sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),";  //'* DSA-FII/454-00175
		                sSql = sSql + "0," ;                                                 //'* DSA-FII/454-00176
		                sSql = sSql + "1," ;                                                 //'* DSA-FII/454-00177
		                
		                if (sMesAnt != "H") {                                              //'* DSA-FII/454-00178
		                    sSql = sSql + "0,";                                              //'* DSA-FII/454-00179
		                    sSql = sSql + "1)";                                              //'* DSA-FII/454-00180
		                }
		                else {                                                                //'* DSA-FII/454-00181
		                    sSql = sSql + Year(vGuiaDet.dtFechaMov) + ",";                   //'* DSA-FII/454-00182
		                    sSql = sSql + "13)";                                             //'* DSA-FII/454-00183
		                }   
	                }
	                else {
	                	
	                	sSql = " UPDATE FDSALDOS" + sMesAnt;                                 //'* DSA-FII/454-00186
                        sSql = sSql + " SET    SALD_ABONOS_MES   = SALD_ABONOS_MES + " + vGuiaDet.dValor + ",";     //'* DSA-FII/454-00187
                        sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + ",";  //'* DSA-FII/454-00188
                        sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ;  //'* DSA-FII/454-00189
                        sSql = sSql + "        SALD_NABONOS_MES  = SALD_NABONOS_MES + 1";    //'* DSA-FII/454-00190
                        if (sMesAnt != "H") {                                              //'* DSA-FII/454-00191
                            sSql = sSql + "        , SALD_ABONOS_EJER  = SALD_ABONOS_EJER + " + vGuiaDet.dValor + ",";  //'* DSA-FII/454-00192
                            sSql = sSql + "        SALD_NABONOS_EJER = SALD_NABONOS_EJER + 1";   //'* DSA-FII/454-00193
                        }                                                                  //'* DSA-FII/454-00194
                        //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta)          '* DSA-FII/454-00195   '*PAP-CAREJ01/550-0028
                        sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;          //'*PAP-CAREJ01/550-0029
                        sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;        //'* DSA-FII/454-00196
                        sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;       //'* DSA-FII/454-00197
                        sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;       //'* DSA-FII/454-00198
                        sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;       //'* DSA-FII/454-00199
                        sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;    //'* DSA-FII/454-00200
                        sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;               //'* DSA-FII/454-00201
                        sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;               //'* DSA-FII/454-00202
                        sSql = sSql + " AND    SALD_AX3 = " + 0;                                 //'* DSA-FII/454-00203
                        if (sMesAnt == "H") {                                                   //'* DSA-FII/454-00204
                            sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov); //'* DSA-FII/454-00205
                            sSql = sSql + " AND  SALD_MES = 13 ";                                //'* DSA-FII/454-00206
                        }  
	                }
	                
	                oAccesoBD = new IfiduBaseDatos.BaseDatos();                     //'* DSA-FII/454-00209
                    //		DoEvents                                                //'* DSA-FII/454-00210
                    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError);          //'* DSA-FII/454-00211
                    //		DoEvents                                                //'* DSA-FII/454-00212
                    if (oAccesoBD != null) { oAccesoBD = null ; }                    //'* DSA-FII/454-00213
                    if (!bCorrecto) {//GoTo EscalaSaldosAbonoErr                             //'* DSA-FII/454-00214
            		    if (oAccesoBD != null) { oAccesoBD = null ; }
            		    if (rsSaldos != null) { rsSaldos = null ; }
            		    sMsgError = sMsgError + "\n" + 
            		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                        return resultado ;
                    }  
                }
       //Linea 442'****************************FIN AUXILIAR 3**************************           '* DSA-FII/454-00216
                //'****************************INICIA AMBOS**************************** '* DSA-FII/454-00217
                
                if ((vGuiaDet.sAux3 > 0) && (vGuiaDet.lAux2 > 0)) {//'* DSA-FII/454-00218
	                PA_SALD_AX3 = vGuiaDet.sAux3; //'* DSA-FII/454-00219
	                PA_SALD_AX2 = vGuiaDet.lAux2; //'* DSA-FII/454-00220
	                vGuiaDet.sAux3 = 0 ;//'* DSA-FII/454-00221
	                vGuiaDet.lAux2 = 0 ;//'* DSA-FII/454-00222
	                sSql = " SELECT CCON_CTA" ; //'* DSA-FII/454-00223
	                sSql = sSql + " FROM   FDSALDOS" + sMesAnt; //'* DSA-FII/454-00224
            		//'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) '* DSA-FII/454-00225   //'*PAP-CAREJ01/550-0030
	                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //'*PAP-CAREJ01/550-0031
	                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; //'* DSA-FII/454-00226
	                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; //'* DSA-FII/454-00227
	                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; //'* DSA-FII/454-00228
	                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; //'* DSA-FII/454-00229
	                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; //'* DSA-FII/454-00230
	                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; //'* DSA-FII/454-00231
	                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2; //'* DSA-FII/454-00232
	                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3; //'* DSA-FII/454-00233
                
	                if (sMesAnt == "H") {//'* DSA-FII/454-00234
                        sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ; //'* DSA-FII/454-00235
                        sSql = sSql + " AND  SALD_MES = 13 " ; //'* DSA-FII/454-00236
	                } //'* DSA-FII/454-00237
                
	                oAccesoBD = new IfiduBaseDatos.BaseDatos(); //'* DSA-FII/454-00238
                    //		DoEvents '* DSA-FII/454-00239
                    rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ; //'* DSA-FII/454-00240
                    //		DoEvents '* DSA-FII/454-00241
                    if (sMsgError.length() > 0) { //GoTo EscalaSaldosAbonoErr //'* DSA-FII/454-00242
            		    if (oAccesoBD != null) { oAccesoBD = null ; }
            		    if (rsSaldos != null) { rsSaldos = null ; }
            		    sMsgError = sMsgError + "\n" + 
            		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                        return resultado ;
                    }
                    if (oAccesoBD != null) { oAccesoBD = null ; } //'* DSA-FII/454-00243
                    vGuiaDet.sAux3 = PA_SALD_AX3 ; //'* DSA-FII/454-00244
                    vGuiaDet.lAux2 = PA_SALD_AX2 ; //'* DSA-FII/454-00245
                
                    if (rsSaldos.EOF) { //'* DSA-FII/454-00246
	                    sSql = " INSERT INTO FDSALDOS" + sMesAnt; //'* DSA-FII/454-00247
	                    sSql = sSql + " VALUES("; //'* DSA-FII/454-00248
                		//'sSql = sSql + vGuiaDet.iNumCta) + "," '* DSA-FII/454-00249   //'*PAP-CAREJ01/550-0032
	                    sSql = sSql + lngCuentaConBanco + ",";    //'*PAP-CAREJ01/550-0033
	                    sSql = sSql + vGuiaDet.iNumSubCta + ","; //'* DSA-FII/454-00250
	                    sSql = sSql + vGuiaDet.iNum2SubCta + ","; //'* DSA-FII/454-00251
	                    sSql = sSql + vGuiaDet.iNum3SubCta + ","; //'* DSA-FII/454-00252
	                    sSql = sSql + vGuiaDet.iNum4SubCta + ","; //'* DSA-FII/454-00253
	                    sSql = sSql + vGuiaDet.iNumMoneda + "," ;//'* DSA-FII/454-00254
	                    sSql = sSql + vGuiaDet.lAux1 + ","; //'* DSA-FII/454-00255
	                    sSql = sSql + "0," ;//'* DSA-FII/454-00256
	                    sSql = sSql + "0," ;//'* DSA-FII/454-00257
	                    sSql = sSql + "0," ;//'* DSA-FII/454-00258
	                    sSql = sSql + "0," ;//'* DSA-FII/454-00259
	                    sSql = sSql + vGuiaDet.dValor + ","; //'* DSA-FII/454-00260
	                    
	                    if (sMesAnt != "H") {//'* DSA-FII/454-00261
		                    sSql = sSql + "0," ; //'* DSA-FII/454-00262
		                    sSql = sSql + "0," ; //'* DSA-FII/454-00263
		                    sSql = sSql + vGuiaDet.dValor + ","; //'* DSA-FII/454-00264
	                    } //'* DSA-FII/454-00265
		                sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + "," ; //'* DSA-FII/454-00266
		                sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ;//'* DSA-FII/454-00267
		                sSql = sSql + "0," ;//'* DSA-FII/454-00268
		                sSql = sSql + "1," ;//'* DSA-FII/454-00269
	                    
		                if (sMesAnt != "H") {//'* DSA-FII/454-00270
		                    sSql = sSql + "0," ; //'* DSA-FII/454-00271
		                    sSql = sSql + "1)" ; //'* DSA-FII/454-00272
		                }
		                else { //'* DSA-FII/454-00273
		                    sSql = sSql + Year(vGuiaDet.dtFechaMov) + "," ; //'* DSA-FII/454-00274
		                    sSql = sSql + "13)"; //'* DSA-FII/454-00275
		                } //'* DSA-FII/454-00276 
                    }
                    else {
                    	
                    	sSql = " UPDATE FDSALDOS" + sMesAnt; //'* DSA-FII/454-00278
                        sSql = sSql + " SET    SALD_ABONOS_MES   = SALD_ABONOS_MES + " + vGuiaDet.dValor + ","; //'* DSA-FII/454-00279
                        sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + "," ; //'* DSA-FII/454-00280
                        sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ; //'* DSA-FII/454-00281
                        sSql = sSql + "        SALD_NABONOS_MES  = SALD_NABONOS_MES + 1" ; //'* DSA-FII/454-00282
                        
                        if (sMesAnt != "H") {//'* DSA-FII/454-00283
	                        sSql = sSql + "        , SALD_ABONOS_EJER  = SALD_ABONOS_EJER + " + vGuiaDet.dValor + ","; //'* DSA-FII/454-00284
	                        sSql = sSql + "        SALD_NABONOS_EJER = SALD_NABONOS_EJER + 1"; //'* DSA-FII/454-00285
                        } //'* DSA-FII/454-00286
                    	
                        //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) '* DSA-FII/454-00287   '*PAP-CAREJ01/550-0034
                        sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;    //'*PAP-CAREJ01/550-0035
                        sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ; //'* DSA-FII/454-00288
                        sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ; //'* DSA-FII/454-00289
                        sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ; //'* DSA-FII/454-00290
                        sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ; //'* DSA-FII/454-00291
                        sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ; //'* DSA-FII/454-00292
                        sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ; //'* DSA-FII/454-00293
                        sSql = sSql + " AND    SALD_AX2 = " + 0; //'* DSA-FII/454-00294
                        sSql = sSql + " AND    SALD_AX3 = " + 0; //'* DSA-FII/454-00295
                        
                        if (sMesAnt == "H") { //'* DSA-FII/454-00296
                            sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ; //'* DSA-FII/454-00297
                            sSql = sSql + " AND  SALD_MES = 13 " ; //'* DSA-FII/454-00298
                        } //'* DSA-FII/454-00299
                        
                    }
                    oAccesoBD = new IfiduBaseDatos.BaseDatos() ; //'* DSA-FII/454-00301
                    //		DoEvents '* DSA-FII/454-00302
                    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError); //'* DSA-FII/454-00303
                    //		DoEvents '* DSA-FII/454-00304
                    if (oAccesoBD != null) { oAccesoBD = null ; } //'* DSA-FII/454-00305
                    if (!bCorrecto) { //GoTo EscalaSaldosAbonoErr //'* DSA-FII/454-00306
            		    if (oAccesoBD != null) { oAccesoBD = null ; }
            		    if (rsSaldos != null) { rsSaldos = null ; }
            		    sMsgError = sMsgError + "\n" + 
            		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                        return resultado ;
                    }
                }
                //'****************************FIN AMBOS**************************** '* DSA-FII/454-00309
                
                //'************************************************************
                //'dctb 20051201
                /*
                if (sMesAnt == "MSA") {
        			sSql = " SELECT CCON_CTA";
	                sSql = sSql + " FROM   FDSALDOS" ;
	                //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta)    '*PAP-CAREJ01/550-0036
	                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //'*PAP-CAREJ01/550-0037
	                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;
	                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;
	                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;
	                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;
	                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;
	                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;
	                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;
	                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3;
	                 
	                 oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
	                 //		DoEvents
	                 rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
	                 //		DoEvents
	                 if (sMsgError.length() > 0){// Then GoTo EscalaSaldosAbonoErr
	         		    if (oAccesoBD != null) { oAccesoBD = null ; }
	         		    if (rsSaldos != null) { rsSaldos = null ; }
	         		    sMsgError = sMsgError + "\n" + 
	         		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
	                     return resultado ;
	                 }
	                 if (oAccesoBD != null) { oAccesoBD = null ; }
	                 
	                 if (rsSaldos.EOF) {
						sSql = " INSERT INTO FDSALDOS";
						sSql = sSql + " VALUES(";
						//'sSql = sSql + vGuiaDet.iNumCta) + ","                     '*PAP-CAREJ01/550-0038
						sSql = sSql + lngCuentaConBanco + "," ;                    //'*PAP-CAREJ01/550-0039
						sSql = sSql + vGuiaDet.iNumSubCta + "," + vGuiaDet.iNum2SubCta + ",";
						sSql = sSql + vGuiaDet.iNum3SubCta + "," + vGuiaDet.iNum4SubCta + ",";
						sSql = sSql + vGuiaDet.iNumMoneda + "," + vGuiaDet.lAux1 + ",";
						sSql = sSql + vGuiaDet.lAux2 + "," + vGuiaDet.sAux3 + ",";
						sSql = sSql +  String.valueOf(vGuiaDet.dValor * -1) + ",";
						sSql = sSql + "0," + "0,";
						sSql = sSql +  String.valueOf(vGuiaDet.dValor * -1) + ",";
						sSql = sSql + "0," + "0,";
						sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ",";
						sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),";
						sSql = sSql + "0," + "0," + "0," + "0)";
	                 }
	                 else {
	                	 sSql = " UPDATE FDSALDOS";
                         sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + String.valueOf(vGuiaDet.dValor * -1) + ",";
                         sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + ",";
            		 	 //'sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "'),";
                         sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')"; //'* RAC FII/136-1892
            		 	 //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta;        //'*PAP-CAREJ01/550-0040
                         sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;        //'*PAP-CAREJ01/550-0041
                         sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;
                         sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;
                         sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;
                         sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;
                         sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;
                         sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;
                         sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;
                         sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3;
	                 }
	                 
	                 oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
                     //		DoEvents
                     bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
                     //		DoEvents
                     if (oAccesoBD != null) { oAccesoBD = null ; }
                     if (!bCorrecto){ //GoTo EscalaSaldosAbonoErr
						if (oAccesoBD != null) { oAccesoBD = null ; }
						if (rsSaldos != null) { rsSaldos = null ; }
						sMsgError = sMsgError + "\n" + 
						    "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
						return resultado ;
                     }
                     //'****************************INICIO SUCONTRATO************************** '* DSA-FII/454-00310
                     */
                 if (sMesAnt == "MSA") {
            		 sSql = " SELECT CCON_CTA";
                     sSql = sSql + " FROM   FDSALDOS";
                    		 //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta)    //'*PAP-CAREJ01/550-0036
                     sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //'*PAP-CAREJ01/550-0037
                     sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;
                     sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;
                     sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;
                     sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;
                     sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;
                     sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;
                     sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;
                     sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3;
                      
                     oAccesoBD = new IfiduBaseDatos.BaseDatos();
                     //		DoEvents
                     rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError);
                     //		DoEvents
                     if (sMsgError.length() > 0) {//GoTo EscalaSaldosAbonoErr
						if (oAccesoBD != null) { oAccesoBD = null ; }
						if (rsSaldos != null) { rsSaldos = null ; }
						sMsgError = sMsgError + "\n" + 
						            "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
						return resultado ;
                     }
                     if (oAccesoBD != null) { oAccesoBD = null ; }
                     
                     if (rsSaldos.EOF) {
                    	sSql = " INSERT INTO FDSALDOS" + " VALUES(" ;
						//'sSql = sSql + vGuiaDet.iNumCta) + ","   ;                   '*PAP-CAREJ01/550-0038
						sSql = sSql + lngCuentaConBanco + "," ;                     //'*PAP-CAREJ01/550-0039
						sSql = sSql + vGuiaDet.iNumSubCta + "," + vGuiaDet.iNum2SubCta + "," ;
						sSql = sSql + vGuiaDet.iNum3SubCta + "," + vGuiaDet.iNum4SubCta + "," ;
						sSql = sSql + vGuiaDet.iNumMoneda + "," + vGuiaDet.lAux1 + "," ;
						sSql = sSql + vGuiaDet.lAux2 + "," + vGuiaDet.sAux3 + "," ;
						sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + "," ;
						sSql = sSql + "0," + "0," ;
						sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + "," ;
						sSql = sSql + "0," + "0," ;
						sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + "," ;
						sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')," ;
						sSql = sSql + "0," + "0," + "0," + "0)" ;
                     }
                     else {
						sSql = " UPDATE FDSALDOS" ;
						sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + String.valueOf(vGuiaDet.dValor * -1) + ",";
						sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + ",";
						//'sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),";
						sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')"; //'* RAC FII/136-1892
								//'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta;        '*PAP-CAREJ01/550-0040
						sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;        //'*PAP-CAREJ01/550-0041
						sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;
						sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;
						sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;
						sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;
						sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;
						sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;
						sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;
						sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3;
                     }
                     
                     oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
                     //		DoEvents
                     bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
                     //		DoEvents
                     if (oAccesoBD != null) { oAccesoBD = null ; }
                     if (!bCorrecto) {//GoTo EscalaSaldosAbonoErr
						if (oAccesoBD != null) { oAccesoBD = null ; }
						if (rsSaldos != null) { rsSaldos = null ; }
						sMsgError = sMsgError + "\n" + 
						        "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
						return resultado ;
                     }
                     
                     //'****************************INICIO SUCONTRATO************************** '* DSA-FII/454-00310
                     if (vGuiaDet.lAux2 > 0) { //'* DSA-FII/454-00311
                         PA_SALD_AX2 = vGuiaDet.lAux2; //'* DSA-FII/454-00312
                         vGuiaDet.lAux2 = 0; //'* DSA-FII/454-00313
                         sSql = " SELECT CCON_CTA"; //'* DSA-FII/454-00314
                         sSql = sSql + " FROM   FDSALDOS"; //'* DSA-FII/454-00315
                		 //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta); '* DSA-FII/454-00316    '*PAP-CAREJ01/550-0042
                         sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //'*PAP-CAREJ01/550-0043
                         sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; //'* DSA-FII/454-00317
                         sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; //'* DSA-FII/454-00318
                         sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; //'* DSA-FII/454-00319
                         sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; //'* DSA-FII/454-00320
                         sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; //'* DSA-FII/454-00321
                         sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; //'* DSA-FII/454-00322
                         sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2; //'* DSA-FII/454-00323
                         sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3; //'* DSA-FII/454-00324
                     
                         if (sMesAnt == "H") {// Then '* DSA-FII/454-00325
                             sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov); //'* DSA-FII/454-00326
                             sSql = sSql + " AND  SALD_MES = 13 "; //'* DSA-FII/454-00327
                         } //'* DSA-FII/454-00328
                         oAccesoBD = new IfiduBaseDatos.BaseDatos() ; //'* DSA-FII/454-00329
                         //		DoEvents //'* DSA-FII/454-00330
                         rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ; //'* DSA-FII/454-00331
                         //		DoEvents //'* DSA-FII/454-00332
                         if (sMsgError.length() > 0) {//GoTo EscalaSaldosAbonoErr //'* DSA-FII/454-00333
                 		     if (oAccesoBD != null) { oAccesoBD = null ; }
                 		     if (rsSaldos != null) { rsSaldos = null ; }
                 		     sMsgError = sMsgError + "\n" + 
                 		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                             return resultado ;
                         }
                         if (oAccesoBD != null) { oAccesoBD = null ; } //'* DSA-FII/454-00334
                         vGuiaDet.lAux2 = PA_SALD_AX2 ; //'* DSA-FII/454-00335
                         
                         if (rsSaldos.EOF) {// Then '* DSA-FII/454-00336
                             sSql = " INSERT INTO FDSALDOS"; //'* DSA-FII/454-00337
                             sSql = sSql + " VALUES("; //'* DSA-FII/454-00338
                    		 //'sSql = sSql + vGuiaDet.iNumCta + "," '* DSA-FII/454-00339  '*PAP-CAREJ01/550-0044
                             sSql = sSql + lngCuentaConBanco + ",";    //'*PAP-CAREJ01/550-0045
                             sSql = sSql + vGuiaDet.iNumSubCta + ","; //'* DSA-FII/454-00340
                             sSql = sSql + vGuiaDet.iNum2SubCta + ","; //'* DSA-FII/454-00341
                             sSql = sSql + vGuiaDet.iNum3SubCta + ","; //'* DSA-FII/454-00342
                             sSql = sSql + vGuiaDet.iNum4SubCta + ","; //'* DSA-FII/454-00343
                             sSql = sSql + vGuiaDet.iNumMoneda + ","; //'* DSA-FII/454-00344
                             sSql = sSql + vGuiaDet.lAux1 + ","; //'* DSA-FII/454-00345
                             sSql = sSql + "0,"; //'* DSA-FII/454-00346
                             sSql = sSql + vGuiaDet.sAux3 + ","; //'* DSA-FII/454-00347
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00348
                             sSql = sSql + "0,"; //'* DSA-FII/454-00349
                             sSql = sSql + "0,"; //'* DSA-FII/454-00350
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00351
                             sSql = sSql + "0,"; //'* DSA-FII/454-00352
                             sSql = sSql + "0,"; //'* DSA-FII/454-00353
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00354
                             sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),"; //'* DSA-FII/454-00355
                             sSql = sSql + "0,"; //'* DSA-FII/454-00356
                             sSql = sSql + "0,"; //'* DSA-FII/454-00357
                             sSql = sSql + "0,"; //'* DSA-FII/454-00358
                             sSql = sSql + "0)"; //'* DSA-FII/454-00359
                         }
                         else {
                        	 sSql = " UPDATE FDSALDOS" ; //'* DSA-FII/454-00361
                             sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + String.valueOf(vGuiaDet.dValor * -1) + "," ; //'* DSA-FII/454-00362
                             sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + "," ; //'* DSA-FII/454-00363
                    		 //'sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),"; //'* DSA-FII/454-00364
                             sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')"; //'* RAC FII/136-1892 '* DSA-FII/454-00365
                    		 //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) '* DSA-FII/454-00366    '*PAP-CAREJ01/550-0046
                             sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //'*PAP-CAREJ01/550-0047
                             sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; //'* DSA-FII/454-00367
                             sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; //'* DSA-FII/454-00368
                             sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; //'* DSA-FII/454-00369
                             sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; //'* DSA-FII/454-00370
                             sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; //'* DSA-FII/454-00371
                             sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; //'* DSA-FII/454-00372
                             sSql = sSql + " AND    SALD_AX2 = " + 0 ; //'* DSA-FII/454-00373
                             sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3; //'* DSA-FII/454-00374
                         }
                         
                         oAccesoBD = new IfiduBaseDatos.BaseDatos() ; // '* DSA-FII/454-00376
                         //		DoEvents '* DSA-FII/454-00377
                         bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;// '* DSA-FII/454-00378
                         //		DoEvents '* DSA-FII/454-00379
                         if (oAccesoBD != null) { oAccesoBD = null ; }//'* DSA-FII/454-00380
                         if (!bCorrecto) {// Then GoTo EscalaSaldosAbonoErr '* DSA-FII/454-00381
                 		     if (oAccesoBD != null) { oAccesoBD = null ; }
                 		     if (rsSaldos != null) { rsSaldos = null ; }
                 		     sMsgError = sMsgError + "\n" + 
                 		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                             return resultado ;
                         }
                         
                     }
                     
                     //'****************************FIN SUCONTRATO************************** '* DSA-FII/454-00383
                     //'****************************INICIO AUXILIAR 3************************** '* DSA-FII/454-00384
                     
                     if (vGuiaDet.sAux3 > 0) {// Then '* DSA-FII/454-00385
                         PA_SALD_AX3 = vGuiaDet.sAux3 ;//'* DSA-FII/454-00386
                         vGuiaDet.sAux3 = 0 ;//'* DSA-FII/454-00387
                         sSql = " SELECT CCON_CTA" ;//'* DSA-FII/454-00388
                         sSql = sSql + " FROM   FDSALDOS"; //'* DSA-FII/454-00389
                		 //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) '* DSA-FII/454-00390   '*PAP-CAREJ01/550-0048
                         sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //'*PAP-CAREJ01/550-0049
                         sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; //'* DSA-FII/454-00391
                         sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; //'* DSA-FII/454-00392
                         sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; //'* DSA-FII/454-00393
                         sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; //'* DSA-FII/454-00394
                         sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; //'* DSA-FII/454-00395
                         sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; //'* DSA-FII/454-00396
                         sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2; //'* DSA-FII/454-00397
                         sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3; //'* DSA-FII/454-00398
                         
                         if (sMesAnt == "H") {// Then '* DSA-FII/454-00399
                             sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;// '* DSA-FII/454-00400
                             sSql = sSql + " AND  SALD_MES = 13 "; //'* DSA-FII/454-00401
                         } //'* DSA-FII/454-00402
                         oAccesoBD = new IfiduBaseDatos.BaseDatos() ; //'* DSA-FII/454-00403
                         //		DoEvents //'* DSA-FII/454-00404
                         rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ; //'* DSA-FII/454-00405
                         //		DoEvents //'* DSA-FII/454-00406
                         if (sMsgError.length() > 0) {// Then GoTo EscalaSaldosAbonoErr //'* DSA-FII/454-00407
                 		     if (oAccesoBD != null) { oAccesoBD = null ; }
                 		     if (rsSaldos != null) { rsSaldos = null ; }
                 		     sMsgError = sMsgError + "\n" + 
                 		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                             return resultado ;
                         }
                         if (oAccesoBD != null) { oAccesoBD = null ; }//'* DSA-FII/454-00408
                         vGuiaDet.sAux3 = PA_SALD_AX3 ; //'* DSA-FII/454-00409
                         
                         if (rsSaldos.EOF) {// Then '* DSA-FII/454-00410
                             sSql = " INSERT INTO FDSALDOS"; //'* DSA-FII/454-00411
                             sSql = sSql + " VALUES("; //'* DSA-FII/454-00412
                    		 //'sSql = sSql + vGuiaDet.iNumCta + "," '* DSA-FII/454-00413   '*PAP-CAREJ01/550-0050
                             sSql = sSql + lngCuentaConBanco + ",";    //'*PAP-CAREJ01/550-0051
                             sSql = sSql + vGuiaDet.iNumSubCta + ","; //'* DSA-FII/454-00414
                             sSql = sSql + vGuiaDet.iNum2SubCta + ","; //'* DSA-FII/454-00415
                             sSql = sSql + vGuiaDet.iNum3SubCta + ","; //'* DSA-FII/454-00416
                             sSql = sSql + vGuiaDet.iNum4SubCta + ","; //'* DSA-FII/454-00417
                             sSql = sSql + vGuiaDet.iNumMoneda + ","; //'* DSA-FII/454-00418
                             sSql = sSql + vGuiaDet.lAux1 + ","; //'* DSA-FII/454-00419
                             sSql = sSql + vGuiaDet.lAux2 + ","; //'* DSA-FII/454-00420
                             sSql = sSql + "0,"; //'* DSA-FII/454-00421
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00422
                             sSql = sSql + "0,"; //'* DSA-FII/454-00423
                             sSql = sSql + "0,"; //'* DSA-FII/454-00424
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00425
                             sSql = sSql + "0,"; //'* DSA-FII/454-00426
                             sSql = sSql + "0,"; //'* DSA-FII/454-00427
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00428
                             sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),"; //'* DSA-FII/454-00429
                             sSql = sSql + "0,"; //'* DSA-FII/454-00430
                             sSql = sSql + "0,"; //'* DSA-FII/454-00431
                             sSql = sSql + "0,"; //'* DSA-FII/454-00432
                             sSql = sSql + "0)"; //'* DSA-FII/454-00433
                         }
                         else {
                        	 sSql = " UPDATE FDSALDOS" ; //'* DSA-FII/454-00435
                             sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00436
                             sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + ","; //'* DSA-FII/454-00437
                    		 //'sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),"; '* DSA-FII/454-00438
                             sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')"; //'* RAC FII/136-1892 '* DSA-FII/454-00439
                    		 //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta; '* DSA-FII/454-00440   '*PAP-CAREJ01/550-0052
                             sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;   //'*PAP-CAREJ01/550-0053
                             sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; //'* DSA-FII/454-00441
                             sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; //'* DSA-FII/454-00442
                             sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; //'* DSA-FII/454-00443
                             sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; //'* DSA-FII/454-00444
                             sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; //'* DSA-FII/454-00445
                             sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; //'* DSA-FII/454-00446
                             sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2; //'* DSA-FII/454-00447
                             sSql = sSql + " AND    SALD_AX3 = " + 0; //'* DSA-FII/454-00448
                         }
                         
                         oAccesoBD = new IfiduBaseDatos.BaseDatos() ;// '* DSA-FII/454-00450
                         //		DoEvents '* DSA-FII/454-00451
                         bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;// '* DSA-FII/454-00452
                         //		DoEvents '* DSA-FII/454-00453
                         if (oAccesoBD != null) { oAccesoBD = null ; } //'* DSA-FII/454-00454
                         if (!bCorrecto) {// Then GoTo EscalaSaldosAbonoErr '* DSA-FII/454-00455
                 		     if (oAccesoBD != null) { oAccesoBD = null ; }
                 		     if (rsSaldos != null) { rsSaldos = null ; }
                 		     sMsgError = sMsgError + "\n" + 
                 		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                             return resultado ;
                         }
                     }
                     
                     //'****************************FIN AUXILIAR 3************************** '* DSA-FII/454-00457
                     //'* DSA-FII/454-00458
                     //'****************************INICIO AMBOS************************** '* DSA-FII/454-00459
                     if ((vGuiaDet.sAux3 > 0) && (vGuiaDet.lAux2 > 0)) {// Then '* DSA-FII/454-00460
                         PA_SALD_AX3 = vGuiaDet.sAux3 ; //'* DSA-FII/454-00461
                         PA_SALD_AX2 = vGuiaDet.lAux2; //'* DSA-FII/454-00462
                         vGuiaDet.sAux3 = 0; //'* DSA-FII/454-00463
                         vGuiaDet.lAux2 = 0; //'* DSA-FII/454-00464
                         sSql = " SELECT CCON_CTA"; //'* DSA-FII/454-00465
                         sSql = sSql + " FROM   FDSALDOS"; //'* DSA-FII/454-00466
                		 //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) '* DSA-FII/454-00467   '*PAP-CAREJ01/550-0054
                         sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //'*PAP-CAREJ01/550-0055
                         sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; //'* DSA-FII/454-00468
                         sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; //'* DSA-FII/454-00469
                         sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; //'* DSA-FII/454-00470
                         sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; //'* DSA-FII/454-00471
                         sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; //'* DSA-FII/454-00472
                         sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; //'* DSA-FII/454-00473
                         sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2; //'* DSA-FII/454-00474
                         sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3; //'* DSA-FII/454-00475
                     
                         if (sMesAnt == "H"){// Then '* DSA-FII/454-00476
                             sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov); //'* DSA-FII/454-00477
                             sSql = sSql + " AND  SALD_MES = 13 "; // '* DSA-FII/454-00478
                         } //'* DSA-FII/454-00479
                         oAccesoBD = new IfiduBaseDatos.BaseDatos() ; //'* DSA-FII/454-00480
                         //		DoEvents '* DSA-FII/454-00481
                         rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError); // '* DSA-FII/454-00482
                         //		DoEvents '* DSA-FII/454-00483
                         if (sMsgError.length() > 0) {// Then GoTo EscalaSaldosAbonoErr '* DSA-FII/454-00484
                 		     if (oAccesoBD != null) { oAccesoBD = null ; }
                 		     if (rsSaldos != null) { rsSaldos = null ; }
                 		     sMsgError = sMsgError + "\n" + 
                 		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                             return resultado ;
                         }
                         if (oAccesoBD != null) { oAccesoBD = null ; } //'* DSA-FII/454-00485
                         vGuiaDet.sAux3 = PA_SALD_AX3; // '* DSA-FII/454-00486
                         vGuiaDet.lAux2 = PA_SALD_AX2; // '* DSA-FII/454-00487
                     
                         if (rsSaldos.EOF) {// Then '* DSA-FII/454-00488
                             sSql = " INSERT INTO FDSALDOS" ; //'* DSA-FII/454-00489
                             sSql = sSql + " VALUES(" ; //'* DSA-FII/454-00490
                             //'sSql = sSql + vGuiaDet.iNumCta) + "," '* DSA-FII/454-00491   '*PAP-CAREJ01/550-0056
                             sSql = sSql + lngCuentaConBanco + "," ; //   '*PAP-CAREJ01/550-0057
                             sSql = sSql + vGuiaDet.iNumSubCta + ","; // '* DSA-FII/454-00492
                             sSql = sSql + vGuiaDet.iNum2SubCta + ","; // '* DSA-FII/454-00493
                             sSql = sSql + vGuiaDet.iNum3SubCta + ","; // '* DSA-FII/454-00494
                             sSql = sSql + vGuiaDet.iNum4SubCta + ","; // '* DSA-FII/454-00495
                             sSql = sSql + vGuiaDet.iNumMoneda + ","; // '* DSA-FII/454-00496
                             sSql = sSql + vGuiaDet.lAux1 + ","; // '* DSA-FII/454-00497
                             sSql = sSql + "0,"; // '* DSA-FII/454-00498
                             sSql = sSql + "0,"; // '* DSA-FII/454-00499
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; // '* DSA-FII/454-00500
                             sSql = sSql + "0,"; // '* DSA-FII/454-00501
                             sSql = sSql + "0,"; // '* DSA-FII/454-00502
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; // '* DSA-FII/454-00503
                             sSql = sSql + "0,"; // '* DSA-FII/454-00504
                             sSql = sSql + "0,"; // '* DSA-FII/454-00505
                             sSql = sSql + String.valueOf(vGuiaDet.dValor * -1) + ","; // '* DSA-FII/454-00506
                             sSql = sSql + "DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),"; // '* DSA-FII/454-00507
                             sSql = sSql + "0,"; // '* DSA-FII/454-00508
                             sSql = sSql + "0,"; // '* DSA-FII/454-00509
                             sSql = sSql + "0,"; // '* DSA-FII/454-00510
                             sSql = sSql + "0)"; // '* DSA-FII/454-00511
                         }
                         else {
                        	 sSql = " UPDATE FDSALDOS"; // '* DSA-FII/454-00513
                             sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + String.valueOf(vGuiaDet.dValor * -1) + ","; // '* DSA-FII/454-00514
                             sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + String.valueOf(vGuiaDet.dValor * -1) + ","; // '* DSA-FII/454-00515
                             //'sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "'),"; // '* DSA-FII/454-00516
                             sSql = sSql + "        SALD_ULTMOD       = DATE('" + formateador.format(vGuiaDet.dtFechaMov) + "')"; // '* RAC FII/136-1892 '* DSA-FII/454-00517
                             //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta); // '* DSA-FII/454-00518   '*PAP-CAREJ01/550-0058
                             sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ; //   '*PAP-CAREJ01/550-0059
                             sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; // '* DSA-FII/454-00519
                             sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; // '* DSA-FII/454-00520
                             sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; // '* DSA-FII/454-00521
                             sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; // '* DSA-FII/454-00522
                             sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; // '* DSA-FII/454-00523
                             sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; // '* DSA-FII/454-00524
                             sSql = sSql + " AND    SALD_AX2 = " + 0; // '* DSA-FII/454-00525
                             sSql = sSql + " AND    SALD_AX3 = " + 0; // '* DSA-FII/454-00526
                         }
                         oAccesoBD = new IfiduBaseDatos.BaseDatos() ;// '* DSA-FII/454-00528
                         //		DoEvents '* DSA-FII/454-00529
                         bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError);// '* DSA-FII/454-00530
                         //		DoEvents '* DSA-FII/454-00531
                         if (oAccesoBD != null) { oAccesoBD = null ; } //'* DSA-FII/454-00532
                         if (!bCorrecto) {// Then GoTo EscalaSaldosAbonoErr '* DSA-FII/454-00533
                 		     if (oAccesoBD != null) { oAccesoBD = null ; }
                 		     if (rsSaldos != null) { rsSaldos = null ; }
                 		     sMsgError = sMsgError + "\n" + 
                 		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
                             return resultado ;
                         }
                     }
                 }
                 
                 //'        if (sMesAnt == ""){
                 //'            bIndicador = false ;
	    		 //			}
                 //'        else{
                 //'          if (sMesAnt == "H"){
                 //'            bIndicador = false ;
	    		 //			  }
                 //'          else{
                 //'            sMesAnt = "" ;
                 //'          }
                 //'        }
                 bIndicador = false ;
        		 //' dctb 20051201
        		 //'******************************************************************************
                 rsSaldos.Close ;
                //}
	    	}
	    	
	    	resultado = true ;
		    if (rsSaldos != null) { rsSaldos = null ; }
		    
		    if (sMesAnt == "H") {
		      sMsgError = "Mes13" ;
		    }
	  
	    }catch(Exception ex) {
	    	
	    	resultado = false ;
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (rsSaldos != null) { rsSaldos = null ; }
		    sMsgError = sMsgError + "\n" + 
		                "Fuente : " + m_sObjectName + ".EscalaSaldosAbono" ;
            return resultado ;
	    }
		
		return resultado ;
	}

	@Override
	public boolean EscalaSaldosCargo(GuiaDet vGuiaDet, boolean bMesAnt, String sMsgError) {
		// TODO Auto-generated method stub
		
		IfiduBaseDatos.clsBaseDatos oAccesoBD ;
		String sSql ;
		ADODB.Recordset rsSaldos = new ADODB.Recordset() ;
		String sMesAnt ;
	    boolean bIndicador ;
	    boolean bCorrecto ;
	    
	    long PA_SALD_AX2 ;       //'* DSA-FII/454-00536
	    double PA_SALD_AX3 ;     //'* DSA-FII/454-00537
		
	    try {
	    	
	    	if (sMsgError == "Mes13") {
		      sMesAnt = "H";
		      sMsgError = "";
	    	}
		    else {
		      if (bMesAnt) {
		          sMesAnt = "MSA" ;
		      }
		      else {
		          sMesAnt = "";
		      }
		    }
	    	bIndicador = true ;
	    	
	    	while (bIndicador) {
	    		sSql = " SELECT CCON_CTA" ;
			    sSql = sSql + " FROM   FDSALDOS" + sMesAnt ;
			    //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta        '*PAP-CAREJ01/550-0060
			    sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;        //'*PAP-CAREJ01/550-0061
			    sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;
			    sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;
			    sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;
			    sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;
			    sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;
			    sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;
			    sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;
			    sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;
			    if (sMesAnt == "H") {
			       sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;
			       sSql = sSql + " AND  SALD_MES = 13 " ;
			    }
			     
			    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
			    //		DoEvents
			    rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
			    //		DoEvents
			    if (oAccesoBD != null) { oAccesoBD = null ; }
			    if (sMsgError.length() > 0) {
			    	if (rsSaldos != null) { rsSaldos = null ; }
				    sMsgError = sMsgError + "\n" + 
				                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		            return false ;
			    }
	    	
			    if (rsSaldos.EOF) {
			    	sSql = " INSERT INTO FDSALDOS" + sMesAnt ;
			        sSql = sSql + " VALUES(" ;
			        //'sSql = sSql + vGuiaDet.iNumCta) + "," ;        '*PAP-CAREJ01/550-0062
			        sSql = sSql + lngCuentaConBanco + "," ;        //'*PAP-CAREJ01/550-0063
			        sSql = sSql + vGuiaDet.iNumSubCta + "," ;
			        sSql = sSql + vGuiaDet.iNum2SubCta + "," ;
			        sSql = sSql + vGuiaDet.iNum3SubCta + "," ;
			        sSql = sSql + vGuiaDet.iNum4SubCta + "," ;
			        sSql = sSql + vGuiaDet.iNumMoneda + "," ;
			        sSql = sSql + vGuiaDet.lAux1 + "," ;
			        sSql = sSql + vGuiaDet.lAux2 + "," ;
			        sSql = sSql + vGuiaDet.sAux3 + "," ;
			        sSql = sSql + "0," ;
			        sSql = sSql + vGuiaDet.dValor + "," ;
			        if (sMesAnt != "H") {
			           sSql = sSql + "0," ;
			           sSql = sSql + "0," ;
			           sSql = sSql + vGuiaDet.dValor + "," ;
			        }
			        sSql = sSql + "0," ;
			        sSql = sSql + vGuiaDet.dValor + "," ;
			        sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;
			        sSql = sSql + "1,0," ;
			        if (sMesAnt != "H") {
			           sSql = sSql + "1,0)" ;
			        }
			        else {
			           sSql = sSql + Year(vGuiaDet.dtFechaMov) + "," ;
			           sSql = sSql + "13)" ;
			        }
			    }
			    else {
			    	sSql = " UPDATE FDSALDOS" + sMesAnt ;
	                sSql = sSql + " SET    SALD_CARGOS_MES   = SALD_CARGOS_MES + " + vGuiaDet.dValor + "," ;
	                sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + "," ;
	                sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;
	                sSql = sSql + "        SALD_NCARGOS_MES  = SALD_NCARGOS_MES + 1" ;
	                if (sMesAnt != "H") {
	                  sSql = sSql + "        , SALD_NCARGOS_EJER = SALD_NCARGOS_EJER + 1" ;
	                  sSql = sSql + "        , SALD_CARGOS_EJER  = SALD_CARGOS_EJER + " +vGuiaDet.dValor ;
	                }
	                //'sSql = sSql + " WHERE  CCON_CTA   = " +vGuiaDet.iNumCta ;         '*PAP-CAREJ01/550-0064
	                sSql = sSql + " WHERE  CCON_CTA   = " +lngCuentaConBanco ;         //'*PAP-CAREJ01/550-0065
	                sSql = sSql + " AND    CCON_SCTA  = " +vGuiaDet.iNumSubCta ;
	                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;
	                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;
	                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;
	                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;
	                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;
	                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;
	                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;
	                if (sMesAnt == "H") {
	                  sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;
	                  sSql = sSql + " AND  SALD_MES = 13 " ;
	                }
			    }
	    	
			    oAccesoBD = new IfiduBaseDatos.BaseDatos();
	            //		DoEvents
	            bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
	            //		DoEvents
	            if (oAccesoBD != null) { oAccesoBD = null ; }
	            if (bCorrecto) {
	            	if (rsSaldos != null) { rsSaldos = null ; }
	    		    sMsgError = sMsgError + "\n" + 
	    		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
	                return false ;
	            }
	            //'****************************INICIO SUCONTRATO************************** '* DSA-FII/454-00536
	            if (vGuiaDet.lAux2 > 0) {//'* DSA-FII/454-00537
		            PA_SALD_AX2 = vGuiaDet.lAux2 ; // '* DSA-FII/454-00538
		            vGuiaDet.lAux2 = 0 ; //'* DSA-FII/454-00539
		            sSql = " SELECT CCON_CTA" ; //'* DSA-FII/454-00540
		            sSql = sSql + " FROM   FDSALDOS" + sMesAnt ; //'* DSA-FII/454-00541
		            //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) ; //'* DSA-FII/454-00542     '*PAP-CAREJ01/550-0066
		            sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ; //   '*PAP-CAREJ01/550-0067
		            sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; // '* DSA-FII/454-00543
		            sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; // '* DSA-FII/454-00544
		            sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; // '* DSA-FII/454-00545
		            sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; // '* DSA-FII/454-00546
		            sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; // '* DSA-FII/454-00547
		            sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; // '* DSA-FII/454-00548
		            sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2; // '* DSA-FII/454-00549
		            sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3; // '* DSA-FII/454-00550
	            
		            if (sMesAnt == "H") {//'* DSA-FII/454-00551
	                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov)); // '* DSA-FII/454-00552
	                    sSql = sSql + " AND  SALD_MES = 13 " ; //'* DSA-FII/454-00553
		            }// '* DSA-FII/454-00554
	                oAccesoBD = new IfiduBaseDatos.BaseDatos(); // '* DSA-FII/454-00555
	                //		DoEvents '* DSA-FII/454-00556
	                rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ; //'* DSA-FII/454-00557
	                //		DoEvents '* DSA-FII/454-00558
	                if (sMsgError.length() > 0) {//'* DSA-FII/454-00559
	                	if (oAccesoBD != null) { oAccesoBD = null ; }
	        		    if (rsSaldos != null) { rsSaldos = null ; }
	        		    sMsgError = sMsgError + "\n" + 
	        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
	                    return false ;
	                }
	                if (oAccesoBD != null) { oAccesoBD = null ; } //'* DSA-FII/454-00560
	                vGuiaDet.lAux2 = PA_SALD_AX2 ; //'* DSA-FII/454-00561
	            
	                if (rsSaldos.EOF) {//'* DSA-FII/454-00562
	                   	sSql = " INSERT INTO FDSALDOS" + sMesAnt ; // '* DSA-FII/454-00563
		                sSql = sSql + " VALUES("; // '* DSA-FII/454-00564
		                //'sSql = sSql + vGuiaDet.iNumCta + "," ; //'* DSA-FII/454-00565    ''*PAP-CAREJ01/550-0068
		                sSql = sSql + lngCuentaConBanco + ","; //    '*PAP-CAREJ01/550-0069
		                sSql = sSql + vGuiaDet.iNumSubCta + ","; // '* DSA-FII/454-00566
		                sSql = sSql + vGuiaDet.iNum2SubCta + ","; // '* DSA-FII/454-00567
		                sSql = sSql + vGuiaDet.iNum3SubCta + ","; // '* DSA-FII/454-00568
		                sSql = sSql + vGuiaDet.iNum4SubCta + ","; // '* DSA-FII/454-00569
		                sSql = sSql + vGuiaDet.iNumMoneda + ","; // '* DSA-FII/454-00570
		                sSql = sSql + vGuiaDet.lAux1 + ","; // '* DSA-FII/454-00571
		                sSql = sSql + "0," ; //'* DSA-FII/454-00572
		                sSql = sSql + vGuiaDet.sAux3 + "," ; //'* DSA-FII/454-00573
		                sSql = sSql + "0," ; //'* DSA-FII/454-00574
		                sSql = sSql + vGuiaDet.dValor + "," ; //'* DSA-FII/454-00575
		                
		                if (sMesAnt != "H") {//'* DSA-FII/454-00576
		                    sSql = sSql + "0," ; //'* DSA-FII/454-00577
		                    sSql = sSql + "0," ; //'* DSA-FII/454-00578
		                    sSql = sSql + vGuiaDet.dValor + "," ; //'* DSA-FII/454-00579
		                }// '* DSA-FII/454-00580
		                
		                sSql = sSql + "0," ; //'* DSA-FII/454-00581
                        sSql = sSql + vGuiaDet.dValor + "," ; //'* DSA-FII/454-00582
                        sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ; //'* DSA-FII/454-00583
                        sSql = sSql + "1,0," ; //'* DSA-FII/454-00584
                        if (sMesAnt != "H") {//'* DSA-FII/454-00585
                            sSql = sSql + "1,0)" ; //'* DSA-FII/454-00586
                        }
                        else { //'* DSA-FII/454-00587
                            sSql = sSql + Year(vGuiaDet.dtFechaMov) + "," ; //'* DSA-FII/454-00588
                            sSql = sSql + "13)" ; //'* DSA-FII/454-00589
                        } //'* DSA-FII/454-00590
	                }
	                else {// '* DSA-FII/454-00591
		                sSql = " UPDATE FDSALDOS" + sMesAnt ; //'* DSA-FII/454-00592
		                sSql = sSql + " SET    SALD_CARGOS_MES   = SALD_CARGOS_MES + " + vGuiaDet.dValor + "," ; //'* DSA-FII/454-00593
		                sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + "," ; //'* DSA-FII/454-00594
		                sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ; //'* DSA-FII/454-00595
		                sSql = sSql + "        SALD_NCARGOS_MES  = SALD_NCARGOS_MES + 1" ; //'* DSA-FII/454-00596
		                if (sMesAnt != "H") {//'* DSA-FII/454-00597
		                    sSql = sSql + "        , SALD_NCARGOS_EJER = SALD_NCARGOS_EJER + 1" ; //'* DSA-FII/454-00598
		                    sSql = sSql + "        , SALD_CARGOS_EJER  = SALD_CARGOS_EJER + " + vGuiaDet.dValor ; //'* DSA-FII/454-00599
		                }// '* DSA-FII/454-00600
		                //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta ; //'* DSA-FII/454-00601   '*PAP-CAREJ01/550-0070
		                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco; //'*PAP-CAREJ01/550-0071
		                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; //'* DSA-FII/454-00602
		                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; //'* DSA-FII/454-00603
		                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; //'* DSA-FII/454-00604
		                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; //'* DSA-FII/454-00605
		                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; //'* DSA-FII/454-00606
		                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; //'* DSA-FII/454-00607
		                sSql = sSql + " AND    SALD_AX2 = " + 0 ; //'* DSA-FII/454-00608
		                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3; //'* DSA-FII/454-00609
		                if (sMesAnt == "H") {//'* DSA-FII/454-00610
		                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov); //'* DSA-FII/454-00611
		                    sSql = sSql + " AND  SALD_MES = 13 " ; //'* DSA-FII/454-00612
		                }// '* DSA-FII/454-00613
	                }
	                oAccesoBD = new IfiduBaseDatos.BaseDatos(); //'* DSA-FII/454-00615
                    //		DoEvents '* DSA-FII/454-00616
                    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ; //'* DSA-FII/454-00617
                    //		DoEvents '* DSA-FII/454-00618
                    if (oAccesoBD != null) { oAccesoBD = null ; }//'* DSA-FII/454-00619
                    if (bCorrecto ) {//'* DSA-FII/454-00620
                    	if (rsSaldos != null) { rsSaldos = null ; }
            		    sMsgError = sMsgError + "\n" + 
            		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
                        return false ;
                    }
	            }
	    	//1085
	            if (vGuiaDet.sAux3 > 0) {//'* DSA-FII/454-00624
		            PA_SALD_AX3 = vGuiaDet.sAux3 ;//'* DSA-FII/454-00625
		            vGuiaDet.sAux3 = 0 ;//'* DSA-FII/454-00626
		            sSql = " SELECT CCON_CTA" ;//'* DSA-FII/454-00627
		            sSql = sSql + " FROM   FDSALDOS" + sMesAnt ;//'* DSA-FII/454-00628
		            //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) '* DSA-FII/454-00629   ;//'*PAP-CAREJ01/550-0072
		            sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;//'*PAP-CAREJ01/550-0073
		            sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;//'* DSA-FII/454-00630
		            sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;//'* DSA-FII/454-00631
		            sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;//'* DSA-FII/454-00632
		            sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;//'* DSA-FII/454-00633
		            sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;//'* DSA-FII/454-00634
		            sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;//'* DSA-FII/454-00635
		            sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;//'* DSA-FII/454-00636
		            sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;//'* DSA-FII/454-00637
	            
		            if (sMesAnt == "H") {//'* DSA-FII/454-00638
		                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;//'* DSA-FII/454-00639
		                    sSql = sSql + " AND  SALD_MES = 13 " ;//'* DSA-FII/454-00640
		            }// '* DSA-FII/454-00641
	                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;//'* DSA-FII/454-00642
	                //		DoEvents '* DSA-FII/454-00643
	                rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;//'* DSA-FII/454-00644
	                //		DoEvents '* DSA-FII/454-00645
	                if (sMsgError.length() > 0) {//'* DSA-FII/454-00646
	                	if (oAccesoBD != null) { oAccesoBD = null ; }
	        		    if (rsSaldos != null) { rsSaldos = null ; }
	        		    sMsgError = sMsgError + "\n" + 
	        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
	                    return false ;
	                }
	                if (oAccesoBD != null) { oAccesoBD = null ; }//'* DSA-FII/454-00647
	                vGuiaDet.sAux3 = PA_SALD_AX3 ;//'* DSA-FII/454-00648
		            
	                if (rsSaldos.EOF) {//'* DSA-FII/454-00649
		                sSql = " INSERT INTO FDSALDOS" + sMesAnt ;//'* DSA-FII/454-00650
		                sSql = sSql + " VALUES(" ;//'* DSA-FII/454-00651
		                //'sSql = sSql + vGuiaDet.iNumCta) + "," ;//'* DSA-FII/454-00652   '*PAP-CAREJ01/550-0074
		                sSql = sSql + lngCuentaConBanco + ",";//    '*PAP-CAREJ01/550-0075
		                sSql = sSql + vGuiaDet.iNumSubCta + "," ;//'* DSA-FII/454-00653
		                sSql = sSql + vGuiaDet.iNum2SubCta + "," ;//'* DSA-FII/454-00654
		                sSql = sSql + vGuiaDet.iNum3SubCta + "," ;//'* DSA-FII/454-00655
		                sSql = sSql + vGuiaDet.iNum4SubCta + "," ;//'* DSA-FII/454-00656
		                sSql = sSql + vGuiaDet.iNumMoneda + "," ;//'* DSA-FII/454-00657
		                sSql = sSql + vGuiaDet.lAux1 + "," ;//'* DSA-FII/454-00658
		                sSql = sSql + vGuiaDet.lAux2 + "," ;//'* DSA-FII/454-00659
		                sSql = sSql + "0," ;//'* DSA-FII/454-00660
		                sSql = sSql + "0," ;//'* DSA-FII/454-00661
		                sSql = sSql + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00662
		             
		                if (sMesAnt != "H") {//'* DSA-FII/454-00663
		                    sSql = sSql + "0," ;//'* DSA-FII/454-00664
		                    sSql = sSql + "0," ;//'* DSA-FII/454-00665
		                    sSql = sSql + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00666
		                }// '* DSA-FII/454-00667
		                sSql = sSql + "0," ;//'* DSA-FII/454-00668
		                sSql = sSql + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00669
		                sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;//'* DSA-FII/454-00670
		                sSql = sSql + "1,0," ;//'* DSA-FII/454-00671
		                if (sMesAnt != "H") {//'* DSA-FII/454-00672
		                  sSql = sSql + "1,0)" ;//'* DSA-FII/454-00673
		                }
		                else {// '* DSA-FII/454-00674
		                	sSql = sSql + Year(vGuiaDet.dtFechaMov) + "," ;//'* DSA-FII/454-00675
                            sSql = sSql + "13)" ;//'* DSA-FII/454-00676
		                }  
	                }
	                else {
	                	sSql = " UPDATE FDSALDOS" + sMesAnt ;//'* DSA-FII/454-00679
                        sSql = sSql + " SET    SALD_CARGOS_MES   = SALD_CARGOS_MES + " + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00680
                        sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00681
                        sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;//'* DSA-FII/454-00682
                        sSql = sSql + "        SALD_NCARGOS_MES  = SALD_NCARGOS_MES + 1" ;//'* DSA-FII/454-00683
                        if (sMesAnt != "H") {//'* DSA-FII/454-00684
                            sSql = sSql + "        , SALD_NCARGOS_EJER = SALD_NCARGOS_EJER + 1" ;//'* DSA-FII/454-00685
                            sSql = sSql + "        , SALD_CARGOS_EJER  = SALD_CARGOS_EJER + " + vGuiaDet.dValor ;//'* DSA-FII/454-00686
                        }// '* DSA-FII/454-00687
                        //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) ;//'* DSA-FII/454-00688   '*PAP-CAREJ01/550-0076
                        sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;//   '*PAP-CAREJ01/550-0077
                        sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;//'* DSA-FII/454-00689
                        sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;//'* DSA-FII/454-00690
                        sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;//'* DSA-FII/454-00691
                        sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;//'* DSA-FII/454-00692
                        sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;//'* DSA-FII/454-00693
                        sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;//'* DSA-FII/454-00694
                        sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;//'* DSA-FII/454-00695
                        sSql = sSql + " AND    SALD_AX3 = " + 0 ;//'* DSA-FII/454-00696
                        if (sMesAnt == "H") {//'* DSA-FII/454-00697
                            sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;//'* DSA-FII/454-00698
                            sSql = sSql + " AND  SALD_MES = 13 " ;//'* DSA-FII/454-00699
                        }// '* DSA-FII/454-00700
	                }
	                
	                oAccesoBD = new IfiduBaseDatos.BaseDatos();// '* DSA-FII/454-00702
                    //		DoEvents '* DSA-FII/454-00703
                    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError);// '* DSA-FII/454-00704
                    //		DoEvents '* DSA-FII/454-00705
                    if (oAccesoBD != null) { oAccesoBD = null ; }//'* DSA-FII/454-00706
                    if (!bCorrecto) {//'* DSA-FII/454-00707  
                    	if (rsSaldos != null) { rsSaldos = null ; }
            		    sMsgError = sMsgError + "\n" + 
            		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
                        return false ;
                    }
	            }
	            //'****************************FIN AUXILIAR 3************************** '* DSA-FII/454-00709
	            //'****************************INICIA AMBOS**************************** '* DSA-FII/454-00710
	            
	            if ((vGuiaDet.sAux3 > 0) && (vGuiaDet.lAux2 > 0)){//'* DSA-FII/454-00711
		            PA_SALD_AX3 = vGuiaDet.sAux3 ;//'* DSA-FII/454-00712
		            PA_SALD_AX2 = vGuiaDet.lAux2 ;//'* DSA-FII/454-00713
		            vGuiaDet.sAux3 = 0 ;//'* DSA-FII/454-00714
		            vGuiaDet.lAux2 = 0 ;//'* DSA-FII/454-00715
		            sSql = " SELECT CCON_CTA" ;//'* DSA-FII/454-00716
		            sSql = sSql + " FROM   FDSALDOS" + sMesAnt ;//'* DSA-FII/454-00717
		            //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) ;//'* DSA-FII/454-00718   '*PAP-CAREJ01/550-0078
		            sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;//    '*PAP-CAREJ01/550-0079
		            sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;//'* DSA-FII/454-00719
		            sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;//'* DSA-FII/454-00720
		            sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;//'* DSA-FII/454-00721
		            sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;//'* DSA-FII/454-00722
		            sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;//'* DSA-FII/454-00723
		            sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;//'* DSA-FII/454-00724
		            sSql = sSql + " AND    SALD_AX2 = " + 0 ;//'* DSA-FII/454-00725
		            sSql = sSql + " AND    SALD_AX3 = " + 0 ;//'* DSA-FII/454-00726
		            if(sMesAnt == "H") {// Then '* DSA-FII/454-00727
		                sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;//'* DSA-FII/454-00728
		                sSql = sSql + " AND  SALD_MES = 13 " ;//'* DSA-FII/454-00729
		            }//'* DSA-FII/454-00730
		            oAccesoBD = new IfiduBaseDatos.BaseDatos() ;//'* DSA-FII/454-00731
		            //		DoEvents '* DSA-FII/454-00732
		            rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;//'* DSA-FII/454-00733
		            //		DoEvents '* DSA-FII/454-00734
		            if(sMsgError.length() > 0) {//'* DSA-FII/454-00735
		            	if (oAccesoBD != null) { oAccesoBD = null ; }
		    		    if (rsSaldos != null) { rsSaldos = null ; }
		    		    sMsgError = sMsgError + "\n" + 
		    		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                return false ;
		            }
		            if(oAccesoBD != null) { oAccesoBD = null ; }//'* DSA-FII/454-00736
		            vGuiaDet.sAux3 = PA_SALD_AX3 ;//'* DSA-FII/454-00737
		            vGuiaDet.lAux2 = PA_SALD_AX2 ;//'* DSA-FII/454-00738
		            if(rsSaldos.EOF) {//'* DSA-FII/454-00739
		                sSql = " INSERT INTO FDSALDOS" + sMesAnt ;//'* DSA-FII/454-00740
		                sSql = sSql + " VALUES(" ;//'* DSA-FII/454-00741
		                //'sSql = sSql + vGuiaDet.iNumCta) + "," ;//'* DSA-FII/454-00742   '*PAP-CAREJ01/550-0080
		                sSql = sSql + lngCuentaConBanco + "," ;//   '*PAP-CAREJ01/550-0081
		                sSql = sSql + vGuiaDet.iNumSubCta + "," ;//'* DSA-FII/454-00743
		                sSql = sSql + vGuiaDet.iNum2SubCta + "," ;//'* DSA-FII/454-00744
		                sSql = sSql + vGuiaDet.iNum3SubCta + "," ;//'* DSA-FII/454-00745
		                sSql = sSql + vGuiaDet.iNum4SubCta + "," ;//'* DSA-FII/454-00746
		                sSql = sSql + vGuiaDet.iNumMoneda + "," ;//'* DSA-FII/454-00747
		                sSql = sSql + vGuiaDet.lAux1 + "," ;//'* DSA-FII/454-00748
		                sSql = sSql + "0," ;//'* DSA-FII/454-00749
		                sSql = sSql + "0," ;//'* DSA-FII/454-00750
		                sSql = sSql + "0," ;//'* DSA-FII/454-00751
		                sSql = sSql + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00752
		                if(sMesAnt != "H") {//'* DSA-FII/454-00753
		                    sSql = sSql + "0," ;//'* DSA-FII/454-00754
		                    sSql = sSql + "0," ;//'* DSA-FII/454-00755
		                    sSql = sSql + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00756
		                }//'* DSA-FII/454-00757
		                sSql = sSql + "0," ;//'* DSA-FII/454-00758
		                sSql = sSql + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00759
		                sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;//'* DSA-FII/454-00760
		                sSql = sSql + "1,0," ;//'* DSA-FII/454-00761
		                if(sMesAnt != "H") {//'* DSA-FII/454-00762
		                    sSql = sSql + "1,0)" ;//'* DSA-FII/454-00763
		                }
		                else {// '* DSA-FII/454-00764
		                    sSql = sSql + Year(vGuiaDet.dtFechaMov) + "," ;//'* DSA-FII/454-00765
		                    sSql = sSql + "13)" ;//'* DSA-FII/454-00766
		                }//'* DSA-FII/454-00767
		            }
		            else {//'* DSA-FII/454-00768
		                sSql = " UPDATE FDSALDOS" + sMesAnt ;//'* DSA-FII/454-00769
		                sSql = sSql + " SET    SALD_CARGOS_MES   = SALD_CARGOS_MES + " + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00770
		                sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + "," ;//'* DSA-FII/454-00771
		                sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;//'* DSA-FII/454-00772
		                sSql = sSql + "        SALD_NCARGOS_MES  = SALD_NCARGOS_MES + 1" ;//'* DSA-FII/454-00773
		                if(sMesAnt != "H") {//'* DSA-FII/454-00774
		                    sSql = sSql + "        , SALD_NCARGOS_EJER = SALD_NCARGOS_EJER + 1" ;//'* DSA-FII/454-00775
		                    sSql = sSql + "        , SALD_CARGOS_EJER  = SALD_CARGOS_EJER + " + vGuiaDet.dValor ;//'* DSA-FII/454-00776
		                }//'* DSA-FII/454-00777
		                //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta ;//'* DSA-FII/454-00778   '*PAP-CAREJ01/550-0082
		                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;//   '*PAP-CAREJ01/550-0083
		                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;// '* DSA-FII/454-00779
		                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;// '* DSA-FII/454-00780
		                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;// '* DSA-FII/454-00781
		                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;//'* DSA-FII/454-00782
		                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;//'* DSA-FII/454-00783
		                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;//'* DSA-FII/454-00784
		                sSql = sSql + " AND    SALD_AX2 = " + 0 ;//'* DSA-FII/454-00785
		                sSql = sSql + " AND    SALD_AX3 = " + 0 ;//'* DSA-FII/454-00786
		                if(sMesAnt == "H") {//'* DSA-FII/454-00787
		                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov)) ;//'* DSA-FII/454-00788
		                    sSql = sSql + " AND  SALD_MES = 13 " ;//'* DSA-FII/454-00789
		                }//'* DSA-FII/454-00790
		            }//'* DSA-FII/454-00791
		            oAccesoBD = new IfiduBaseDatos.BaseDatos() ;//'* DSA-FII/454-00792
		            //		DoEvents '* DSA-FII/454-00793
		            bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;//'* DSA-FII/454-00794
		            //		DoEvents '* DSA-FII/454-00795
		            if(oAccesoBD != null) { oAccesoBD = null;}//'* DSA-FII/454-00796
		            if(!bCorrecto) {//'* DSA-FII/454-00797
		            	if (rsSaldos != null) { rsSaldos = null ; }
		    		    sMsgError = sMsgError + "\n" + 
		    		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                return false ;
		            }
	            }//'* DSA-FII/454-00798
	            //'****************************FIN AMBOS**************************** '* DSA-FII/454-00799
	            //'*****************************************************************
	            //'DCTB 20051201
	            
	            if (sMesAnt == "MSA") {
            		sSql = " SELECT CCON_CTA" ;
		            sSql = sSql + " FROM   FDSALDOS" ;
		            //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta)     ;//PAP-CAREJ01/550-0084
		            sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;//PAP-CAREJ01/550-0085
		            sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;
		            sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;
		            sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;
		            sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;
		            sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;
		            sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;
		            sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;
		            sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3;
		            
		            oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
		            //		DoEvents
		            rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
		            //		DoEvents
		            if (oAccesoBD != null) { oAccesoBD = null ; }
		            if (sMsgError.length() > 0) {
		    		    if (rsSaldos != null) { rsSaldos = null ; }
		    		    sMsgError = sMsgError + "\n" + 
		    		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                return false ;
		            }
		    
		            if (rsSaldos.EOF) {
		            	sSql = " INSERT INTO FDSALDOS" ;
		                sSql = sSql + " VALUES(" ;
		                //'sSql = sSql + vGuiaDet.iNumCta) + ","                    ;//PAP-CAREJ01/550-0086
		                sSql = sSql + lngCuentaConBanco + ",";//PAP-CAREJ01/550-0087
		                sSql = sSql + vGuiaDet.iNumSubCta + "," ;
		                sSql = sSql + vGuiaDet.iNum2SubCta + ",";
		                sSql = sSql + vGuiaDet.iNum3SubCta + ",";
		                sSql = sSql + vGuiaDet.iNum4SubCta + ",";
		                sSql = sSql + vGuiaDet.iNumMoneda + ",";
		                sSql = sSql + vGuiaDet.lAux1 + ",";
		                sSql = sSql + vGuiaDet.lAux2 + ",";
		                sSql = sSql + vGuiaDet.sAux3 + ",";
		                sSql = sSql + vGuiaDet.dValor + ",";
		                sSql = sSql + "0,";
		                sSql = sSql + "0,";
		                sSql = sSql + vGuiaDet.dValor + ",";
		                sSql = sSql + "0,";
		                sSql = sSql + "0,";
		                sSql = sSql + vGuiaDet.dValor + ",";
		                sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "'),";
		                sSql = sSql + "0,";
		                sSql = sSql + "0,";
		                sSql = sSql + "0,";
		                sSql = sSql + "0)";
		            }
		            else {
		            	sSql = " UPDATE FDSALDOS" ;//'& sMesAnt         ;// DSA-FII/460-000012
		                sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + vGuiaDet.dValor + ",";
		                sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + ",";
		                //'sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "'),";
		                sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')" ;// RAC FII/136-1893
		                //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta)          ;//PAP-CAREJ01/550-0088
		                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;//PAP-CAREJ01/550-0089
		                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta;
		                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta;
		                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta;
		                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta;
		                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda;
		                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1;
		                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2;
		                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3;
		            }
		            
		            oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
		            //		DoEvents
		            bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError);
		            //		DoEvents
		            if (oAccesoBD != null) { oAccesoBD = null ; }
		            if (!bCorrecto) {
		    		    if (rsSaldos != null) { rsSaldos = null ; }
		    		    sMsgError = sMsgError + "\n" + 
		    		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                return false ;
		            }
		            
                    //'****************************INICIO SUCONTRATO************************** '* DSA-FII/454-00800
		            if (vGuiaDet.lAux2 > 0) {// DSA-FII/454-00801
		                PA_SALD_AX2 = vGuiaDet.lAux2 ;// DSA-FII/454-00802
		                vGuiaDet.lAux2 = 0 ;// DSA-FII/454-00803
		                sSql = " SELECT CCON_CTA" ;// DSA-FII/454-00804
		                sSql = sSql + " FROM   FDSALDOS" ;// DSA-FII/454-00805
		                //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta ;// DSA-FII/454-00806   '*PAP-CAREJ01/550-0090
		                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco    ;//PAP-CAREJ01/550-0091
		                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;// DSA-FII/454-00807
		                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;// DSA-FII/454-00808
		                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;// DSA-FII/454-00809
		                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;// DSA-FII/454-00810
		                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;// DSA-FII/454-00811
		                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;// DSA-FII/454-00812
		                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;// DSA-FII/454-00813
		                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;// DSA-FII/454-00814
		                if (sMesAnt == "H") {//'* DSA-FII/454-00815
		                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;// DSA-FII/454-00816
		                    sSql = sSql + " AND  SALD_MES = 13 " ;// DSA-FII/454-00817
		                }//'* DSA-FII/454-00818
		                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;// DSA-FII/454-00819
		                //		DoEvents '* DSA-FII/454-00820
		                rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;// DSA-FII/454-00821
		                //		DoEvents '* DSA-FII/454-00822
		                if (sMsgError.length() > 0) {// DSA-FII/454-00823
		                	if (oAccesoBD != null) { oAccesoBD = null ; }
		        		    if (rsSaldos != null) { rsSaldos = null ; }
		        		    sMsgError = sMsgError + "\n" + 
		        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                    return false ;
		                }
		                if (oAccesoBD !=null) { oAccesoBD = null; }// DSA-FII/454-00824
		                vGuiaDet.lAux2 = PA_SALD_AX2 ;// DSA-FII/454-00825
		                if (rsSaldos.EOF) {// DSA-FII/454-00826
		                    sSql = " INSERT INTO FDSALDOS" ;// DSA-FII/454-00827
		                    sSql = sSql + " VALUES(" ;// DSA-FII/454-00828
		                    //'sSql = sSql + vGuiaDet.iNumCta) + "," '* DSA-FII/454-00829   '*PAP-CAREJ01/550-0092
		                    sSql = sSql + lngCuentaConBanco + ","    ;//PAP-CAREJ01/550-0093
		                    sSql = sSql + vGuiaDet.iNumSubCta + "," ;// DSA-FII/454-00830
		                    sSql = sSql + vGuiaDet.iNum2SubCta + "," ;// DSA-FII/454-00831
		                    sSql = sSql + vGuiaDet.iNum3SubCta + "," ;// DSA-FII/454-00832
		                    sSql = sSql + vGuiaDet.iNum4SubCta + "," ;// DSA-FII/454-00833
		                    sSql = sSql + vGuiaDet.iNumMoneda + "," ;// DSA-FII/454-00834
		                    sSql = sSql + vGuiaDet.lAux1 + "," ;// DSA-FII/454-00835
		                    sSql = sSql + "0," ;// DSA-FII/454-00836
		                    sSql = sSql + vGuiaDet.sAux3 + "," ;// DSA-FII/454-00837
		                    sSql = sSql + vGuiaDet.dValor + "," ;// DSA-FII/454-00838
		                    sSql = sSql + "0," ;// DSA-FII/454-00839
		                    sSql = sSql + "0," ;// DSA-FII/454-00840
		                    sSql = sSql + vGuiaDet.dValor + "," ;// DSA-FII/454-00841
		                    sSql = sSql + "0," ;// DSA-FII/454-00842
		                    sSql = sSql + "0," ;// DSA-FII/454-00843
		                    sSql = sSql + vGuiaDet.dValor + "," ;// DSA-FII/454-00844
		                    sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;// DSA-FII/454-00845
		                    sSql = sSql + "0," ;// DSA-FII/454-00846
		                    sSql = sSql + "0," ;// DSA-FII/454-00847
		                    sSql = sSql + "0," ;// DSA-FII/454-00848
		                    sSql = sSql + "0)" ;// DSA-FII/454-00849
		                }
		                else {//'* DSA-FII/454-00850
		                    sSql = " UPDATE FDSALDOS" ;//'& sMesAnt '* DSA-FII/454-00851   ;// DSA-FII/460-000013
		                    sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + vGuiaDet.dValor + "," ;// DSA-FII/454-00852
		                    sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + "," ;// DSA-FII/454-00853
		                    //'sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;// DSA-FII/454-00854
		                    sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')" ;// RAC FII/136-1893 '* DSA-FII/454-00855
		                    //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta '* DSA-FII/454-00856   ''*PAP-CAREJ01/550-0094
		                    sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco    ;//PAP-CAREJ01/550-0095
		                    sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;// DSA-FII/454-00857
		                    sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;// DSA-FII/454-00858
		                    sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;// DSA-FII/454-00859
		                    sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;// DSA-FII/454-00860
		                    sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;// DSA-FII/454-00861
		                    sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;// DSA-FII/454-00862
		                    sSql = sSql + " AND    SALD_AX2 = " + 0 ;// DSA-FII/454-00863
		                    sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;// DSA-FII/454-00864
		            	}//'* DSA-FII/454-00865
		                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;// DSA-FII/454-00866
		                //		DoEvents '* DSA-FII/454-00867
		                bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;// DSA-FII/454-00868
		                //		DoEvents '* DSA-FII/454-00869
		                if (oAccesoBD != null) { oAccesoBD = null ;}// DSA-FII/454-00870
		                if (!bCorrecto) {// DSA-FII/454-00871
		        		    if (rsSaldos != null) { rsSaldos = null ; }
		        		    sMsgError = sMsgError + "\n" + 
		        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                    return false ;
		                }
		            }//'* DSA-FII/454-00872
//		        '****************************FIN SUCONTRATO************************** '* DSA-FII/454-00873
//		         '* DSA-FII/454-00874
//		        '****************************INICIO AUXILIAR 3************************** '* DSA-FII/454-00875
		            if (vGuiaDet.sAux3 > 0) {//'* DSA-FII/454-00876
		                PA_SALD_AX3 = vGuiaDet.sAux3 ;// DSA-FII/454-00877
		                vGuiaDet.sAux3 = 0 ;// DSA-FII/454-00878
		                sSql = " SELECT CCON_CTA" ;// DSA-FII/454-00879
		                sSql = sSql + " FROM   FDSALDOS" ;// DSA-FII/454-00880
		                //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta '* DSA-FII/454-00881   '*PAP-CAREJ01/550-0096
		                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco ;//PAP-CAREJ01/550-0097
		                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;// DSA-FII/454-00882
		                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;// DSA-FII/454-00883
		                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;// DSA-FII/454-00884
		                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;// DSA-FII/454-00885
		                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;// DSA-FII/454-00886
		                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;// DSA-FII/454-00887
		                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;// DSA-FII/454-00888
		                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;// DSA-FII/454-00889
		                if (sMesAnt == "H") {// DSA-FII/454-00890
		                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;// DSA-FII/454-00891
		                    sSql = sSql + " AND  SALD_MES = 13 " ;// DSA-FII/454-00892
		                }//'* DSA-FII/454-00893
		                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;// DSA-FII/454-00894
		                //		DoEvents '* DSA-FII/454-00895
		                rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;// DSA-FII/454-00896
		                //		DoEvents '* DSA-FII/454-00897
		                if (sMsgError.length() > 0 ) {// DSA-FII/454-00898
		                	if (oAccesoBD != null) { oAccesoBD = null ; }
		        		    if (rsSaldos != null) { rsSaldos = null ; }
		        		    sMsgError = sMsgError + "\n" + 
		        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                    return false ;	
		                }
		                if (oAccesoBD != null) { oAccesoBD = null ; }// DSA-FII/454-00899
		                vGuiaDet.sAux3 = PA_SALD_AX3 ;// DSA-FII/454-00900
		                if (rsSaldos.EOF) {//'* DSA-FII/454-00901
		                    sSql = " INSERT INTO FDSALDOS" ;// DSA-FII/454-00902
		                    sSql = sSql + " VALUES(" ;// DSA-FII/454-00903
		                    //'sSql = sSql + vGuiaDet.iNumCta + "," '* DSA-FII/454-00904   '*PAP-CAREJ01/550-0098
		                    sSql = sSql + lngCuentaConBanco + ","    ;//PAP-CAREJ01/550-0099
		                    sSql = sSql + vGuiaDet.iNumSubCta + "," ;// DSA-FII/454-00905
		                    sSql = sSql + vGuiaDet.iNum2SubCta + "," ;// DSA-FII/454-00906
		                    sSql = sSql + vGuiaDet.iNum3SubCta + "," ;// DSA-FII/454-00907
		                    sSql = sSql + vGuiaDet.iNum4SubCta + "," ;// DSA-FII/454-00908
		                    sSql = sSql + vGuiaDet.iNumMoneda + "," ;// DSA-FII/454-00909
		                    sSql = sSql + vGuiaDet.lAux1 + "," ;// DSA-FII/454-00910
		                    sSql = sSql + vGuiaDet.lAux2 + "," ;// DSA-FII/454-00911
		                    sSql = sSql + "0," ;// DSA-FII/454-00912
		                    sSql = sSql + vGuiaDet.dValor + "," ;// DSA-FII/454-00913
		                    sSql = sSql + "0," ;// DSA-FII/454-00914
		                    sSql = sSql + "0," ;// DSA-FII/454-00915
		                    sSql = sSql + vGuiaDet.dValor + "," ;// DSA-FII/454-00916
		                    sSql = sSql + "0," ;// DSA-FII/454-00917
		                    sSql = sSql + "0," ;// DSA-FII/454-00918
		                    sSql = sSql + vGuiaDet.dValor + "," ;// DSA-FII/454-00919
		                    sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;// DSA-FII/454-00920
		                    sSql = sSql + "0," ;// DSA-FII/454-00921
		                    sSql = sSql + "0," ;// DSA-FII/454-00922
		                    sSql = sSql + "0," ;// DSA-FII/454-00923
		                    sSql = sSql + "0)" ;// DSA-FII/454-00924
		                }
		                else {// '* DSA-FII/454-00925
		                    sSql = " UPDATE FDSALDOS" ; //'& sMesAnt ;// DSA-FII/454-00926       '* DSA-FII/460-000014
		                    sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + vGuiaDet.dValor + "," ;// DSA-FII/454-00927
		                    sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + "," ;// DSA-FII/454-00928
		                    //'sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," ;// DSA-FII/454-00929
		                    sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')" ;// RAC FII/136-1893 '* DSA-FII/454-00930
		                    //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta) '* DSA-FII/454-00931   '*PAP-CAREJ01/550-0100
		                    sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco  ;//PAP-CAREJ01/550-0101
		                    sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;// DSA-FII/454-00932
		                    sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;// DSA-FII/454-00933
		                    sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;// DSA-FII/454-00934
		                    sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;// DSA-FII/454-00935
		                    sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;// DSA-FII/454-00936
		                    sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;// DSA-FII/454-00937
		                    sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;// DSA-FII/454-00938
		                    sSql = sSql + " AND    SALD_AX3 = " + 0 ;// DSA-FII/454-00939
		                }//'* DSA-FII/454-00940
		                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;// DSA-FII/454-00941
		                //		DoEvents '* DSA-FII/454-00942
		                bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;// DSA-FII/454-00943
		                //		DoEvents '* DSA-FII/454-00944
		                if (oAccesoBD != null ) { oAccesoBD = null ; }// DSA-FII/454-00945
		                if (!bCorrecto) {//'* DSA-FII/454-00946
		        		    if (rsSaldos != null) { rsSaldos = null ; }
		        		    sMsgError = sMsgError + "\n" + 
		        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                    return false ;
		                }
		            }//'* DSA-FII/454-00947
//		        '****************************FIN AUXILIAR 3************************** '* DSA-FII/454-00948
//		         '* DSA-FII/454-00949
//		        '****************************INICIO AMBOS************************** '* DSA-FII/454-00950
		            if ((vGuiaDet.sAux3 > 0) && (vGuiaDet.lAux2 > 0)) {// DSA-FII/454-00951
		                PA_SALD_AX3 = vGuiaDet.sAux3 ;// DSA-FII/454-00952
		                PA_SALD_AX2 = vGuiaDet.lAux2 ;// DSA-FII/454-00953
		                vGuiaDet.sAux3 = 0 ;// DSA-FII/454-00954
		                vGuiaDet.lAux2 = 0 ;// DSA-FII/454-00955
		                sSql = " SELECT CCON_CTA" ;// DSA-FII/454-00956
		                sSql = sSql + " FROM   FDSALDOS" ;// DSA-FII/454-00957
		                //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta '* DSA-FII/454-00958   '*PAP-CAREJ01/550-0102
		                sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco  ;//PAP-CAREJ01/550-0103
		                sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta ;// DSA-FII/454-00959
		                sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta ;// DSA-FII/454-00960
		                sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta ;// DSA-FII/454-00961
		                sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta ;// DSA-FII/454-00962
		                sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda ;// DSA-FII/454-00963
		                sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1 ;// DSA-FII/454-00964
		                sSql = sSql + " AND    SALD_AX2 = " + vGuiaDet.lAux2 ;// DSA-FII/454-00965
		                sSql = sSql + " AND    SALD_AX3 = " + vGuiaDet.sAux3 ;// DSA-FII/454-00966
		                if (sMesAnt == "H") {//'* DSA-FII/454-00967
		                    sSql = sSql + " AND  SALD_ANO = " + Year(vGuiaDet.dtFechaMov) ;// DSA-FII/454-00968
		                    sSql = sSql + " AND  SALD_MES = 13 " ;// DSA-FII/454-00969
		                }//'* DSA-FII/454-00970
		                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;// DSA-FII/454-00971
		                //		DoEvents '* DSA-FII/454-00972
		                rsSaldos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;// DSA-FII/454-00973
		                //		DoEvents '* DSA-FII/454-00974
		                if (sMsgError.length() > 0) {// DSA-FII/454-00975
		                	if (oAccesoBD != null) { oAccesoBD = null ; }
		        		    if (rsSaldos != null) { rsSaldos = null ; }
		        		    sMsgError = sMsgError + "\n" + 
		        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                    return false ;
		                }
		                if (oAccesoBD != null) { oAccesoBD = null ;}// DSA-FII/454-00976
		                vGuiaDet.sAux3 = PA_SALD_AX3 ;// DSA-FII/454-00977
		                vGuiaDet.lAux2 = PA_SALD_AX2 ;// DSA-FII/454-00978
		                if (rsSaldos.EOF) {// DSA-FII/454-00979
		                    sSql = " INSERT INTO FDSALDOS" ;// DSA-FII/454-00980
		                    sSql = sSql + " VALUES(" ;// DSA-FII/454-00981
		                    //'sSql = sSql + vGuiaDet.iNumCta) + "," '* DSA-FII/454-00982   '*PAP-CAREJ01/550-0104
		                    sSql = sSql + lngCuentaConBanco + ",";    //PAP-CAREJ01/550-0105
		                    sSql = sSql + vGuiaDet.iNumSubCta + ","; // DSA-FII/454-00983
		                    sSql = sSql + vGuiaDet.iNum2SubCta + ","; // DSA-FII/454-00984
		                    sSql = sSql + vGuiaDet.iNum3SubCta + ","; // DSA-FII/454-00985
		                    sSql = sSql + vGuiaDet.iNum4SubCta + ","; // DSA-FII/454-00986
		                    sSql = sSql + vGuiaDet.iNumMoneda + ","; // DSA-FII/454-00987
		                    sSql = sSql + vGuiaDet.lAux1 + ","; // DSA-FII/454-00988
		                    sSql = sSql + "0,"; // DSA-FII/454-00989
		                    sSql = sSql + "0,"; // DSA-FII/454-00990
		                    sSql = sSql + vGuiaDet.dValor + ","; // DSA-FII/454-00991
		                    sSql = sSql + "0,"; // DSA-FII/454-00992
		                    sSql = sSql + "0,"; // DSA-FII/454-00993
		                    sSql = sSql + vGuiaDet.dValor + ","; // DSA-FII/454-00994
		                    sSql = sSql + "0," ;// DSA-FII/454-00995
		                    sSql = sSql + "0,"; // DSA-FII/454-00996
		                    sSql = sSql + vGuiaDet.dValor + "," ;// DSA-FII/454-00997
		                    sSql = sSql + "DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "'),"; // DSA-FII/454-00998
		                    sSql = sSql + "0,"; // DSA-FII/454-00999
		                    sSql = sSql + "0,"; // DSA-FII/454-001000
		                    sSql = sSql + "0,"; // DSA-FII/454-001001
		                    sSql = sSql + "0)" ;// DSA-FII/454-001002
		                }
		                else { // DSA-FII/454-001003
		                    sSql = " UPDATE FDSALDOS" ;//'& sMesAnt // DSA-FII/454-001004  // DSA-FII/460-000015
		                    sSql = sSql + " SET    SALD_INICIO_MES   = SALD_INICIO_MES + " + vGuiaDet.dValor + ","; // DSA-FII/454-001005
		                    sSql = sSql + "        SALD_SALDO_ACTUAL = SALD_SALDO_ACTUAL + " + vGuiaDet.dValor + ","; // DSA-FII/454-001006
		    				//'sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')," // DSA-FII/454-001007
		                    sSql = sSql + "        SALD_ULTMOD       = DATE('" + Format(vGuiaDet.dtFechaMov, "yyyy-mm-dd") + "')"; // RAC FII/136-1893 // DSA-FII/454-001008
		                    //'sSql = sSql + " WHERE  CCON_CTA   = " + vGuiaDet.iNumCta; // DSA-FII/454-001009   //PAP-CAREJ01/550-0106
		                    sSql = sSql + " WHERE  CCON_CTA   = " + lngCuentaConBanco;    //PAP-CAREJ01/550-0107
		                    sSql = sSql + " AND    CCON_SCTA  = " + vGuiaDet.iNumSubCta; // DSA-FII/454-001010
		                    sSql = sSql + " AND    CCON_2SCTA = " + vGuiaDet.iNum2SubCta; // DSA-FII/454-001011
		                    sSql = sSql + " AND    CCON_3SCTA = " + vGuiaDet.iNum3SubCta; // DSA-FII/454-001012
		                    sSql = sSql + " AND    CCON_4SCTA = " + vGuiaDet.iNum4SubCta; // DSA-FII/454-001013
		                    sSql = sSql + " AND    MONE_ID_MONEDA = " + vGuiaDet.iNumMoneda; // DSA-FII/454-001014
		                    sSql = sSql + " AND    SALD_AX1 = " + vGuiaDet.lAux1; // DSA-FII/454-001015
		                    sSql = sSql + " AND    SALD_AX2 = " + 0 ;// DSA-FII/454-001016
		                    sSql = sSql + " AND    SALD_AX3 = " + 0; // DSA-FII/454-001017
		                }// DSA-FII/454-001018
		                oAccesoBD = new IfiduBaseDatos.BaseDatos() ; // DSA-FII/454-001019
		                //		DoEvents '* DSA-FII/454-001020
		                bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;// DSA-FII/454-001021
		                //		DoEvents '* DSA-FII/454-001022
		                if (oAccesoBD != null) { oAccesoBD = null ; }// DSA-FII/454-001023
		                if (!bCorrecto) {// DSA-FII/454-001024
		        		    if (rsSaldos != null) { rsSaldos = null ; }
		        		    sMsgError = sMsgError + "\n" + 
		        		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
		                    return false ;
		                }
		            } // DSA-FII/454-001025
		        //***************************FIN AMBOS************************** '* DSA-FII/454-001026
	            }
//	            If sMesAnt = "" Then
//		'            bIndicador = False
//		'        Else
//		'          If sMesAnt = "H" Then
//		'            bIndicador = False
//		'          Else
//		'            sMesAnt = ""
//		'          End If
//		'        End If
//		        bIndicador = False
//		        'DCTB 20051201
//		        '******************************************************************
		        rsSaldos.Close ;
	    	}
		    if (rsSaldos != null) { rsSaldos = null ; }
		    
		    if (sMesAnt == "H") { sMsgError = "Mes13" ;}
	    	
	    }catch(Exception ex) {
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (rsSaldos != null) { rsSaldos = null ; }
		    sMsgError = sMsgError + "\n" + 
		                "Fuente : " + m_sObjectName + ".EscalaDatosCargo" ;
            return false ;
	    }
	    
		return true;
	}

	@Override
	public boolean GenAplicaOperacionSrv(DatosFiso vFiso, SubFiso vSFiso, DetMovimiento vDetMov,
			DatosMovimiento[] vDatosMov, long lNumFolioCont, Cliente vCliente, String sMsgServ, String sMsgError,
			/*Optional*/boolean bConSuger) {
		// TODO Auto-generated method stub
		
		
		
		return false;
	}

	@Override
	public boolean GeneraDetMov(DetMovimiento vDetMov, DatosMovimiento[] vDatosMov, String sMsgError) {
		// TODO Auto-generated method stub
		
	    String sSql ;
	    boolean bError ;
	    IfiduBaseDatos.IBaseDatos oAccesoBD ; 
	    
	    SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	    
	    try {
	    	
	    	sSql = "INSERT INTO FDDETMOV VALUES (" ;
		    sSql = sSql + vDetMov.getMvarlNumFolio() + "," ;
		    sSql = sSql + "'" + vDetMov.getMvarsNumOperacion().trim() + "'," ;
		    sSql = sSql + "DATE('" + formateador.format(vDetMov.getMvardtFechaMovimiento()) + "')," ;
		    sSql = sSql + "CURRENT TIMESTAMP," ;
		    sSql = sSql + "'" + vDetMov.getMvarsStatus() + "')" ;
		    
		    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
    	    //		DoEvents
    	    bError = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
    	    //		DoEvents
    	    if (oAccesoBD != null) { oAccesoBD = null ; }
    	    if (bError == false) {//Then GoTo GeneraDetMovErr
    	    	if (oAccesoBD != null) { oAccesoBD = null ; }
    		    sMsgError = sMsgError + "\n" + 
    		                "Fuente : " + m_sObjectName + ".GeneraDetMov" ;
                return false ;
    	    }
    	    
    	    for (int i = 0; i < vDetMov.getMvariNumDatos() - 1; i++) {
				bError = GeneraDatosMov(vDetMov, vDatosMov, i, sMsgError) ;
				if (bError == false) {// Then GoTo GeneraDetMovErr
					if (oAccesoBD != null) { oAccesoBD = null ; }
				    sMsgError = sMsgError + "\n" + 
				                "Fuente : " + m_sObjectName + ".GeneraDetMov" ;
		            return false ;
				}
    	    }
	    	
	    }catch(Exception ex) {
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    sMsgError = sMsgError + "\n" + 
		                "Fuente : " + m_sObjectName + ".GeneraDetMov" ;
            return false ;
	    }
		
		return true;
	}

	@Override
	public boolean GrabaMovimiento(DatosFiso vDatosFiso, SubFiso vSubFiso, DetMovimiento vDetMov,
			DatosMovimiento[] vDatosMov, Transaccion[] vTransacc, Cliente vCliente, int iNumTransacc, boolean _bMesAnt,
			String sMsgError) {
		// TODO Auto-generated method stub
		
		String sSql ;
		ADODB.Recordset rsGuia = new ADODB.Recordset() ;
		GuiaDet vGuiaDet = new GuiaDet() ;
	    int iSecuencia ;
	    boolean bCorrecto ;
	    IfiduBaseDatos.IBaseDatos oAccesoBD ;
	    IfiduFechas.IFechas ofecha ;
	    Date fecha_contable ;
		
	    try {
	    	
	    	ofecha = new IfiduFechas.Fechas() ;
		    fecha_contable = ofecha.dtContable ;//Get al objeto ofecha
		    ofecha = null ;

		    sSql = "INSERT INTO fdmovimiento VALUES (" ;
		    sSql = sSql + vDetMov.getMvarlNumFolio() + "," ;
		    sSql = sSql + "'" + vDetMov.getMvarsNumOperacion().trim() + "'," ;
		    sSql = sSql + vTransacc(iNumTransacc).lNumero + "," ;
		    sSql = sSql + "DATE('" + Format(vDetMov.getMvardtFechaMovimiento(), "yyyy-mm-dd") + "')," ;
		    sSql = sSql + vDatosFiso.getMvarlNumFiso() + "," ;
		    sSql = sSql + vSubFiso.getMvariNumero() + "," ;
		    sSql = sSql + vCliente.getMvarlUsuario() + "," ;
		    sSql = sSql + "'" + Left(vTransacc(iNumTransacc).sNombre + vCliente.getMvarsDirWin(), 125) + "'," ;// 'rac
		    //'sSql = sSql + "'" + vTransacc(iNumTransacc).sNombre + "'," ;
		    sSql = sSql + vDetMov.getMvardImporte() + "," ;
		    sSql = sSql + "'" + Format(fecha_contable, "yyyy-mm-dd") + "-00.00.00.000000" + "'," ;
		    sSql = sSql + "'ACTIVO')" ;

		    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
		    //		DoEvents
		    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
		    //		DoEvents
	    	
		    if (oAccesoBD != null) { oAccesoBD = null ; }
    	    if (bCorrecto == false) {
    	    	if (rsGuia != null) { rsGuia = null ; }
    		    sMsgError = sMsgError + "\n" + 
    		                "Fuente : " + m_sObjectName + ".GrabaMovimiento" ;
                return false ;
    	    }
		    
    	    sSql = " SELECT    b.GUID_SECUENCIA," + "\n" ;
    	    sSql = sSql + "           b.CCON_CTA    CTA," + "\n" ;
    	    sSql = sSql + "           b.CCON_SCTA   SCTA," + "\n" ;
    	    sSql = sSql + "           b.CCON_2SCTA  SSCTA," + "\n" ;
    	    sSql = sSql + "           b.CCON_3SCTA  SSSCTA," + "\n" ;
    	    sSql = sSql + "           b.CCON_4SCTA  SSSSCTA," + "\n" ;
    	    sSql = sSql + "           c1.CVE_DESC_CLAVE AX1," + "\n" ;
    	    sSql = sSql + "           c2.CVE_DESC_CLAVE AX2," + "\n" ;
    	    sSql = sSql + "           c3.CVE_DESC_CLAVE AX3," + "\n" ;
    	    sSql = sSql + "           b.GUID_VALOR," + "\n" ;
    	    sSql = sSql + "           b.GUID_TIPO_ASI," + "\n" ;
    	    sSql = sSql + "           b.GUID_DESCRIPCION," + "\n" ;
    	    sSql = sSql + "           a.MONE_ID_MONEDA" + "\n" ;
    	    sSql = sSql + " FROM (((((FDTRANS a INNER JOIN FDGUIADET b " + "\n" ;
    	    sSql = sSql + "           ON a.TRAN_ID_TRAN = b.TRAN_ID_TRAN)" + "\n" ;
    	    sSql = sSql + "          INNER JOIN FDCATCONTABLE c" + "\n" ;
    	    sSql = sSql + "          ON  b.CCON_CTA   = c.CCON_CTA" + "\n" ;
    	    sSql = sSql + "          AND b.CCON_SCTA  = c.CCON_SCTA " + "\n" ;
    	    sSql = sSql + "          AND b.CCON_2SCTA = c.CCON_2SCTA " + "\n" ;
    	    sSql = sSql + "          AND b.CCON_3SCTA = c.CCON_3SCTA " + "\n" ;
    	    sSql = sSql + "          AND b.CCON_4SCTA = c.CCON_4SCTA) " + "\n" ;
    	    sSql = sSql + "         LEFT OUTER JOIN CLAVES c1 " + "\n" ;
    	    sSql = sSql + "         ON  c1.CVE_NUM_CLAVE = 22 " + "\n" ;
    	    sSql = sSql + "         AND  c.CCON_AX1      = c1.CVE_NUM_SEC_CLAVE) " + "\n" ;
    	    sSql = sSql + "        LEFT OUTER JOIN CLAVES c2 " + "\n" ;
    	    sSql = sSql + "        ON  c2.CVE_NUM_CLAVE = 22 " + "\n" ;
    	    sSql = sSql + "        AND  c.CCON_AX2      = c2.CVE_NUM_SEC_CLAVE) " + "\n" ;
    	    sSql = sSql + "       LEFT OUTER JOIN CLAVES c3 " + "\n" ;
    	    sSql = sSql + "       ON  c3.CVE_NUM_CLAVE = 22" + "\n" ;
    	    sSql = sSql + "       AND  c.CCON_AX3      = c3.CVE_NUM_SEC_CLAVE) " + "\n" ;
    	    sSql = sSql + " WHERE a.TRAN_ID_TRAN = " + vTransacc(iNumTransacc).lNumero ;
    	    
    	    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
    	    //		DoEvents
    	    rsGuia = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
    	    //		DoEvents
    	    if (oAccesoBD != null) { oAccesoBD = null ; }
    	    if (sMsgError.length() > 0) { 
    	    	if (rsGuia != null) { rsGuia = null ; }
    		    sMsgError = sMsgError + "\n" + 
    		                "Fuente : " + m_sObjectName + ".GrabaMovimiento" ;
                return false ;
    	    }
    	    
    	    if (rsGuia.RecordCount == 0) {
    	    	iSecuencia = 0 ;
    	    	while (Not rsGuia.EOF) {
    	    		iSecuencia = iSecuencia + 1 ;
//		  '        vGuiaDet.sNomGuia = rsGuia!GUID_DESCRIPCION
    	    		vGuiaDet.sNomGuia = Left(rsGuia!GUID_DESCRIPCION + vCliente.getMvarsDirWin(), 100) ;  //'' rac 2
    	    		vGuiaDet.iNumSecuencia = iSecuencia ;
//		          'vGuiaDet.iNumCta = rsGuia!CTA                    '*PAP-CAREJ01/550-0114
    	    		lngCuentaSinBanco = rsGuia!CTA  ;                   //'*PAP-CAREJ01/550-0115
    	    		EmpacaCuenta(vGuiaDet) ;                       //'*PAP-CAREJ01/550-0116
    	    		vGuiaDet.iNumSubCta = rsGuia!SCTA ;
    	    		vGuiaDet.iNum2SubCta = rsGuia!SSCTA ;
    	    		vGuiaDet.iNum3SubCta = rsGuia!SSSCTA ;
    	    		vGuiaDet.iNum4SubCta = rsGuia!SSSSCTA ;
    	    		vGuiaDet.sTipoAsiento = rsGuia!GUID_TIPO_ASI ;
    	    		vGuiaDet.iNumMoneda = vTransacc(iNumTransacc).iNumMoneda ;
    	    		vGuiaDet.dtFechaMov = vDetMov.dtFechaMovimiento ;
    	    		vGuiaDet.lAux1 = BuscaDato(rsGuia!AX1 + "", vDatosMov, vDetMov.getMvariNumDatos()) ;
    	    		vGuiaDet.lAux2 = BuscaDato(rsGuia!AX2 + "", vDatosMov, vDetMov.getMvariNumDatos()) ;
    	    		vGuiaDet.sAux3 = BuscaDato(rsGuia!AX3 + "", vDatosMov, vDetMov.getMvariNumDatos()) ;
    	    		vGuiaDet.dValor = BuscaDato(Format(vTransacc(iNumTransacc).iNumMoneda, "00") + rsGuia!GUID_VALOR, vDatosMov, vDetMov.getMvariNumDatos()) ;
		          
    	    		bCorrecto = GrabaAsiento(vTransacc, vDatosFiso, vDetMov, vGuiaDet, iNumTransacc, bMesAnt, sMsgError) ;
    	    		if (bCorrecto == false) {
    	    			if (oAccesoBD != null) { oAccesoBD = null ; }
    	    		    if (rsGuia != null) { rsGuia = null ; }
    	    		    sMsgError = sMsgError + "\n" + 
    	    		                "Fuente : " + m_sObjectName + ".GrabaMovimiento" ;
    	                return false ;
    	    		}
		          
    				rsGuia.MoveNext
    	    	}
    	    }
    	    
    	    rsGuia.Close
    		if (rsGuia != null) { rsGuia = null ; }
    	    
	    }catch(Exception ex) {
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (rsGuia != null) { rsGuia = null ; }
		    sMsgError = sMsgError + "\n" + 
		                "Fuente : " + m_sObjectName + ".GrabaMovimiento" ;
            return false ;
	    }
	    
		return true;
	}

	@Override
	public boolean InsertaAsiento(Transaccion[] vTransacc, DetMovimiento vDetMov, GuiaDet vGuiaDet, int iNumTran,
			boolean bMesAnt, String sMsgError) {
		// TODO Auto-generated method stub
		
		fiduConstantes.Constantes oConst ;
		ADODB.Connection oConn = new ADODB.Connection() ;
		ADODB.Command oSPCall = new ADODB.Command() ;
		ADODB.Recordset rsSP_Call = new ADODB.Recordset() ;
		
		try {
			
			ObtieneCuentaOriginal(vGuiaDet) ; 			//'*PAP-CAREJ01/550-0108
	          
		    if (sMsgError == "LIBRO MAYOR") {
            //'------------------------EJECUTA EL PROCEDIMIENTO PARA ACTUALIZAR LIBRO MAYOR
		    	oConst = new fiduConstantes.Constantes() ;
    			oConn.ConnectionString = oConst.GlCadenaConec(2); //Esto seguramente sea la posicion 2 de un array.
    			oConst = null ;

    			oConn.CursorLocation = adUseClient ;
    			oConn.Mode = adModeReadWrite ;
    			oConn.Open ;
    			oSPCall.ActiveConnection = oConn ;
		    
    			//With oSPCall
    			oSPCall.CommandText = UCase("SP_SALDOS_DIARIOS_FOLIO") ;
				oSPCall.CommandType = adCmdStoredProc ;
				oSPCall.Parameters.Refresh ;

				oSPCall.Parameters("P_FOLIO") = vDetMov.lNumFolio ;
				oSPCall.Parameters("S_STATUS") = "ACTIVO" ;
    			//End With
    			oConn.BeginTrans ;
                oSPCall.Execute ;
                oConn.CommitTrans ;
                sMsgError = oSPCall.Parameters("P_MSGERR_OUT").Value ;
                oConn = null ;
                oSPCall = null ;
                return true ;
            //'------------------------	
		    }
		    else {
		    	oConst = CreateObject("fiduConstantes.Constantes") ;
    			oConn.ConnectionString = oConst.GlCadenaConec(2) ; //Esto seguramente sea la posicion 2 de un array.
    			oConst = null ;
		          
    			sMsgError = "" ;
    			oConn.CursorLocation = adUseClient ;
    			oConn.Mode = adModeReadWrite ;
    			oConn.Open ;
    			oSPCall.ActiveConnection = oConn ;
    			
    			//With oSPCall
    			oSPCall.CommandText = "SP_INSERTA_ASIENTO".toUpperCase();      //'Nombre del Procedimiento almacenado
				oSPCall.CommandType = adCmdStoredProc;               //'Tipo de Comando
				oSPCall.Parameters.Refresh ;
				oSPCall.Parameters("PI_FOLIO") = vDetMov.lNumFolio ;
				oSPCall.Parameters("PS_OPERACION") = vDetMov.sNumOperacion ;
				oSPCall.Parameters("PI_NUM_TRANS") = vTransacc(iNumTran).lNumero ;
				oSPCall.Parameters("PI_NUM_SEC") = vGuiaDet.iNumSecuencia ;
				oSPCall.Parameters("PSM_NUM_POLIZA") = vDetMov.lNumPoliza ;
                'oSPCall.Parameters("PI_CCON_CTA") = vGuiaDet.iNumCta  ;          '*PAP-CAREJ01/550-0109
                oSPCall.Parameters("PI_CCON_CTA") = lngCuentaConBanco  ;          '*PAP-CAREJ01/550-0110
                oSPCall.Parameters("PI_CCON_SCTA") = vGuiaDet.iNumSubCta ;
                oSPCall.Parameters("PI_CCON_2SCTA") = vGuiaDet.iNum2SubCta ;
                oSPCall.Parameters("PI_CCON_3SCTA") = vGuiaDet.iNum3SubCta ;
                oSPCall.Parameters("PI_CCON_4SCTA") = vGuiaDet.iNum4SubCta ;
                oSPCall.Parameters("PI_MONE_ID_MONEDA") = vGuiaDet.iNumMoneda ;
                oSPCall.Parameters("PD_SALD_AX1") = vGuiaDet.lAux1 ;
                oSPCall.Parameters("PSM_SALD_AX2") = vGuiaDet.lAux2 ;
                '* RAC FII/Carga  zzz Ini
                if ((vGuiaDet.lAux3 != 0) || (vGuiaDet.sAux3 == ""){ vGuiaDet.sAux3 = vGuiaDet.lAux3 ; }
        		oSPCall.Parameters("PD_SALD_AX3") = vGuiaDet.sAux3 ;
                '* RAC FII/Carga  zzz Fin
        		oSPCall.Parameters("PD_IMPORTE") = vGuiaDet.dValor ;
        		oSPCall.Parameters("PDT_FECHAMOV") = vDetMov.dtFechaMovimiento ;
        		oSPCall.Parameters("PCH_TIPOMOV") = vGuiaDet.sTipoAsiento ;
        		oSPCall.Parameters("PS_NOMGUIA") = vGuiaDet.sNomGuia ;
                if (bMesAnt ) { oSPCall.Parameters("PSM_MES") = 1 ; } 
                else { oSPCall.Parameters("PSM_MES") = 0 ; }
              //End With
                
                oConn.BeginTrans ;
                oSPCall.Execute ;
                oConn.CommitTrans ;
                sMsgError = oSPCall.Parameters("PS_MSGERR_OUT").Value ;
                oConn = null ;
                oSPCall = null ; 
                if sMsgError.trim() != "") { return false ; }
                
		    }
		    
		    
		}catch(Exception ex) {
			
		    sMsgError = sMsgError + "\n" + 
		                "Fuente : " + m_sObjectName + ".InsertaAsiento"
            return false ;
		}
		
		return true ;
	}

	@Override
	public boolean SolicitaFolioPoliza(DatosFiso vDatosFiso, DetMovimiento vDetMov, String sMsgError) {
		// TODO Auto-generated method stub
		
		String sSql ;
		ADODB.Recordset rsFolio = new ADODB.Recordset() ;
		IfiduFechas.IFechas ofecha ;
		boolean bCorrecto ;
		long lAux ;
		IfiduBaseDatos.IBaseDatos oAccesoBD ;
		
		try {
			
			rsFolio = new ADODB.Recordset() ;
		    ofecha = new IfiduFechas.Fechas() ;
		  
		    sSql = " SELECT FOC_NUM_FOLIO," ;
		    sSql = sSql + "        FOC_NUM_FOLIO_MSA" ;
		    sSql = sSql + " FROM   FOLIOCTO " ;
		    sSql = sSql + " WHERE  FOC_NUM_CONTRATO = " + vDatosFiso.getMvarlNumFiso() ;

		    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
		    //		DoEvents
		    rsFolio = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
		    //		DoEvents
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (sMsgError.length() > 0) {
		    	if (ofecha != null) { ofecha = null ; }
			    if (rsFolio != null) { rsFolio = null ; }
			    sMsgError = sMsgError + "\n" +
			                "Fuente : " + m_sObjectName + ".SolicitaFolioPoliza" ;
	            return false ;
		    }
		    
		    if (Not rsFolio.EOF) {
		        lAux = rsFolio!FOC_NUM_FOLIO ;
		        if (Format(vDetMov.dtFechaMovimiento, "MMYYYY") == Format(ofecha.dtUltDiaNatMesAnte, "MMYYYY")) {
		            vDetMov.setMvarlNumPoliza(rsFolio!FOC_NUM_FOLIO_MSA + 1); 
		            sSql = " UPDATE FOLIOCTO" ;
		            sSql = sSql + " SET FOC_NUM_FOLIO_MSA = FOC_NUM_FOLIO_MSA + 1" ;
		        }
		        else {
		            vDetMov.setMvarlNumPoliza(rsFolio!FOC_NUM_FOLIO + 1); 
		            sSql = " UPDATE FOLIOCTO" ;
		            sSql = sSql + " SET FOC_NUM_FOLIO = FOC_NUM_FOLIO + 1" ;
		        }
		        sSql = sSql + " WHERE FOC_NUM_CONTRATO = " + vDatosFiso.getMvarlNumFiso() ;
		    }
		    else {
		    	vDetMov.setMvarlNumPoliza(1);
		    	if (Format(vDetMov.getMvardtFechaMovimiento() , "MMYYYY") == Format(ofecha.dtUltDiaNatMesAnte, "MMYYYY")) {
        			sSql = " INSERT INTO FOLIOCTO VALUES(" ;
        			sSql = sSql + vDatosFiso.getMvarlNumFiso() + "," ;
					sSql = sSql + "0," ;
					sSql = sSql + "'ACTIVO'," ;
					sSql = sSql + vDetMov.getMvarlNumPoliza() + ")" ;
		    	}
		    	else {
            		sSql = "INSERT INTO FOLIOCTO VALUES (" ;
            		sSql = sSql + vDatosFiso.getMvarlNumFiso() + "," ;
            		sSql = sSql + vDetMov.getMvarlNumPoliza() + "," ;
            		sSql = sSql + "'ACTIVO'," ;
        			sSql = sSql + "0)" ;
		    	}
		    }
		    
		    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
    	    //		DoEvents
    	    //clsContabilizador_SolicitaFolioPoliza = oAccesoBD.EjecutaTransaccion(sSql, sMsgError)
    	    //		DoEvents
    	    if (oAccesoBD != null) { oAccesoBD = null ; }
    	    rsFolio.Close
    	    if (ofecha != null) { ofecha = null ; }
    	    if (rsFolio != null) { rsFolio = null ; }
		    
			
		}catch(Exception ex){
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (ofecha != null) { ofecha = null ; }
		    if (rsFolio != null) { rsFolio = null ; }
		    sMsgError = sMsgError + "\n" +
		                "Fuente : " + m_sObjectName + ".SolicitaFolioPoliza" ;
            return false ;
		}
		
		return false;
	}

	@Override
	public void GeneraEjes(long lNumFisoEje, boolean bMesAnt, String sMsgError) {
		// TODO Auto-generated method stub
		
		IfiduBaseDatos.IBaseDatos oAccesoBD ;
		IfiduBitacora.IBitacora oBitacora ;
	    boolean bCorrecto ;
	    String sSql ;
	    ADODB.Recordset rsEje = new ADODB.Recordset() ;
	    ADODB.Recordset rsHijos = new ADODB.Recordset() ;
	    String sMsgBitacora ;
	    
	    try {
	    	
	    	if (lNumFisoEje == 0) {
    			sSql = "SELECT DISTINCT(CTO_NUM_CTO_EJE)" ;
				sSql = sSql + " FROM  CONTRATO" ;
				sSql = sSql + " WHERE CTO_NUM_CTO_EJE != 0 " ;
				sSql = sSql + " ORDER BY CTO_NUM_CTO_EJE" ;
	    	}
	    	else {
            	sSql = "SELECT DISTINCT(CTO_NUM_CTO_EJE)" ;
    			sSql = sSql + " FROM  CONTRATO" ;
    			sSql = sSql + " WHERE CTO_NUM_CTO_EJE = " + lNumFisoEje ;
	    	}
	    	
	    	oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
		    //		DoEvents
		    rsEje = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
		    //		DoEvents
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (sMsgError.length() > 0) {
		    	if (oBitacora!= null) {oBitacora = null ;}
			    if (rsEje != null) {rsEje = null ;}
			    if (rsHijos != null) {rsHijos = null ; }
			    sMsgError = sMsgError + "Fuente : " + m_sObjectName + ".GeneraEjes" ;
		    }
		    
		    while (Not rsEje.EOF) {
		        if (bMesAnt) {
		        	sSql = "DELETE FROM FDSALDOSMSA" ;
		            sSql = sSql + " WHERE SALD_AX1 = " + rsEje("CTO_NUM_CTO_EJE") ;
		        }
			    else {
			    	sSql = "DELETE FROM FDSALDOS" ;
		            sSql = sSql + " WHERE SALD_AX1 = " + rsEje("CTO_NUM_CTO_EJE") ;
			    }
			    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
			    //		DoEvents
			    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
			    //		DoEvents
			    if (oAccesoBD != null) { oAccesoBD = null ; }
			    if (!bCorrecto) {
			    	if (oBitacora!= null) {oBitacora = null ;}
				    if (rsEje != null) {rsEje = null ;}
				    if (rsHijos != null) {rsHijos = null ; }
				    sMsgError = sMsgError + "Fuente : " + m_sObjectName + ".GeneraEjes" ;
			    }
			    
			    sSql = "SELECT DISTINCT(CTO_NUM_CONTRATO)   NFISO" ;
	    	    sSql = sSql + " FROM  CONTRATO" ;
	    	    sSql = sSql + " WHERE CTO_NUM_CTO_EJE  = " + rsEje("CTO_NUM_CTO_EJE") ;
	    	    sSql = sSql + " AND   CTO_NUM_CTO_EJE != CTO_NUM_CONTRATO" ;
	    	    
	    	    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
	    	    //		DoEvents
	    	    rsHijos = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgError) ;
	    	    //		DoEvents
	    	    if (oAccesoBD != null) { oAccesoBD = null ; }
	    	    if (sMsgError.length() > 0) {
	    	    	if (oBitacora!= null) {oBitacora = null ;}
				    if (rsEje != null) {rsEje = null ;}
				    if (rsHijos != null) {rsHijos = null ; }
				    sMsgError = sMsgError + "Fuente : " + m_sObjectName + ".GeneraEjes" ;
	    	    }
			    
	    	    while (rsHijos.EOF) {
		            AcumulaEje(rsEje("CTO_NUM_CTO_EJE"), rsHijos("NFISO"), bMesAnt, sMsgError) ;
		            if (sMsgError.length() > 0) {
		            	if (oBitacora!= null) {oBitacora = null ;}
					    if (rsEje != null) {rsEje = null ;}
					    if (rsHijos != null) {rsHijos = null ; }
					    sMsgError = sMsgError + "Fuente : " + m_sObjectName + ".GeneraEjes" ;
		            }
		            rsHijos.MoveNext
	    	    }
		        rsHijos.Close
		        
		        sMsgBitacora = "Generaci�n de Contrato Eje " + rsEje("CTO_NUM_CTO_EJE") ;
		        oBitacora = new IfiduBitacora.Bitacora() ;
		        //		DoEvents
		        bCorrecto = oBitacora.GeneraBitacora("GeneraEjes", "fiduContabilidad.Contabilizador" + "/" 
		        			+ rsEje("CTO_NUM_CTO_EJE") + "-0", sMsgBitacora, "Maestra", 0) ;
		        //		DoEvents
		        if (bCorrecto == false {
		        	if (oBitacora!= null) {oBitacora = null ;}
				    if (rsEje != null) {rsEje = null ;}
				    if (rsHijos != null) {rsHijos = null ; }
				    sMsgError = sMsgError + "Fuente : " + m_sObjectName + ".GeneraEjes" ;
		        }
		        if (oBitacora != null) { oBitacora = null ; }
		        
		        rsEje.MoveNext
		    }
		    rsEje.Close
		    
	    }catch(Exception ex){
	    	if (oAccesoBD!= null) {oAccesoBD = null ;}
		    if (oBitacora!= null) {oBitacora = null ;}
		    if (rsEje != null) {rsEje = null ;}
		    if (rsHijos != null) {rsHijos = null ; }
		    sMsgError = sMsgError + "\n" + ex.getMessage() + "\n" +
		                "Fuente : " + m_sObjectName + ".GeneraEjes" ;
	    }
		
	}

	//'************* Implementaciones Sin Interfaz ******************'
	//'GeneraDatosMov
	private boolean GeneraDatosMov(DetMovimiento vDetMov, DatosMovimiento vDatosMov[], int iSecuencia, String sMsgError) {
		
		String sSql ;
	    boolean bCorrecto ;
	    IfiduBaseDatos.IBaseDatos oAccesoBD ;

		try {
            sSql = " INSERT INTO FDDATOSMOV" ;
		    sSql = sSql + " VALUES (" ;
		    sSql = sSql + vDetMov.getMvarlNumFolio() + "," ;
		    sSql = sSql + iSecuencia + ",'" ;
		    sSql = sSql + vDatosMov[iSecuencia].getMvarsConcepto() + "','" ;
		    sSql = sSql + vDatosMov[iSecuencia].getMvarvValor() + "')" ;
		    
		    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
		    //		DoEvents
		    bCorrecto = oAccesoBD.EjecutaTransaccion(sSql, sMsgError) ;
		    //		DoEvents
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (!bCorrecto) { return false ; }
		    
		}catch(Exception ex) { return false ; }
		
		return true ;
	}
	
	
	
}
