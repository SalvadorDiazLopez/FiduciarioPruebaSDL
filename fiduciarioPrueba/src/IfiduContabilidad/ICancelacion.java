package IfiduContabilidad;

/*
VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCancelacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'*************************************************************'
'*           Derechos Reservados EFISOFT S.A. 2002           *'
'*************************************************************'
'*  ARCHIVO     : IfiduContabilidad.vbp                      *'
'*  CLASES      : clsCancelacion.cls                         *'
'*                                                           *'
'*  DESCRIPCION : Interfaz de Servicios para el Modulo       *'
'*                de CONTABILIDAD.                           *'
'*                                                           *'
'*  AUTOR       : OAP - Septiembre 2002                      *'
'*************************************************************'
Option Explicit
*/
public interface ICancelacion {

	public boolean CancelaFolio(long lNumFolio, String sMsgErr) ;

	public boolean ReconstruyeSaldos(String sNumFiso, boolean bMesAnt, String sMsgErr) ;
	
}
