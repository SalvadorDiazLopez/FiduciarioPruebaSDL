package IfiduGenerales;

import java.util.Date;

public interface IGenerales {
/*
	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "clsGenerales"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : IfiduGenerales.vbp                         *'
	'*  CLASE       : clsGenerales.cls                           *'
	'*  DESCRIPCION : Interfase del COM fiduGenerales            *'
	'*  AUTOR       : MRHC - Octubre 2002                        *'
	'*************************************************************'
*/
	public boolean VerificaSubContratoSrv(fiduEstrucContables.DatosFiso oFiso, int iSubFiso, boolean bSoloActivo,
	                                       fiduEstrucContables.SubFiso oSubFiso, String sMsgError,
	                                       /*Optional*/boolean bChecBloqueo, /*Optional*/String sTipoBloque,
	                                       /*Optional*/fiduCOMParametros.ICOMParametros objPTransportador) ;  //'* JRN-FII/023-0771

	public void BuscaNumClaves(String sNumClave, String sNombre, int iSubClave) ;

	public boolean VerificaContratoSrv(String pNFiso, boolean pSoloActivo, fiduEstrucContables.DatosFiso opFiso,
	                             String pMess_Err, /*Optional*/boolean pChecBloqueo, /*Optional*/String pTipoBloque, _
	                             /*Optional*/IfiduCOMParametros.ICOMParametros objPTransportador) ;//'* JRN-FII/023-0772
	//End Function   '* JRN-FII/023-0773

	public void BuscaEjecutivoAgenda(Long lContrato, String sTipoEjec, long lNumEjecutiv, 
									String sNomEjecutiv, long lNumUsuario, String sNomUsuario) ;
	
	public String GetNomBenefeci(long lContrato, long lNumPersona) ;

	public String GetNomTercero(long lContrato, long lNumPersona) ;

	public double BuscaIva() ;

	public double TipoCambio(int iMoneda, double dTipoCambioAnt, Date dtFecha) ;

	public String GetNombanco(int iNumBanco) ;

	public boolean LeeCtoInv(long lNumFiso, int iSubFiso, String sOrigen, fiduEstrucContables.Sugerencias oSuger) ;

	public boolean GetDireccion(long lFiso, IfiduEstrucGnrls.persona oPersona, IfiduEstrucGnrls.Direccion oDirecci) ;

	public double GetFactor(String sPeriodicidad) ;

	public void ImporteEnLetras(double dImpEntrada, String sImpLetras, int iMoneda) ;
	
}
