package IfiduGenerales;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fiduCOMParametros.ICOMParametros;
import fiduEstrucContables.DatosFiso;
import fiduEstrucContables.SubFiso;
import fiduEstrucContables.Sugerencias;
import fiduEstrucGnrls.Direccion;
import fiduEstrucGnrls.persona;

public class Generales implements IGenerales {

	public final String sTerminal = "Maestra" ;
	public final long lNumUsuario = 999 ;
	
	@Override
	public boolean VerificaSubContratoSrv(DatosFiso oFiso, int iSubFiso, boolean bSoloActivo, SubFiso oSubFiso,
			String sMsgError, /*Optional*/boolean bChecBloqueo, /*Optional*/String sTipoBloque, /*Optional*/ICOMParametros objPTransportador) {
		// TODO Auto-generated method stub
		
		boolean resultado ;
		String sHayError ;
		ADODB.Recordset rsSubFiso = new ADODB.Recordset() ;
		String sSqlSc ; 
		IfiduBaseDatos.IBaseDatos odb ; //I = cls
		IfiduConstructor.IConstructor oC ; //I = cls
		try {
			resultado = false ;
			sMsgError = "Fideicomiso no v�lido (0)" ;
			
			if (oFiso.getMvarlNumFiso() == 0) {   /*Linea 601 en VISUALBASIC*/        }
			
			else {
				
				sMsgError = "" ;
			    if (iSubFiso == 0) {
			      return true ;
			    }
			    
			    if (!oFiso.isMvarbSubContrato()) {
			      if (iSubFiso == 0) {
			          oSubFiso.setMvariNumero(iSubFiso); 
			          oSubFiso.setMvarsNombre("");
			        resultado = true ;
			      }
			      else {
			        sMsgError = "Este Fideicomiso no puede tener SubFisos" ;
			      }
			      return resultado ;
			    }//Linea 616
			    
			    sSqlSc = " SELECT  SCT_NOM_SUB_CTO FROM SUBCONT " ;
		    	    sSqlSc = sSqlSc + " WHERE SCT_num_contrato = " + oFiso.getMvarlNumFiso() ;
		    	    sSqlSc = sSqlSc + " AND SCT_SUB_CONTRATO = " + iSubFiso ;
		    	    
	    	    if (bSoloActivo) { sSqlSc = sSqlSc + " and SCT_CVE_ST_SUBCONT='ACTIVO'" ; } // LINEA 622
	    	    
	    	    odb = new IfiduBaseDatos.BaseDatos() ;
	    	    rsSubFiso = odb.EjecutaSeleccion(sSqlSc, 0, sHayError) ;
	    	    odb = null ;
	    	    
	    	    if (!rsSubFiso.EOF) {
	    	    	oSubFiso.setMvariNumero(iSubFiso);
	    			oSubFiso.setMvarsNombre(rsSubFiso.Fields("SCT_NOM_SUB_CTO")) ;
	    			//' Si la bandera de checar Bloqueo se encuentra encendida     '   '* JRN-FII/023-0790
	    			if (bChecBloqueo) {
	    				oC = new IfiduConstructor.Constructor() ;
				        // 			DoEvents
		                sSqlSc = " select CTB_ANO_INI_BLOQUE,CTB_MES_INI_BLOQUE," ;
					        sSqlSc = sSqlSc + " CTB_DIA_INI_BLOQUE,CTB_ANO_FIN_BLOQUE,CTB_MES_FIN_BLOQUE," ;
					        sSqlSc = sSqlSc + " CTB_DIA_FIN_BLOQUE,CTB_CVE_ENTRADAS,CTB_CVE_SALIDAS," ;
					        sSqlSc = sSqlSc + " CTB_CVE_INVERSION,(SELECT DATE( RTRIM(CHAR(FCO_ANO_DIA)) || '-' || RTRIM(CHAR(FCO_MES_DIA)) || '-' || RTRIM(CHAR(FCO_DIA_DIA))) FROM FECCONT) AS HOY, CTB_CONCEPTO_BLOQUEO, CTB_CVE_HONORARIOS from CTOBLOQU WHERE " ;//   '* JRN-FII/023-0791
					        sSqlSc = sSqlSc + " ctB_contrato=" +oFiso.getMvarlNumFiso() ; //   '* JRN-FII/023-0792
					        sSqlSc = sSqlSc + " and CTB_SUB_CONTRATO=" +iSubFiso ;
					        sSqlSc = sSqlSc + " and CTB_CVE_ST_CTOBLOQ='ACTIVO'" ;
				        oC = null ;
				        
				        odb = new IfiduBaseDatos.BaseDatos() ;
				        rsSubFiso = odb.EjecutaSeleccion(sSqlSc, 0, sHayError) ;
				        odb = null ;
				        
				        resultado = true ; //LINEA 649
				        
				        if (!rsSubFiso.EOF) {
				        	//' + Cambiar la fecha de fin de bloqueo por fecha de inicio   '* JRN-FII/023-0793
				        	/*Estos Format quizas no sean necesarios, segun que numero devuelva el resultSet,
			    	           * ya que al guardarlo supongo que ya abra sido formateado.
			    	           * sSqlSc = Format(rsSubFiso("CTB_ANO_INI_BLOQUE"), "0000") 
				                     + "-" + Format(rsSubFiso("CTB_MES_INI_BLOQUE"), "00") 
				                     + "-" + Format(rsSubFiso("CTB_DIA_INI_BLOQUE"), "00") ;   //'* JRN-FII/023-0778
					      */
				        	if (IsDate(sSqlSc)) {
				        		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				        		if (rsSubFiso("HOY") >= formatter.parse(sSqlSc)) {  // '* JRN-FII/023-0795
				        			//' Si el usuario tiene Nivel de Staff y la causa de bloqueo es "BAJA DE CONTRATO"    '   '* JRN-FII/023-0796
				                    //' Dejar correr el codigo sin hacer las siguientes validaciones de bloqueo.          '   '* JRN-FII/023-0797
				                    if ((ExtraeNivelUsuario(objPTransportador) != 0) || (rsSubFiso("CTB_CONCEPTO_BLOQUEO").indexOf("BAJA",0) > -1) && (rsSubFiso("CTB_CONCEPTO_BLOQUEO").indexOf("CONTRATO", 0) > -1)) { //'* JRN-FII/023-0478  '   '* DSA-FII/23-00077
				                    	switch(sTipoBloque.toUpperCase()) {
					                    	case "E":{
					                    		if (rsSubFiso("CTB_CVE_ENTRADAS") + "" == -1) {
				                                    sMsgError = "Subfiso bloqueado para Entradas" ;
				                                    resultado = false ;
					                    		}
					                    	}
					                    	case "S":{
					                    		if (rsSubFiso("CTB_CVE_SALIDAS") + "" == -1) {
				                                    sMsgError = "Subfiso bloqueado para Salidas" ;
				                                    resultado = false ;
					                    		}
					                    	}
					                    	case "I":{
					                    		if (rsSubFiso("CTB_CVE_INVERSION") + "" == -1) {
				                                    sMsgError = "Subfiso bloqueado para Inversiones" ;
				                                    resultado = false ;
					                    		}
					                    	}
					                    	/*case "H":{
					                    		if (rsSubFiso("CTB_CVE_HONORARIOS") + "" == -1){        '* DCTB-FII
						'                           sMsgError = "Subfiso bloqueado para Honorarios" ;        '* DCTB-FII
						'                           resultado = false ;            '* DCTB-FII
					'                        	}                                                   '* DCTB-FII
					                      '* DCTB-FII (INICIO)
					                    	}*/ 
					                    	default :{
					                    		if (rsSubFiso("CTB_CVE_HONORARIOS") + "" > -1) {
					                    			if (bolGChecaBqlHono(rsSubFiso("CTB_CVE_HONORARIOS"), sTipoBloque, sMsgError)) {
					                    				sMsgError = "Subfiso bloqueado para " ;
					                    				switch(sTipoBloque.toUpperCase()) {
						                    				case "PARAM":{
						                    					sMsgError = sMsgError + "Parametros de Honorarios" ;
						                    				}
				                                            case "CALC":{
				                                            	sMsgError = sMsgError + "Calculo de Honorarios" ;
				                                            }
				                                            case "BONI":{
				                                            	sMsgError = sMsgError + "Bonificacion de Honorarios" ;
				                                            }
				                                            case "PAGO":{
				                                            	sMsgError = sMsgError + "Pago de Honorarios" ;
				                                            }
				                                            case "CANC":{
				                                            	sMsgError = sMsgError + "Cancelacion de Honorarios" ;
				                                            }
					                    				}
					                    				resultado = false ;
					                    			}
					                    		}
					                    	}
				                    	}
				                    }
				                        
				        		}
				        	}
				        }
				        
	    			}
	    			else{ resultado = true ; }
	    	    }
	    	    else{ sMsgError = "El Subfiso no existe o no est� activo" ; }
	    	    
	    	    rsSubFiso.Close ;	    
			}
		}
		catch(Exception ex) {
			odb = null ;
			oC = null ;
			CallBitacora ("Generales", "VerificaSubContratoSrv" + "/" + oFiso.getMvarlNumFiso() + "-" 
			+ iSubFiso, ex + " " + sMsgError, sTerminal, lNumUsuario) ;
			return false ;
		}
		
		return resultado;
	}

	@Override
	public void BuscaNumClaves(String sNumClave, String sNombre, int iSubClave) {
		// TODO Auto-generated method stub
		
		//'Nombre anterior Busca_num_CLAVES
		String sMsgError ;
		String sSqlClaves ;
		ADODB.Recordset rsClaves new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; //I = cls
		
		try {
			
			iSubClave = 0 ;
					  if (isNumeric(sNumClave)) {
					    if (sNumClave != "" && sNombre != "") {
					      odb = new IfiduBaseDatos.BaseDatos() ;
					      
					      //		DoEvents
					      
		                  sSqlClaves = " select cve_NUM_SEC_clave from claves " ;
					      sSqlClaves = sSqlClaves + " where cve_num_clave = " + sNumClave  ;//Este ultimo "+" tambien estaba en visualBasic
					      sSqlClaves = sSqlClaves + " and cve_DESC_CLAVE = '" + sNombre + "'" ; //Este ultimo "+" tambien estaba en visualBasic
					      
					      rsClaves = odb.EjecutaSeleccion(sSqlClaves, 0, sMsgError) ;
					      //		DoEvents
					      if (!rsClaves.EOF) { 
					        iSubClave = rsClaves("cve_NUM_SEC_CLAVE") ;
					      }
					      
					      rsClaves.Close ;
					      odb = null ;
					    }
					  }
			
		}catch(Exception ex) {
			
			odb = null ;
			//CallBitacora "Generales", "BuscaNumClaves", Err.Description & " " & sMsgError, sTerminal, lNumUsuario
			CallBitacora( "Generales", "BuscaNumClaves", Err.Description + " " + sMsgError, sTerminal, lNumUsuario) ;
			
		}
		
	}

	@Override
	public boolean VerificaContratoSrv(String pNFiso, boolean pSoloActivo, DatosFiso opFiso, String pMess_Err,
			/*Optional*/boolean pChecBloqueo, /*Optional*/String pTipoBloque) {
		// TODO Auto-generated method stub
		
		boolean bEjecuto ;
		String sMsgErr ;
		String sSqlCto ;
		ADODB.Recordset rsCto = new ADODB.Recordset() ;
		IfiduConstructor.IConstructor oConstru ; // I = cls
		IfiduBaseDatos.IBaseDatos odb ; // I = cls
		IfiduBitacora.IBitacora oBita ; // I = cls
		
		try {
			
			oConstru = new IfiduConstructor.Constructor() ;
			// 			DoEvents
			odb = new IfiduBaseDatos.BaseDatos() ;
			// 			DoEvents
			    
			pMess_Err = "" ;
			if (pNFiso != "") {
			  sSqlCto = "select CTO_NUM_CONTRATO,CTO_NOM_CONTRATO,CTO_CVE_ST_CONTRAT,"
			            + "CTO_TIPO_ADMON,CTO_CVE_MON_EXT," 
			            + "CTO_CVE_SUBCTO,CTO_NUM_NIVEL1,CTO_NUM_NIVEL2,CTO_NUM_NIVEL3," 
			            + "CTO_NUM_NIVEL4,CTO_NUM_NIVEL5,t.cve_num_sec_clave TipoNeg," 
			            + "c.cve_num_sec_clave clasif, CTO_CVE_REQ_SORS AbiertoCerrado, " 
			            + "CTO_CVE_EXCLU_30 ImpEsp, CTO_SUB_RAMA ProSubFiso " 
			            + oConstru.DbLJoin("contrato,+claves t#t.cve_num_clave=36 and " 
			            + "CTO_CVE_TIPO_NEG=t.cve_desc_clave,+claves c#c.cve_num_clave=37 and " 
			            + "CTO_CVE_CLAS_PROD=c.cve_desc_clave," 
			            + "cto_num_contrato = " + pNFiso) ;
			//	 			DoEvents
			  
			if (pSoloActivo) { sSqlCto = sSqlCto + " and CTO_CVE_ST_CONTRAT='ACTIVO'" ; }
			//	    DoEvents
			rsCto = odb.EjecutaSeleccion(sSqlCto, 0, sMsgErr) ;
			//	    DoEvents
			
			if (!rsCto.EOF) { 
		      opFiso.setMvarlNumFiso(rsCto("CTO_NUM_CONTRATO"));
		      opFiso.setMvarsNomFiso(rsCto("CTO_NOM_CONTRATO"));
		      if ((rsCto("CTO_TIPO_ADMON") + "").toUpperCase() == "SI") { opFiso.setMvarbAdmonPropia(true); }
		      else { opFiso.setMvarbAdmonPropia(false); }
		      
		      opFiso.setMvariNivelEstructura1(rsCto("CTO_NUM_NIVEL1"));
		      opFiso.setMvariNivelEstructura2(rsCto("CTO_NUM_NIVEL2"));
		      opFiso.setMvariNivelEstructura3(rsCto("CTO_NUM_NIVEL3"));
		      opFiso.setMvariNivelEstructura4(rsCto("CTO_NUM_NIVEL4"));
		      opFiso.setMvariNivelEstructura5(rsCto("CTO_NUM_NIVEL5"));
    	      opFiso.setMvarsStatus(rsCto("CTO_CVE_ST_CONTRAT"));
    	      opFiso.setMvarbSubContrato(false);
    	      
    	      if (rsCto("CTO_CVE_SUBCTO") + "" != "") {
    	        if (rsCto("CTO_CVE_SUBCTO") == 1) { opFiso.setMvarbSubContrato(true); }
    	      }
    	      
    	      opFiso.setMvarbMonedaExtranjera(false);
    	      opFiso.setMvariAbiertoCerrado(rsCto("AbiertoCerrado"));
    	      opFiso.setMvariIVAEspecial(rsCto("ImpEsp"));
    	      
    	      if (rsCto("CTO_CVE_MON_EXT") + "" != "") {
    	        if (rsCto("CTO_CVE_MON_EXT") == 1) { opFiso.setMvarbMonedaExtranjera(true); }
    	      }
    	      /*
    	       * Esto debo preguntarlo puesto que si aqui se hace el return true ya todo lo demas no se hace. 
    	       * //clsGenerales_VerificaContratoSrv = True
    	       */
    	      
    	      
    	      if (rsCto("TipoNeg") + "" != "") { opFiso.setMvariTipoNegocio(rsCto("TipoNeg")); }
    	      else {
    	        pMess_Err = "El Fideicomiso no tiene Tipo de Negocio, notifique al Administrador" ;
    	        opFiso.setMvariTipoNegocio(0);
        		return false ;
    	      }
    	      
    	      if (rsCto("clasif") + "" != ""){ opFiso.setMvariClasifProducto(rsCto("clasif")); }
    	      else {
    	        pMess_Err = "El Fideicomiso no tiene Clasificaci�n, notifique al Administrador" ;
    	        opFiso.setMvariClasifProducto(0);
    	        return false ;
    	      }
    	      
    	      if (rsCto("ProSubFiso") + "" != "") { opFiso.setMvariProrrateaSubFiso(rsCto("ProSubFiso")); }
    	      else { opFiso.setMvariClasifProducto(0); }
    	      
    	      //' Si la bandera de checar Bloqueo se encuentra encendida     '   '* JRN-FII/023-0775
    	      if (pChecBloqueo) {
    	    	  sSqlCto = "SELECT CTB_ANO_INI_BLOQUE,CTB_MES_INI_BLOQUE," 
    	                + "CTB_DIA_INI_BLOQUE,CTB_ANO_FIN_BLOQUE,CTB_MES_FIN_BLOQUE," 
    	                + "CTB_DIA_FIN_BLOQUE,CTB_CVE_ENTRADAS,CTB_CVE_SALIDAS," 
    	                + "CTB_CVE_INVERSION,(SELECT DATE( RTRIM(CHAR(FCO_ANO_DIA)) || '-' || RTRIM(CHAR(FCO_MES_DIA)) || '-' || RTRIM(CHAR(FCO_DIA_DIA))) FROM FECCONT) AS HOY, CTB_CONCEPTO_BLOQUEO, CTB_CVE_HONORARIOS from CTOBLOQU WHERE " 
    	                + "ctB_contrato = " + pNFiso 
    	                + " AND CTB_SUB_CONTRATO = 0 " 
    	                + " AND CTB_CVE_ST_CTOBLOQ = 'ACTIVO'" ;  //'* JRN-FII/023-0776
    	       // 			 DoEvents
	    	       rsCto = odb.EjecutaSeleccion(sSqlCto, 0, sMsgErr) ;
	    	       //	         DoEvents
	    	       if(!rsCto.EOF) {
	    	          //' + Cambiar la fecha de fin de bloqueo por fecha de inicio   '* JRN-FII/023-0777
	    	          /*Estos Format quizas no sean necesarios, segun que numero devuelva el resultSet,
	    	           * ya que al guardarlo supongo que ya abra sido formateado.
	    	           * sSqlCto = Format(rsCto("CTB_ANO_INI_BLOQUE"), "0000") 
	    	                    + "-" + Format(rsCto("CTB_MES_INI_BLOQUE"), "00") 
	    	                    + "-" + Format(rsCto("CTB_DIA_INI_BLOQUE"), "00") ;   //'* JRN-FII/023-0778
			      */
			    	   if (IsDate(sSqlCto)) {
			    		   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		    	            if (rsCto("HOY") >= formatter.parse(sSqlCto)) {//   '* JRN-FII/023-0779
		    	                //' Si el usuario tiene Nivel de Staff y la causa de bloqueo es "BAJA DE CONTRATO"    '   '* JRN-FII/023-0780
		    	                //' Dejar correr el codigo sin hacer las siguientes validaciones de bloqueo.          '   '* JRN-FII/023-0781
		    	                //' If ExtraeNivelUsuario(objPTransportador) <> 0 Or rsCto("CTB_CONCEPTO_BLOQUEO") <> "BAJA CONTRATO" Then  '   '* JRN-FII/023-0782
		    	                if ((ExtraeNivelUsuario(objPTransportador) != 0) || (rsCto("CTB_CONCEPTO_BLOQUEO").indexOf("BAJA", 0) == -1) && (rsCto("CTB_CONCEPTO_BLOQUEO").indexOf("CONTRATO", 0) == -1)){//'* JRN-FII/023-0478  '   '* DSA-FII/23-00076
		    	                    switch (pTipoBloque.toUpperCase()) {
			    	                    case "E" : {
			    	                    	if (rsCto("CTB_CVE_ENTRADAS") + "" == -1) {
			    	                                pMess_Err = "Fideicomiso bloqueado para Entradas" ;
			    	                                return false ;
			    	                    	}
			    	                    }
			    	                    case "S": {
			    	                    	if (rsCto("CTB_CVE_SALIDAS") /*& ""*/ == -1) {
			    	                                pMess_Err = "Fideicomiso bloqueado para Salidas" ;
			    	                                return false ;
			    	                    	}
			    	                    }
			    	                    case "I": {
			    	                    	if (rsCto("CTB_CVE_INVERSION") /*& ""*/ == -1) {
			    	                                pMess_Err = "Fideicomiso bloqueado para Inversiones" ;
			    	                                return false ;
			    	                    	}
			    	                    }
			    	                   /* case "H": {//'* JRN-FII/023-0783 			'* DCTB-FII (FIN)
			    	                    //	if (rsCto("CTB_CVE_HONORARIOS") /*& ""*/// == -1) {//   '* JRN-FII/023-0784             '* DCTB-FII (FIN)
			    	                   // 		pMess_Err = "Fideicomiso bloqueado para Honorarios" ; //   '* JRN-FII/023-0785     '* DCTB-FII (FIN)
			    	                    //		return false ; //  '* JRN-FII/023-0786                '* DCTB-FII (FIN)
			    	                   // 	}//  '* JRN-FII/023-0787                                                    '* DCTB-FII (FIN)
			    	                    	 //                    '* DCTB-FII (FIN)
			    	                    //}*/
			    	                    default : {
			    	                    	if (rsCto("CTB_CVE_HONORARIOS") /*& ""*/ > 0){ 
			                                    if (bolGChecaBqlHono(rsCto("CTB_CVE_HONORARIOS"), pTipoBloque, pMess_Err)) {
			                                        pMess_Err = "Fideicomiso bloqueado para " ;
			                                        switch (pTipoBloque.toUpperCase()) {
			                                            case "PARAM" :{
			                                            	pMess_Err = pMess_Err + "Parametros de Honorarios" ;
			                                            }
			                                            case "CALC" :{
			                                            	pMess_Err = pMess_Err + "Calculo de Honorarios" ;
			                                            }
			                                            case "BONI" :{
			                                            	pMess_Err = pMess_Err + "Bonificacion de Honorarios" ;
			                                            }
			                                            case "PAGO" :{
			                                            	pMess_Err = pMess_Err + "Pago de Honorarios" ;
			                                            }
			                                            case "CANC" :{
			                                            	pMess_Err = pMess_Err + "Cancelacion de Honorarios" ;
			                                            }
			                                        }
			                                        return false ;
		                                    	}
			    	                    	}
			    	                    }
		    	                    }
		    	                }
		    	            }
			    	   }
		    	   }
    	      }
			}
    	      else {
    	    	   pMess_Err = "El Fideicomiso no existe, no est� activo o no le pertenece" ;
    	    	   return false ;
    	      } //'Not rsCto.EOF  
		}
	    else {//'pNFiso <> ""
    	    pMess_Err = "Fideicomiso en blanco" ;
    	    return false ;
	    } //'pNFiso <> ""
		}catch(Exception ex) {
			
			/*
			 * oBita = new IfiduBitacora.Bitacora() ;
			//		  DoEvents
			bEjecuto = oBita.GeneraBitacora("Generales", "BuscaIva", sMsgError + " " + ex, "Maestra", 0) ;
			 * */
			generarBitacora("Generales", "VerificaContratoSrv" + "/" + pNFiso + "-0", "Fiso:" + pNFiso + " " + ex + " " + sMsgErr, sTerminal, lNumUsuario) ;
			oConstru = null ;
			rsCto = null ;
			return false ;
		}
		
		return true ;
	}

	@Override
	public void BuscaEjecutivoAgenda(Long lContrato, String sTipoEjec, long lNumEjecutiv, String sNomEjecutiv,
			long lNumUsuario, String sNomUsuario) {
		// TODO Auto-generated method stub
		
		String SqlEjec ;
		String sMsgErr ; 
		ADODB.Recordset rsEjec = new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; //I = cls
		
		try {
			
			odb = new IfiduBaseDatos.BaseDatos() ;
			
			//		  DoEvents
			
			SqlEjec = "SELECT USU_NOM_USUARIO,USU_NUM_USUARIO FROM FDATENCION JOIN FDUSUARIOS ON ATE_NUM_USUARIO=USU_NUM_USUARIO" ;
			SqlEjec = SqlEjec + " WHERE  ATE_NUM_CONTRATO =" + lContrato.toString() + " AND  ATE_GEN_AGENDA =1 " ;
			SqlEjec = SqlEjec + " AND  USU_TIPO_USUARIO ='" + sTipoEjec + "'" ;
			
			rsEjec = odb.EjecutaSeleccion(SqlEjec, 0, sMsgErr) ;
			
			//		  DoEvents
			  
			  if (!rsEjec.EOF){ 
			    sNomEjecutiv = rsEjec("USU_NOM_USUARIO") + "" ;
			    lNumEjecutiv = rsEjec("USU_NUM_USUARIO") ;
			  }
			  else {
			    sNomEjecutiv = sNomUsuario ;
			    lNumEjecutiv = lNumUsuario ;
			  }
			  
			  //  rsEjec.Close ;
			  odb = null ;
			
		}catch(Exception ex) {
			odb = null ;
		}
		
	}

	@Override
	public String GetNomBenefeci(long lContrato, long lNumPersona) {
		// TODO Auto-generated method stub
		
		String sMsgError ;
		String sSql ; 
		ADODB.Recordset rsBene= new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; // I = cls
		
		try {
			
			sSql = " SELECT BEN_NOM_BENEF FROM BENEFICI " ;
				sSql = sSql + " WHERE BEN_NUM_CONTRATO = " + String.valueOf(lContrato) ; //Este ultimo "+" tambien estaba en visualBasic
				sSql = sSql + " And BEN_BENEFICIARIO = " + String.valueOf(lNumPersona) ; //Este ultimo "+" tambien estaba en visualBasic
			   
			odb = new IfiduBaseDatos.BaseDatos() ;
			// 			DoEvents
			  
			rsBene = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
			// 			DoEvents
			    
			if (!rsBene.EOF) { 
			    return rsBene("BEN_NOM_BENEF") ;
			}
			rsBene.Close ;
			odb = null ;
			
		}catch(Exception ex) {
			
			odb = null ;
			/*
			 * oBita = new IfiduBitacora.Bitacora() ;
			//		  DoEvents
			bEjecuto = oBita.GeneraBitacora("Generales", "BuscaIva", sMsgError + " " + ex, "Maestra", 0) ;
			 * */
			generarBitacora("Generales", "GetNomBenefeci" + "/" + String.valueOf(lContrato) + "-0", ex + " " + sMsgError, sTerminal, lNumUsuario) ;
			
		}
		
		return "";
	}

	@Override
	public String GetNomTercero(long lContrato, long lNumPersona) {
		// TODO Auto-generated method stub
		
		String sMsgError ;
		String sSql ; 
		ADODB.Recordset rsTerce= new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; // I = cls
		
		try {
			
			sSql = " SELECT TER_NOM_TERCERO " ;
				sSql = sSql + " FROM TERCEROS " ;
				sSql = sSql + " WHERE TER_NUM_CONTRATO = " + String.valueOf(lContrato) ; //Este ultimo "+" tambien estaba en visualBasic
				sSql = sSql + " AND TER_NUM_TERCERO = " + String.valueOf(lNumPersona) ; //Este ultimo "+" tambien estaba en visualBasic
					  
				odb = new IfiduBaseDatos.BaseDatos() ; // Aqu� IfiduBaseDatos(JAVA) = fiduBaseDatos(VISUALBASIC)
				// 			DoEvents
				  
				rsTerce = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
				// 			DoEvents
				    
				if (!rsTerce.EOF) { 
				    return rsTerce("TER_NOM_TERCERO") ;
				}
				rsTerce.Close ;
				odb = null ;
			
		}catch(Exception ex) {

			odb = null ;
			/*
			 * oBita = new IfiduBitacora.Bitacora() ;
			//		  DoEvents
			bEjecuto = oBita.GeneraBitacora("Generales", "BuscaIva", sMsgError + " " + ex, "Maestra", 0) ;
			 * */
			generarBitacora ("Generales", "GetNomTercero" + "/" + String.valueOf(lContrato) + "-0", ex + " " + sMsgError, sTerminal, lNumUsuario) ;

		}
		
		return "";
	}

	@Override
	public double BuscaIva() {
		// TODO Auto-generated method stub
		
		boolean bEjecuto ;
		String sMsgError ;
		String sSql ;
		ADODB.Recordset rsIndice = new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; //I = cls
		IfiduBitacora.IBitacora oBita ; //I = cls
		
		try {
			
			odb = new IfiduBaseDatos.BaseDatos() ;
			//		  DoEvents
			sSql = "SELECT IND_VALOR_INDICE FROM INDICES WHERE IND_CVE_INDICE = 'IVA'" ;
			rsIndice = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
			if (!rsIndice.EOF){
				return rsIndice("IND_VALOR_INDICE") ;
			}
			else{
				return /*Empty*/0 ;
			}
			//rsIndice.Close
			odb = null ;
			
		}catch(Exception ex) {
			
			oBita = new IfiduBitacora.Bitacora() ;
			//		  DoEvents
			bEjecuto = oBita.GeneraBitacora("Generales", "BuscaIva", sMsgError + " " + ex, "Maestra", 0) ;
			oBita = null ;
			odb = null ;
			return /*Empty*/0 ;
			
		}
		
		return 0;
	}

	@Override
	public double TipoCambio(int iMoneda, double dTipoCambioAnt, Date dtFecha) {
		// TODO Auto-generated method stub
		
		IfiduBaseDatos.IBaseDatos odb ;//I = cls
		ADODB.Recordset rsTipoCambio = new ADODB.Recordset () ;
		boolean bEjecuto ;
		String sSql ;
		String sMsgError ;
		
		try {
			
			odb = new fiduBaseDatos.BaseDatos() ;
			// 			DoEvents
			   
			sSql = " SELECT TIC_IMP_TIPO_CAMB " ;
				sSql = sSql + " FROM TIPOCAMB" ;
				sSql = sSql + " WHERE TIC_NUM_PAIS = " + iMoneda ;
			  
			if (dtFecha != null) {
			    sSql = sSql + " AND TIC_ANO_ALTA_REG = " + dtFecha.getYear() ;
			    sSql = sSql + " AND TIC_MES_ALTA_REG = " + dtFecha.getMonth() ;
			    sSql = sSql + " AND TIC_DIA_ALTA_REG = " + dtFecha.getDay() ;
			}
			  
			sSql = sSql + " ORDER BY  TIC_ANO_ALTA_REG DESC ," ;
			sSql = sSql + " TIC_MES_ALTA_REG DESC," ;
			sSql = sSql + " TIC_DIA_ALTA_REG DESC," ;
			sSql = sSql + " TIC_HORA_ALTA DESC ," ;
			sSql = sSql + " TIC_MINUTO_ALTA Desc" ;
			  
			rsTipoCambio = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
			// 			DoEvents
			    
			if (!rsTipoCambio.EOF) {
			    rsTipoCambio.MoveFirst ;
			    return rsTipoCambio.Fields(0)  ; // ESTO DEVE DEVOLVER UN DOUBLE
			}
			else {
			    return dTipoCambioAnt ;
			}
			rsTipoCambio.Close ;
			odb = null ;
			
		}catch(Exception ex) {
			
			odb = null ;
			generarBitacora("Generales", "TipoCambio", Err.Description + " " + sMsgError, sTerminal, lNumUsuario) ;
			
		}
		
		return 0; // 
	}

	@Override
	public String GetNombanco(int iNumBanco) {
		// TODO Auto-generated method stub
		
		boolean bEjecuto ;
		String sMsgError ;
		String sSql ;
		ADODB.Recordset rsBanco = new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; //I = cls
		
		try {
			
			sSql = " SELECT CVE_DESC_CLAVE " ;
				sSql = sSql + " FROM CLAVES " ;
				sSql = sSql + " WHERE CVE_NUM_CLAVE = 27 " ;
				sSql = sSql + " AND CVE_NUM_SEC_CLAVE = " + iNumBanco ;
				
			odb = new fiduBaseDatos.BaseDatos() ;
			//			  DoEvents
			rsBanco = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
			odb = null ;
			
			if (!rsBanco.EOF) { clsGenerales_GetNombanco = rsBanco("CVE_DESC_CLAVE") ; }
		    rsBanco.Close ;
			
		}catch(Exception ex) {
			CallBitacora ("Generales", "GetNombanco", Err.Description + " " + sMsgError, sTerminal, lNumUsuario)
			odb = null ;
		}
		
		return null;
	}

	@Override
	public boolean LeeCtoInv(long lNumFiso, int iSubFiso, String sOrigen, Sugerencias oSuger) {
		// TODO Auto-generated method stub
		
		boolean resultado ;
		boolean bEjecuto ;
		String sMsgError ;
		String sSql ;
		ADODB.Recordset rsCont = new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; // I = cls
		
		try {
			
			sSql = "select CPR_CVE_ORIG_REC, CPR_ENTIDAD_FIN, CPR_CONTRATO_INTER " ;
				sSql = sSql + " FROM CONTINTE WHERE " ;
				sSql = sSql + " CPR_NUM_CONTRATO = " + lNumFiso ;
				sSql = sSql + " and CPR_SUB_CONTRATO = " + iSubFiso ;
				sSql = sSql + " and CPR_CVE_ST_CONTINT = 'ACTIVO'" ;
				
			odb = new IfiduBaseDatos.BaseDatos() ;
			// 			DoEvents
			rsCont = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
			odb = null ;
			
			if (!rsCont.EOF) {
			    oSuger.iIntermediario = rsCont("CPR_ENTIDAD_FIN") ;
			    oSuger.lCtoInv = rsCont("CPR_CONTRATO_INTER") ;
			    sOrigen = rsCont("cpr_cve_orig_rec").trim() ;
			    resultado = true ;
			}
			else {
				sOrigen = "" ;
			    oSuger.iIntermediario = 0 ;
			    oSuger.lCtoInv = 0 ;
			    resultado = false ;
			}
			
			sSql = " SELECT PAR_NUM_ENTID_FIN,PAR_CONTRATO_INTER " ;
			  sSql = sSql + " FROM PARAINVE WHERE " ;
			  sSql = sSql + " PAR_NUM_CONTRATO = " + lNumFiso ;
			  sSql = sSql + " AND PAR_NUM_SUB_CONT = " + iSubFiso ;
			  sSql = sSql + " AND PAR_CVE_CPA_VTA = 'VENTA'" ;
			  sSql = sSql + " AND PAR_CVE_ST_PARAINV = 'ACTIVO'" ;
			  
			odb = new IfiduBaseDatos.BaseDatos() ;
			//		  DoEvents
			rsCont = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
			odb = null ;
			
			if(!rsCont.EOF) {
			    oSuger.iIntermediario = rsCont("PAR_NUM_ENTID_FIN") ;
			    oSuger.lCtoInv = rsCont("PAR_CONTRATO_INTER") ;
			    resultado = true ;
			}
			
		}catch(Exception ex) {
			odb = null ;
			CallBitacora ("Generales", "LeeCtoInv", Err.Description + " " + sMsgError, sTerminal, lNumUsuario) ;
			return false ;
		}
		
		return resultado;
	}

	@Override
	public boolean GetDireccion(long lFiso, persona oPersona, Direccion oDirecci) {
		// TODO Auto-generated method stub
		
		boolean bEjecuto ;
		String sMsgError ;
		String sSql ;
		ADODB.Recordset rsDir= new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos odb ; //I = cls
		
		try {
			
			sSql = " SELECT * FROM DIRECCI " ;
				sSql = sSql + " WHERE  DIR_NUM_CONTRATO " ;
				sSql = sSql + " AND DIR_CVE_PERS_FID " ;
				sSql = sSql + " AND DIR_NUM_PERS_FID " ;
				sSql = sSql + " AND DIR_NUM_SEC_DIRECC = 1" ;
			  
			odb = new IfiduBaseDatos.BaseDatos() ; // Aqu� IfiduBaseDatos(JAVA) = fiduBaseDatos(VISUALBASIC)
			//			DoEvents
			rsDir = odb.EjecutaSeleccion(sSql, 0, sMsgError) ;
			if (!rsDir.EOF) {
				rsDir("") = oDirecci.sCalleyNum ;
			    
				return true ;
			}
			else {
				return false ;
			}
			rsDir.Close ;
			odb = null ;
			
		}catch(Exception ex) {
			
			CallBitacora("Generales", "GetDireccion", Err.Description + " " + sMsgError, sTerminal, lNumUsuario) ;
			odb = null ;
			
		}
		
		return false;
	}

	@Override
	public double GetFactor(String sPeriodicidad) {
		// TODO Auto-generated method stub
		
		double resultado = 0 ;
		
		switch (sPeriodicidad.substring(0, 3)) {
		
			case "DIAR":{ resultado =  1 / 30 ;  }
			
			case "SEMA":{ resultado =  7 / 30 ;  }
			
			case "QUIN":{ resultado = 15 / 30 ;  }
			
			case "MENS":{ resultado =  1 ; 		 }
			
			case "BIME":{ resultado =  2 ; 		 }
			
			case "TRIM":{ resultado =  3 ; 		 }
			
			case "CUAT":{ resultado =  4 ; 		 }
			
			case "SEME":{ resultado =  6 ; 		 }
			
			case "ANUA":{ resultado = 12 ; 		 }
			
			default :	{ resultado =  1 ; 		 }
		}
		
		return resultado;
	}

	@Override
	public void ImporteEnLetras(double dImpEntrada, String sImpLetras, int iMoneda) {
		// TODO Auto-generated method stub
		
		//Dim importe As Currency
	    //Dim centavos, miles, millones As Currency
	    //Dim unidades, decenas, centenas As Currency
	    int uni_millar, dec_millar, cen_millar ;
	    int uni_millon, dec_millon, cen_millon ;
	    //Dim PASO As Currency
	    int tama�o ;
	    String Cadena ;
	    String[] palabras = new String[100] ;
	    int POSICION ;
	    int faltan ;
	    String ENTEROS, DECIMALES ;
	    int miles_millon ;
	    
	    palabras[1] = "Un" ;
	    palabras[2] = "Dos" ;
	    palabras[3] = "Tres" ;
	    palabras[4] = "Cuatro" ;
	    palabras[5] = "Cinco" ;
	    palabras[6] = "Seis" ;
	    palabras[7] = "Siete" ;
	    palabras[8] = "Ocho" ;
	    palabras[9] = "Nueve" ;
	    palabras[10] = "Diez" ;
	    palabras[11] = "Once" ;
	    palabras[12] = "Doce" ;
	    palabras[13] = "Trece" ;
	    palabras[14] = "Catorce" ;
	    palabras[15] = "Quince" ;
	    palabras[16] = "Diez y Seis" ;
	    palabras[17] = "Diez y Siete" ;
	    palabras[18] = "Diez y Ocho" ;
	    palabras[19] = "Diez y Nueve" ;
	    palabras[20] = "Veinte" ; //'veinti
	    palabras[30] = "Treinta" ;
	    palabras[40] = "Cuarenta" ;
	    palabras[50] = "Cincuenta" ;
	    palabras[60] = "Sesenta" ;
	    palabras[70] = "Setenta" ;
	    palabras[80] = "Ochenta" ;
	    palabras[90] = "Noventa" ;
	    sImpLetras = "" ;
	    
	    //'VALIDA ENTRADA
	    Pro_val_entrada(dImpEntrada, PASO, importe, sImpLetras) ;
	    if (sImpLetras != "") {  		}
	    else {
	    	//' IDENTIFICA MONTOS
	        Cadena = "" + importe ;
	        POSICION = Cadena.indexOf(".", 0) ;
	        if (POSICION > -1) {
	            ENTEROS = Cadena.substring(0, POSICION - 1) ;
	            DECIMALES = Cadena.substring(Cadena.length()-POSICION, Cadena.length()) ;
	            faltan = 10 - (POSICION - 1) ;
	            String entero = "" ;
	            for(int i = 0; i < faltan ; i++) {
	            	entero = entero + "0" ;
	            }
	            ENTEROS = entero + ENTEROS ;
	            faltan = 2 - DECIMALES.length() ;
	            entero = "" ;
	            for(int i = 0; i < faltan ; i++) {
	            	entero = entero + "0" ;
	            }
	            DECIMALES = DECIMALES + entero ;
	        }
	        else {
	            faltan = 10 - Cadena.length() ;
	            entero = "" ;
	            for(int i = 0; i < faltan ; i++) {
	            	entero = entero + "0" ;
	            }
	            ENTEROS = entero + Cadena ;
	            DECIMALES = "00" ;
	        }
	        //eSTO ABRIA QUE MIRARLO YA QUE NO SE SI "ENTEROS" COMO ES UN STRING NO SE SI VIENEN CON PUNTOS DE SEPARACION O NO.
	        millones = Integer.parseInt(Val(ENTEROS.substring(0, 3))) ;
		    miles = Integer.parseInt(Val(ENTEROS.substring(3, 6))) ;
		    centenas = Integer.parseInt(Val(ENTEROS.substring(6, 7))) ;
		    decenas = Integer.parseInt(Val(ENTEROS.substring(7, 8))) ;
		    unidades = Integer.parseInt(Val(ENTEROS.substring(8, 9))) ;
		    
		    if ((Val("" + decenas + ""+ unidades) < 20) && (decenas > 0)){
		        decenas = Val("" + decenas + ""+ unidades) ;
		        unidades = 0 ;
		    }
		    centavos = Val(DECIMALES) ;
		    
		    // LINEA 798 en VB6' PROCESA MILLONES
		    
		    if (millones > 0) {
		        Cadena = ""+millones ;
		        tama�o = Cadena.length() ;
		        if (tama�o < 6) {
		            faltan = 6 - tama�o ;
		            entero = "" ;
		            for(int i = 0; i < faltan ; i++) {
		            	entero = entero + "0" ;
		            }
		            Cadena = entero + Cadena ;
		        }
		        miles_millon = Val(Cadena.substring(0, 3)) ;
		        if (Val(miles_millon) > 0) {
		            Pro_millares(miles_millon, sImpLetras, palabras, importe) ;
		        }
		        uni_millar = Val(Cadena.substring(5, 6)) ;
		        dec_millar = Val(Cadena.substring(4, 5)) ;
		        if ((Val(""+dec_millar + ""+uni_millar) < 20) && (dec_millar > 0)) {
		            dec_millar = Val(""+dec_millar + ""+ uni_millar) ;
		            uni_millar = 0 ;
		        }
		        cen_millar = Val(Cadena.substring(3, 4)) ;
		        if (cen_millar > 0) {
		           Pro_centenas(cen_millar, dec_millar, uni_millar, sImpLetras, palabras) ;
		        }
		        if (dec_millar > 0) {
		            Pro_decenas(uni_millar, dec_millar, sImpLetras, palabras) ;
		        }
		        if (uni_millar > 0) {
		            Pro_unidades(uni_millar, sImpLetras, palabras, dec_millar) ;
		        }
		        if (millones > 1) {
		            sImpLetras = sImpLetras + "Millones" + " " ;
		        }
		        else {
		            sImpLetras = sImpLetras + "Mill�n" + " " ;
		        }
		        if (miles == 0 && centenas == 0 && decenas == 0 && unidades == 0) {
		            sImpLetras = sImpLetras + "de " ;
		        }
		    }
		   // 'PROCESA MILLARES
		    if (miles > 0) {
		       Pro_millares(miles, sImpLetras, palabras, importe) ;
		    }
		    
		    //'PROCESA CENTENAS
		    if (centenas > 0) {
		        Pro_centenas(centenas, decenas, unidades, sImpLetras, palabras) ;
		    }

		    //'PROCESA DECENAS
		    if (decenas > 0) {
		       Pro_decenas(unidades, decenas, sImpLetras, palabras) ;
		    }

		    //'PROCESA UNIDADES
		    Pro_unidades(unidades, sImpLetras, palabras, decenas) ;
		    //'PROCESA CENTAVOS
		    if (importe >= 2) {
		        if (iMoneda == 1) {
		        	//EL FORMAT DE CENTABOS ABRIA QUE MIRARLO YA QUE NOSE QUE QUE TIPO DE VARIABLE SERA CENTAVOS
		        	//SI ES UN INT NO HARA FALTA CONVERSION.
		            sImpLetras = sImpLetras + "Pesos " + Format(CStr(centavos), "00") + "/100 M.N." ;
		        }
        		else {
		            if (iMoneda == 2) {
		            	//EL FORMAT DE CENTABOS ABRIA QUE MIRARLO YA QUE NOSE QUE QUE TIPO DE VARIABLE SERA CENTAVOS
			        	//SI ES UN INT NO HARA FALTA CONVERSION.
		                sImpLetras = sImpLetras + "Dolares " + Format(CStr(centavos), "00") + "/100 US Cy." ;
		            }
            		else{
		                sImpLetras = "=> MONEDA INVALIDA <=" ;
	                }
        		}
		    }
		    else {
		    	if (importe < 1) {
		            if (iMoneda == 1) {
		            	//EL FORMAT DE CENTABOS ABRIA QUE MIRARLO YA QUE NOSE QUE QUE TIPO DE VARIABLE SERA CENTAVOS
			        	//SI ES UN INT NO HARA FALTA CONVERSION.
		                sImpLetras = sImpLetras + "Cero Pesos " + Format(CStr(centavos), "00") + "/100 M.N." ;
		            }
		            else {
		                if (iMoneda == 2) {
		                	//EL FORMAT DE CENTABOS ABRIA QUE MIRARLO YA QUE NOSE QUE QUE TIPO DE VARIABLE SERA CENTAVOS
				        	//SI ES UN INT NO HARA FALTA CONVERSION.
		                    sImpLetras = sImpLetras + "Cero Dolares " + Format(CStr(centavos), "00") + "/100 US Cy." ;
		                }
		                else {
		                    sImpLetras = "=> MONEDA INVALIDA <=" ;
		                }
		            }
		    	}
		        else {
		        	if (iMoneda == 1) {
		        		//EL FORMAT DE CENTABOS ABRIA QUE MIRARLO YA QUE NOSE QUE QUE TIPO DE VARIABLE SERA CENTAVOS
			        	//SI ES UN INT NO HARA FALTA CONVERSION.
	                    sImpLetras = sImpLetras + "Pesos " + Format(CStr(centavos), "00") + "/100 M.N." ;
		        	}
	                else {
	                    if (iMoneda == 2) {
	                    	//EL FORMAT DE CENTABOS ABRIA QUE MIRARLO YA QUE NOSE QUE QUE TIPO DE VARIABLE SERA CENTAVOS
				        	//SI ES UN INT NO HARA FALTA CONVERSION.
	                        sImpLetras = sImpLetras + "Dolar " + Format(CStr(centavos), "00") + "/100 US Cy." ;
	                    }
	                    else {
	                        sImpLetras = "=> MONEDA INVALIDA <=" ;
	                    }
	                }
		        }
		    }
	    }
	}
	
	private void Pro_val_entrada(double Imp_entrada, /*currency PASO, currency importe,*/ String letras) {
		
		int POSICION , tama�o ;
		String trim_imp_entrada = String.valueOf(Imp_entrada).trim() ;
		if (trim_imp_entrada == "") {
	        letras = "=> SE REQUIERE UNA CADENA DE ENTRADA <=" ;
		}
	    else {
	    	if (!(isNumeric(trim_imp_entrada))) {
	    		letras = "=> LA CADENA DEBE SER NUMERICA <=" ;
	    	}
	    	else {
	    		POSICION = trim_imp_entrada.indexOf(".", 0) ;
	            tama�o = trim_imp_entrada.length() ;
	            
	            if (((tama�o - POSICION) > 2) && (POSICION > 0)) {
	                letras = "=> SOLO ADMITO DOS DECIMALES <=" ;
	            }
	            else {
	            	importe = CCur(Imp_entrada) ; //Conversion de double a currency 
                    PASO = importe ;
                    if (importe > 2147483647.49) {
                        letras = "=> NO PUEDO PROCESAR UN IMPORTE MAYOR A 2147483647.49 <=" ;
                    }
                    else {
                        if (importe == 0) {
                            letras = "=> EL IMPORTE ES IGUAL A CERO <=" ;
                        }
                    }
	            }
	    	}
	    }
	}
	
	private void Pro_millares(/*Currency miles,*/String letras, String[] palabras/*, importe*/) {
		
		String Cadena ;
		int tama�o, cen_millar, dec_millar ;
	    int uni_millar ;
	    Cadena = "" + miles ;
	    
	    tama�o = Cadena.length() ;
        if (tama�o == 3) {
           cen_millar = Val(Cadena.substring(1, 1)) ;
           dec_millar = Val(Cadena.substring(2, 1)) ;
           uni_millar = Val(Cadena.substring(3, 1)) ;
           if (((dec_millar * 10 + uni_millar) < 21) && ((dec_millar * 10 + uni_millar) > 9)) {
              dec_millar = dec_millar * 10 + uni_millar ;
              uni_millar = 0 ;
           }
        }
        else {
        	if (tama�o == 2) {
                dec_millar = Val(Cadena.substring(1, 1)) ;
                uni_millar = Val(Cadena.substring(2, 1)) ;
                if (((dec_millar * 10 + uni_millar) < 21) && ((dec_millar * 10 + uni_millar) > 9)) {
                    dec_millar = dec_millar * 10 + uni_millar ;
                    uni_millar = 0 ;
                }
        	}
            else {
                uni_millar = Val(Cadena.substring(1, 1)) ;
            }
        }
        
        if (cen_millar > 0){
        	Pro_centenas(cen_millar, dec_millar, uni_millar, letras, palabras) ;
        }
	    if (dec_millar > 0) {
	    	Pro_decenas(uni_millar, dec_millar, letras, palabras) ;
	    }
	    if (uni_millar > 0) {
	    	Pro_unidades(uni_millar, letras, palabras, dec_millar) ;
	    }
	    letras = letras + "Mil" + " " ;
	}
	
	private void Pro_centenas(int centenas, int decenas, int unidades, String letras, String[] palabras) {
		
		if (centenas == 1 && (unidades == 0 && decenas == 0)) {
		        letras = letras + "Cien" + " " ;
		}
	    else {
	    	if ((decenas > 0 || unidades > 0) && centenas == 1) {
	    		letras = letras + "Ciento" + " " ;
	    	}
	        else {
	        	switch(centenas) {
		        	case 2:{ letras = letras + "Doscientos"    + " " ; }
		        	case 3:{ letras = letras + "Trescientos"   + " " ; }
		        	case 4:{ letras = letras + "Cuatrocientos" + " " ; }
		        	case 5:{ letras = letras + "Quinientos"    + " " ; }
		        	case 6:{ letras = letras + "Seiscientos"   + " " ; }
		        	case 7:{ letras = letras + "Setecientos"   + " " ; }
		        	case 8:{ letras = letras + "Ochocientos"   + " " ; }
		        	case 9:{ letras = letras + "Novecientos"   + " " ; }
		        	default :{
		        		letras = letras + palabras[centenas] + " " ;
                        letras = letras + "Cientos" + " " ;
		        	}
	        	}
	        }
	    }
	}
	
	private void Pro_decenas(int unidades, int decenas, String letras, String[] palabras) {
	    if (decenas > 9 && decenas < 20) {
	       letras = letras + palabras[decenas] + " " ;
	       unidades = 0 ;
	    } 
	    else {
	         if (decenas == 2 && unidades > 0) {
	             letras = letras + "Veinti" ;
	         }
	         else {
	            letras = letras + palabras[decenas * 10] + " " ;
	         }
	    }
	}
	
	private void Pro_unidades(int unidades, int letras, String[] palabras/*, decenas*/) {
		
	    if ((String.valueOf(decenas).length() != 2) && (decenas != 2) && (decenas != "")){ 
	        if (decenas != 0 && unidades > 0) {
	            letras = letras + "y " ;
	        }
	    }
	    if (unidades > 0) {
	        if (decenas == 2) {
	            letras = letras + palabras[unidades].toLowerCase() + " " ;
	        }
	        else {
	        	letras = letras + palabras[unidades] + " " ;
	        }
	    }
	}
	
	private int ExtraeNivelUsuario(IfiduCOMParametros.ICOMParametros objPTransportador) {
		
		IfiduBaseDatos.IBaseDatos odb ; //'* JRN-FII/023-0801
		String strLQuery ; 				//'* JRN-FII/023-0802
		Recordset recLUsuarioPuesto ; 	//'* JRN-FII/023-0803
		String strLError ; 				//'* JRN-FII/023-0804
		String strLUsuario ; 			//'* JRN-FII/023-0805
		
		try {
			
			ExtraeNivelUsuario = -1 ;   //'* JRN-FII/023-0807
				    
	    	//' Recupera el usuario que est� firmado en el sistema   '* JRN-FII/023-0808
		    objPTransportador.subGRecuperaValores(1000, strLUsuario) ;	//   '* JRN-FII/023-0809
		    strLQuery = " select * from FDUSUARIOS U,FDPUESTOS P " + 
		                " where U.PUE_NUM_PUESTO=P.PUE_NUM_PUESTO " +
		                " AND usu_num_usuario=" + strLUsuario +
		                " AND USU_CVE_ST_USUARIO ='ACTIVO'" ; 			//'* JRN-FII/023-0810
			
		    odb = new IfiduBaseDatos.BaseDatos() ; //   '* JRN-FII/023-0811
    	    recLUsuarioPuesto = odb.EjecutaSeleccion(strLQuery, 0, strLError) ; //   '* JRN-FII/023-0812
    	    odb = null ; // '* JRN-FII/023-0813
    	    
    	    //' Si hay error en la lectura del Puesto del Usuario   '* JRN-FII/023-0814
    	    if (strLError != "") { //   '* JRN-FII/023-0815
    	       // Exit Function   '* JRN-FII/023-0816
    	    }//   '* JRN-FII/023-0817
    	    if (!recLUsuarioPuesto.EOF) { //'* JRN-FII/023-0818
    	        ExtraeNivelUsuario = recLUsuarioPuesto.Fields("PUE_APLICA_FILTRO") ; //'* JRN-FII/023-0819
    	    } //   '* JRN-FII/023-0820
		    
		}catch(Exception ex) {
			
		}
	}
	
	private boolean bolGChecaBqlHono(int intLCveBlq, String strLTipoBlq, String strLMsgErr) {
		
		IfiduBaseDatos.IBaseDatos odb ;
		String strLSQL ;
		ADODB.Recordset rsLSQL = new ADODB.Recordset() ;
		boolean resultado ;
		
		try {
			
			strLMsgErr = "" ;
				    
		    strLSQL = "SELECT * FROM FDBLQHONO WHERE BLQ_ID_TIPO=" + intLCveBlq ;

		    odb = new IfiduBaseDatos.BaseDatos() ;
		    rsLSQL = odb.EjecutaSeleccion(strLSQL, 0, strLMsgErr) ;
		    odb = null ;
		    
		    if (strLMsgErr != "") {
	        	if (!rsLSQL.EOF) {
	        		switch(strLTipoBlq) {
		        		case "PARAM":{
		        			if (rsLSQL("BLQ_PARAM_HONO") == 0) {
	                            return false ;
		        			}
		        		}
		        		case "CALC":{
		        			if (rsLSQL("BLQ_CALC_HONO") == 0) {
	                            return false ;
		        			}
		        		}
		        		case "BONI":{
		        			if (rsLSQL("BLQ_BONI_HONO") == 0) {
	                            return false ;
		        			}
		        		}
		        		case "PAGO":{
		        			if (rsLSQL("BLQ_PAGO_HONO") == 0) {
	                            return false ;
		        			}
		        		}
		        		case "CANC":{
		        			if (rsLSQL("BLQ_CANC_HONO") == 0) {
	                            return false ;
		        			}
		        		}
	        		}
	        	}
	        	else {
	        		return false ;
	        	}
		    }
		    else {
		        strLMsgErr = "Ocurrio un error al buscar el concepto del bloqueo de honorarios" ;
		        
		        //If Not rsLSQL Is Nothing Then Set rsLSQL = Nothing
		        if (rsLSQL != null) { rsLSQL = null ; }
        	    if (strLMsgErr == "") {
        	        strLMsgErr = "(" + Err.Number + ") " + Err.Description ;
        	    }
        	    return false ;
		    }
		    
		    if (rsLSQL != null) { rsLSQL = null ; }
		    return true ;
			
		}catch(Exception ex) {
			if (rsLSQL != null) { rsLSQL = null ; }
    	    if (strLMsgErr == "") {
    	        strLMsgErr = "(" + Err.Number + ") " + ex ;
    	    }
    	    return false ;
		}
	}
	
	private void CallBitacora(String sModulo, String sFuncion, String sMsgError, String sEstacion, long lUser) {
	  
		IfiduBitacora.IBitacora oBita ;
		boolean bEjecuto ;
	    
		oBita = new IfiduBitacora.Bitacora() ;
		// 			DoEvents
		bEjecuto = oBita.GeneraBitacora(sModulo, sFuncion, sMsgError, sEstacion, lUser) ;
		//			DoEvents
	 	oBita = null ;
	}
	
	
	public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
	
	public static boolean IsDate(String inDate) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(inDate.trim());
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	}
	
}
