package fiduEstrucContables;

import java.util.Date;

public class DetMovimiento {
/*
	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "DetMovimiento"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
	Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : fiduContabilidad.vbp                       *'
	'*  CLASES      : DetMovimiento.cls                          *'
	'*                                                           *'
	'*  DESCRIPCION : Implementacion de Clases VALUE para el     *'
	'*                Modulo de CONTABILIDAD.                    *'
	'*                                                           *'
	'*  AUTOR       : OAP - Septiembre 2002                      *'
	'*************************************************************'
	*/
	
	private Date mvardtFechaMovimiento ;
	private String mvarsStatus ;
	private long mvarlNumFolio ;
	private String mvarsNumOperacion ;
	private long mvarlNumPoliza ;
	private int mvariNumDatos ;
	private double mvardImporte ;
	
	public Date getMvardtFechaMovimiento() {
		return mvardtFechaMovimiento;
	}
	
	public void setMvardtFechaMovimiento(Date mvardtFechaMovimiento) {
		this.mvardtFechaMovimiento = mvardtFechaMovimiento;
	}
	
	public String getMvarsStatus() {
		return mvarsStatus;
	}
	
	public void setMvarsStatus(String mvarsStatus) {
		this.mvarsStatus = mvarsStatus;
	}
	
	public long getMvarlNumFolio() {
		return mvarlNumFolio;
	}
	
	public void setMvarlNumFolio(long mvarlNumFolio) {
		this.mvarlNumFolio = mvarlNumFolio;
	}
	
	public String getMvarsNumOperacion() {
		return mvarsNumOperacion;
	}
	
	public void setMvarsNumOperacion(String mvarsNumOperacion) {
		this.mvarsNumOperacion = mvarsNumOperacion;
	}
	
	public long getMvarlNumPoliza() {
		return mvarlNumPoliza;
	}
	
	public void setMvarlNumPoliza(long mvarlNumPoliza) {
		this.mvarlNumPoliza = mvarlNumPoliza;
	}
	
	public int getMvariNumDatos() {
		return mvariNumDatos;
	}
	
	public void setMvariNumDatos(int mvariNumDatos) {
		this.mvariNumDatos = mvariNumDatos;
	}
	
	public double getMvardImporte() {
		return mvardImporte;
	}
	
	public void setMvardImporte(double mvardImporte) {
		this.mvardImporte = mvardImporte;
	}
	
	
}
