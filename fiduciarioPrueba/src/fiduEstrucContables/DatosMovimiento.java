package fiduEstrucContables;

public class DatosMovimiento {
/*
	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "DatosMovimiento"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
	Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : fiduContabilidad.vbp                       *'
	'*  CLASES      : DatosMovimiento.cls                        *'
	'*                                                           *'
	'*  DESCRIPCION : Implementacion de Clases VALUE para el     *'
	'*                Modulo de CONTABILIDAD.                    *'
	'*                                                           *'
	'*  AUTOR       : EURITECH - OAP - Septiembre 2002           *'
	'*************************************************************'
	*/
	
	private String mvarsConcepto ;
	private String mvarvValor ;
	private int mvariNumDato ;
	
	
	
	public DatosMovimiento() {
		this.mvarsConcepto = "";
		this.mvarvValor = "";
		this.mvariNumDato = 0;
	}

	public String getMvarsConcepto() {
		return mvarsConcepto;
	}
	
	public void setMvarsConcepto(String mvarsConcepto) {
		this.mvarsConcepto = mvarsConcepto;
	}
	
	public String getMvarvValor() {
		return mvarvValor;
	}
	
	public void setMvarvValor(String mvarvValor) {
		this.mvarvValor = mvarvValor;
	}
	
	public int getMvariNumDato() {
		return mvariNumDato;
	}
	
	public void setMvariNumDato(int mvariNumDato) {
		this.mvariNumDato = mvariNumDato;
	}
	
}
