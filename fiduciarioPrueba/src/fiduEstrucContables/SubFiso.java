package fiduEstrucContables;

public class SubFiso {
/*
	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "Subfiso"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
	Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : fiduContabilidad.vbp                       *'
	'*  CLASES      : SubFiso.cls                                *'
	'*                                                           *'
	'*  DESCRIPCION : Implementacion de Clases VALUE para el     *'
	'*                Modulo de CONTABILIDAD.                    *'
	'*                                                           *'
	'*  AUTOR       : OAP - Septiembre 2002                      *'
	'*************************************************************'
*/
	
	private String mvarsNombre ;
	private int mvariNumero ;
	
	public String getMvarsNombre() {
		return mvarsNombre;
	}
	
	public void setMvarsNombre(String mvarsNombre) {
		this.mvarsNombre = mvarsNombre;
	}
	
	public int getMvariNumero() {
		return mvariNumero;
	}
	
	public void setMvariNumero(int mvariNumero) {
		this.mvariNumero = mvariNumero;
	}
}
