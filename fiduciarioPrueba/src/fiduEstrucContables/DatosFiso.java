package fiduEstrucContables;

public class DatosFiso {
/*
	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "DatosFiso"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
	Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : fiduContabilidad.vbp                       *'
	'*  CLASES      : DatosFiso.cls                              *'
	'*                                                           *'
	'*  DESCRIPCION : Implementacion de Clases VALUE para el     *'
	'*                Modulo de CONTABILIDAD.                    *'
	'*                                                           *'
	'*  AUTOR       : EURITECH - OAP - Septiembre 2002           *'
	'*************************************************************'
	*/
	
	private boolean mvarbAdmonPropia ;
	private int mvariClasifProducto ;
	private long mvarlNivelEstructura ;
	private String mvarsNomFiso ;
	private int mvariTipoNegocio ;
	private long mvarlNumFiso ;
	private int mvariNivelEstructura1 ;
	private int mvariNivelEstructura2 ;
	private int mvariNivelEstructura3 ;
	private int mvariNivelEstructura4 ;
	private int mvariNivelEstructura5 ;
	private boolean mvarbBloqueado ;
	private boolean mvarbMonedaExtranjera ;
	private boolean mvarbSubContrato ;
	private String mvarsStatus ;
	private int mvariAbiertoCerrado ; // '0: Abierto, 1: Cerrado
	private int mvariIVAEspecial ;
	private int mvariProrrateaSubFiso ; //'0: No prorratea, 1:Prorratea
	
	public boolean isMvarbAdmonPropia() {
		return mvarbAdmonPropia;
	}
	
	public void setMvarbAdmonPropia(boolean mvarbAdmonPropia) {
		this.mvarbAdmonPropia = mvarbAdmonPropia;
	}
	
	public int getMvariClasifProducto() {
		return mvariClasifProducto;
	}
	
	public void setMvariClasifProducto(int mvariClasifProducto) {
		this.mvariClasifProducto = mvariClasifProducto;
	}
	
	public long getMvarlNivelEstructura() {
		return mvarlNivelEstructura;
	}
	
	public void setMvarlNivelEstructura(long mvarlNivelEstructura) {
		this.mvarlNivelEstructura = mvarlNivelEstructura;
	}
	
	public String getMvarsNomFiso() {
		return mvarsNomFiso;
	}
	
	public void setMvarsNomFiso(String mvarsNomFiso) {
		this.mvarsNomFiso = mvarsNomFiso;
	}
	
	public int getMvariTipoNegocio() {
		return mvariTipoNegocio;
	}
	
	public void setMvariTipoNegocio(int mvariTipoNegocio) {
		this.mvariTipoNegocio = mvariTipoNegocio;
	}
	
	public long getMvarlNumFiso() {
		return mvarlNumFiso;
	}
	
	public void setMvarlNumFiso(long mvarlNumFiso) {
		this.mvarlNumFiso = mvarlNumFiso;
	}
	
	public int getMvariNivelEstructura1() {
		return mvariNivelEstructura1;
	}
	
	public void setMvariNivelEstructura1(int mvariNivelEstructura1) {
		this.mvariNivelEstructura1 = mvariNivelEstructura1;
	}
	
	public int getMvariNivelEstructura2() {
		return mvariNivelEstructura2;
	}
	
	public void setMvariNivelEstructura2(int mvariNivelEstructura2) {
		this.mvariNivelEstructura2 = mvariNivelEstructura2;
	}
	
	public int getMvariNivelEstructura3() {
		return mvariNivelEstructura3;
	}
	
	public void setMvariNivelEstructura3(int mvariNivelEstructura3) {
		this.mvariNivelEstructura3 = mvariNivelEstructura3;
	}
	
	public int getMvariNivelEstructura4() {
		return mvariNivelEstructura4;
	}
	
	public void setMvariNivelEstructura4(int mvariNivelEstructura4) {
		this.mvariNivelEstructura4 = mvariNivelEstructura4;
	}
	
	public int getMvariNivelEstructura5() {
		return mvariNivelEstructura5;
	}
	
	public void setMvariNivelEstructura5(int mvariNivelEstructura5) {
		this.mvariNivelEstructura5 = mvariNivelEstructura5;
	}
	
	public boolean isMvarbBloqueado() {
		return mvarbBloqueado;
	}
	
	public void setMvarbBloqueado(boolean mvarbBloqueado) {
		this.mvarbBloqueado = mvarbBloqueado;
	}
	
	public boolean isMvarbMonedaExtranjera() {
		return mvarbMonedaExtranjera;
	}
	
	public void setMvarbMonedaExtranjera(boolean mvarbMonedaExtranjera) {
		this.mvarbMonedaExtranjera = mvarbMonedaExtranjera;
	}
	
	public boolean isMvarbSubContrato() {
		return mvarbSubContrato;
	}
	
	public void setMvarbSubContrato(boolean mvarbSubContrato) {
		this.mvarbSubContrato = mvarbSubContrato;
	}
	
	public String getMvarsStatus() {
		return mvarsStatus;
	}
	
	public void setMvarsStatus(String mvarsStatus) {
		this.mvarsStatus = mvarsStatus;
	}
	
	public int getMvariAbiertoCerrado() {
		return mvariAbiertoCerrado;
	}
	
	public void setMvariAbiertoCerrado(int mvariAbiertoCerrado) {
		this.mvariAbiertoCerrado = mvariAbiertoCerrado;
	}
	
	public int getMvariIVAEspecial() {
		return mvariIVAEspecial;
	}
	
	public void setMvariIVAEspecial(int mvariIVAEspecial) {
		this.mvariIVAEspecial = mvariIVAEspecial;
	}
	
	public int getMvariProrrateaSubFiso() {
		return mvariProrrateaSubFiso;
	}
	
	public void setMvariProrrateaSubFiso(int mvariProrrateaSubFiso) {
		this.mvariProrrateaSubFiso = mvariProrrateaSubFiso;
	}
	
}
