package fiduContabHonorarios;

public interface IContabilizador {
/*
	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "clsContabilizador"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : fiduContabHonorarios.vbp                   *'
	'*  CLASES      : Contabilizador.cls                         *'
	'*                                                           *'
	'*  DESCRIPCION : Libreria de Tipos Servicios para Contabili-*'
	'*                zar HONORARIOS.                            *'
	'*                                                           *'
	'*  AUTOR       : OAP - Enero 2003                           *'
	'*************************************************************'
	*/
	
	//'ContabilizaHonorarios
	public boolean ContabilizaHonorarios(int iFuncion, String sNumFiso, String sNumSubFiso, int iTipoPersona, 
	                                       long lNumPersona, String sFormaPago, String sConcepto, String sStatusAdeudo,
	                                       double dImporteHon, double dImporteIVA, double dImporteMorat,
	                                       double dImporteMoratIva, int iMoneda, double dTipoCambioOrig,
	                                       fiduEstrucGlobales.Cliente oCliente, long lNumFolioContab, String sMsgServ,
	                                       String sMsgErr) ;
	//'ContabilizaDevoluciones
	public boolean ContabilizaDevoluciones(int iFuncion, String sNumFiso, int iTipoPersona, long lNumPersona, 
	                                        String sConcepto, String sFormaDeposito, double dImporteDev,  int iMoneda, 
	                                        fiduEstrucGlobales.Cliente oCliente,long lNumFolioContab, String sMsgServ, 
	                                        String sMsgErr) ;
	
}
