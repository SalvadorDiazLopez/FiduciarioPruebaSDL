package fiduContabHonorarios;

import java.text.SimpleDateFormat;
import java.util.Date;

import IfiduGenerales.fiduCOMParametros;
import fiduEstrucContables.DatosMovimiento;

/*
VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Contabilizador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'*************************************************************'
'*           Derechos Reservados EFISOFT S.A. 2002           *'
'*************************************************************'
'*  ARCHIVO     : fiduContabHonorarios.vbp                   *'
'*  CLASES      : Contabilizador.cls                         *'
'*                                                           *'
'*  DESCRIPCION : Implementacion de Servicios para Contabili-*'
'*                zar HONORARIOS.                            *'
'*                                                           *'
'*  AUTOR       : OAP - Enero 2003                           *'
'*************************************************************'
*/

import fiduEstrucGlobales.Cliente;



public class Contabilizador implements IContabilizador {

	private final String m_sObjectName = "fiduContabHonorarios" ;
	//'Funciones
	private final int m_iCOBROCOMISIONES = 1 ;
	private final int m_iDEVOLUCIONPAGOS = 2 ;
	private final int m_iCANCECONDO = 3 ;
	//'Tipos de Persona
	private final int m_iFIDEICOMITENTE = 1 ;
	private final int m_iFIDEICOMISARIO = 2 ;
	private final int m_iTERCERO = 3 ;
	private final int m_iCARGOFONDO = 4 ;
	//'Monedas
	private final int m_iMonedaNac = 1 ;
	private final int m_iDolares = 2 ;
	//'GeneraDatoVal
	String[] sTipDatos ;
	String[] vValDatos ;
	//'Globales usadas en Todos los casos
	DatosMovimiento[] oDatosMov ;
	double dTipoCambio ;
	Date dtFechaCont ;
	//'Cantidad de Elementos a Contabilizar para cada Concepto
	private final int m_iCOMISIONES_MN = 8 ;
	private final int m_iCOMISIONES_DL = 10 ;
	private final int m_iDEVOLUCIONES_MN = 4 ;
	private final int m_iDEVOLUCIONES_DL = 5 ;
	
	@Override
	public boolean ContabilizaHonorarios(int iFuncion, String sNumFiso, String sNumSubFiso, int iTipoPersona,
			long lNumPersona, String sFormaPago, String sConcepto, String sStatusAdeudo, double dImporteHon,
			double dImporteIVA, double dImporteMorat, double dImporteMoratIva, int iMoneda, double dTipoCambioOrig,
			Cliente oCliente, long lNumFolioContab, String sMsgServ, String sMsgErr) {
		// TODO Auto-generated method stub
		
		boolean resultado = false ;
		
		IfiduGenerales.IGenerales oServGnrls ;
		IfiduFechas.IFechas oFechas ;
		fiduEstrucContables.DatosFiso oFiso ;
		fiduEstrucContables.SubFiso oSFiso ; 
		fiduEstrucContables.DetMovimiento oDetMov ;
		IfiduContabilidad.IContabilizador oContable ;
		String sNumOperacion ;
		String sPersona ;
		int iContador ;
		int iNumDatos ;
		double dImporteTotal ;
		boolean bSugerencias ;
		//'' rac Modificaciones 05/08/2003
		int iNumIntermed ;
		long lNumCtoInver ;
		
		try {
			
			//'USO CONTABLE GENERAL
		    //'Creacion de Objetos
		    oFiso = new fiduEstrucContables.DatosFiso() ;
		    oSFiso = new fiduEstrucContables.SubFiso() ;
		    oDetMov = new fiduEstrucContables.DetMovimiento() ;
		    oFechas = new fiduFechas.Fechas() ;
		    oServGnrls = new IfiduGenerales.Generales() ;
		    oContable = new IfiduContabilidad.Contabilizador() ;
		    
		    if (sMsgServ != "") {
		        if (IfiduGenerales.Generales.IsDate(sMsgServ)){
		        	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		            dtFechaCont = formatter.parse(sMsgServ) ;
		        }
		        else {
		            dtFechaCont = oFechas.dtContable ;
		        }
		    }
		    else {
		        dtFechaCont = oFechas.dtContable ;
		    }
		    
		    if (oFechas != null) { oFechas = null ; }
		    	    
		    if (!oServGnrls.VerificaContratoSrv(sNumFiso, true, oFiso, sMsgErr, false, null)) {
				//Erase oDatosMov
			    for(int i=0; i<oDatosMov.length; i++) {
			    	oDatosMov[i] = null ;
			    }
			    if (oDetMov != null){ oDetMov = null ; }
	    		if (oFiso != null){ oFiso = null ; }
			    if (oSFiso != null){ oSFiso = null ; }
			    if (oContable != null){ oContable = null ; }
			    if (oServGnrls != null){ oServGnrls = null ; }
			    if (oFechas != null){ oFechas = null ; }
			    sMsgErr = sMsgErr + "\n" +
			             "Fuente : " + m_sObjectName + ".ContabilizaHonorarios" ;
			    return resultado ;
		    }
		    
		    if ((sNumSubFiso != "") && (sNumSubFiso.length() > 0)) {
		    	//Da error en este parametro fiduCOMParametros.ICOMParametros objPTransportador
		        if (!oServGnrls.VerificaSubContratoSrv(oFiso, Integer.parseInt(sNumFiso), true, oSFiso, sMsgErr, false, null, null)) {
					//Erase oDatosMov
				    for(int i=0; i<oDatosMov.length; i++) {
				    	oDatosMov[i] = null ;
				    }
				    if (oDetMov != null){ oDetMov = null ; }
		    		if (oFiso != null){ oFiso = null ; }
				    if (oSFiso != null){ oSFiso = null ; }
				    if (oContable != null){ oContable = null ; }
				    if (oServGnrls != null){ oServGnrls = null ; }
				    if (oFechas != null){ oFechas = null ; }
				    sMsgErr = sMsgErr + "\n" +
				             "Fuente : " + m_sObjectName + ".ContabilizaHonorarios" ;
				    return resultado ;
		        }
		    }
		    dTipoCambio = oServGnrls.TipoCambio(iMoneda, 0, dtFechaCont) ;
		    
		    if (oServGnrls != null ) { oServGnrls = null ; }
		    
		    //'USO CONTABLE VARIABLE dependiendo del Tipo de Funcion
		    
		    switch (iFuncion) {
			    case m_iCOBROCOMISIONES : {
			    	/*//''' rac se modifica la condici�n de sugerencias para DESCONTAR DEL FONDO 05/08/2003
				    if (sFormaPago.equals("DESCONTAR DEL FONDO")) {
				          bSugerencias = true ;
				    }
				    else {
				          bSugerencias = false ;
				    }
			        sPersona = ObtenStrPersona(iTipoPersona) ;
			        //''' rac se modifica la llamada para traer intermed y ctoinv 05/08/2003
	                sNumOperacion = ArmaOpeCobroComisiones(oFiso, sPersona, sFormaPago, sConcepto, iMoneda, iNumIntermed, lNumCtoInver, sMsgServ, sMsgErr) ;
	                if (iMoneda == m_iMonedaNac) {
	                    iNumDatos = m_iCOMISIONES_MN ;
	                }
	                else {
	                    if (dTipoCambio == 0) {
	                        sMsgServ = "No existe tipo de cambio para el dia." ;
							//Erase oDatosMov
						    for(int i=0; i<oDatosMov.length; i++) {
						    	oDatosMov[i] = null ;
						    }
						    if (oDetMov != null){ oDetMov = null ; }
				    		if (oFiso != null){ oFiso = null ; }
						    if (oSFiso != null){ oSFiso = null ; }
						    if (oContable != null){ oContable = null ; }
						    if (oServGnrls != null){ oServGnrls = null ; }
						    if (oFechas != null){ oFechas = null ; }
						    sMsgErr = sMsgErr + "\n" +
						             "Fuente : " + m_sObjectName + ".ContabilizaHonorarios" ;
				            return resultado ;
	                    }
	                    iNumDatos = m_iCOMISIONES_DL ;
	                    dImporteHon = dImporteHon * dTipoCambio ;
	                    dImporteIVA = dImporteIVA * dTipoCambio ;
	                    dImporteMorat = dImporteMorat * dTipoCambio ;
	                    
	                    dImporteTotal = dImporteHon + dImporteIVA + dImporteMorat ;
	                    
	                    sTipDatos = new String[iNumDatos] ;
	                    vValDatos = new String[iNumDatos] ;
	                    
	                    sTipDatos[0] = "CONTRATO" ;
	                    sTipDatos[1] = sPersona ;
	                    sTipDatos[2] = "PROVEEDOR" ;
	                    sTipDatos[3] = "CTOINVERSION" ;
	                    sTipDatos[4] = Format(iMoneda, "00") + "HONORARIOS" ;
	                    sTipDatos[5] = Format(iMoneda, "00") + "IVA" ;
	                    sTipDatos[6] = Format(iMoneda, "00") + "MORATORIOS" ;
	                    sTipDatos[7] = Format(iMoneda, "00") + "IMPORTE" ;
	                    
	                    if (iMoneda != m_iMonedaNac) {
	                    	sTipDatos[8] = Format(iMoneda, "00") + "TIPO DE CAMBIO" ;
	                    }
	                    
	                    vValDatos[0] = ""+oFiso.getMvarlNumFiso() ;
	                    vValDatos[1] = ""+lNumPersona ;
	                    //''' rac se carga intermed y ctoinver 05/08/2003
	                    vValDatos[2] = ""+iNumIntermed ;
	                    vValDatos[3] = ""+lNumCtoInver ;
	        
	                    vValDatos[4] = Format(dImporteHon, "###############0.00") ;
	                    vValDatos[5] = Format(dImporteIVA, "###############0.00") ;
	                    vValDatos[6] = Format(dImporteMorat, "###############0.00") ;
	                    vValDatos[7] = Format(dImporteTotal, "###############0.00") ;
	                    if (iMoneda != m_iMonedaNac) {
	                        vValDatos[8] = Format(dTipoCambio, "############0.00000000")
	                    }
	                }*/
			    }
			    case m_iCANCECONDO : {
			    	bSugerencias = false ;
	                sPersona = ObtenStrPersona(iTipoPersona) ;
	                sNumOperacion = ArmaOpeCanCon(oFiso, sFormaPago, sPersona, sConcepto, iMoneda, sStatusAdeudo, sMsgServ, sMsgErr) ;
	                if (iMoneda == m_iMonedaNac) {
	                    iNumDatos = m_iCOMISIONES_MN ;
	                }
	                else {
	                    if (dTipoCambioOrig == 0) {
	                        sMsgServ = "No existe tipo de cambio para el dia." ;
	            			//Erase oDatosMov
	            		    for(int i=0; i<oDatosMov.length; i++) {
	            		    	oDatosMov[i] = null ;
	            		    }
	            		    if (oDetMov != null){ oDetMov = null ; }
	                		if (oFiso != null){ oFiso = null ; }
	            		    if (oSFiso != null){ oSFiso = null ; }
	            		    if (oContable != null){ oContable = null ; }
	            		    if (oServGnrls != null){ oServGnrls = null ; }
	            		    if (oFechas != null){ oFechas = null ; }
	            		    sMsgErr = sMsgErr + "\n" +
	            		             "Fuente : " + m_sObjectName + ".ContabilizaHonorarios" ;
	            		    return resultado ;
	                    }
	                    iNumDatos = m_iCOMISIONES_DL ;
	                    dImporteHon = dImporteHon * dTipoCambioOrig ;
	                    dImporteIVA = dImporteIVA * dTipoCambioOrig ;
	                    dImporteMorat = dImporteMorat * dTipoCambioOrig ;
	                }
	                
	                dImporteTotal = dImporteHon + dImporteIVA + dImporteMorat ;
	                
	                sTipDatos = new String[iNumDatos] ;
	                vValDatos = new String[iNumDatos] ;
	                
	                sTipDatos[0] = "CONTRATO" ;
	                sTipDatos[1] = "SUBCONTRATO" ; // '* DCTB-FII
	                sTipDatos[2] = sPersona ;
	                sTipDatos[3] = "PROVEEDOR" ;
	                sTipDatos[4] = "CTOINVERSION" ;
	                sTipDatos[5] = Format(iMoneda, "00") + "HONORARIOS" ;
	                sTipDatos[6] = Format(iMoneda, "00") + "IVA" ;
	                sTipDatos[7] = Format(iMoneda, "00") + "EXTEMPORANEIDAD" ;
	                sTipDatos[8] = Format(iMoneda, "00") + "IMPORTE" ;
	                
	                if (iMoneda != m_iMonedaNac) {
	                    sTipDatos[9] = Format(iMoneda, "00") + "TIPO DE CAMBIO" ;
	                    sTipDatos[10] = Format(iMoneda, "00") + "TC PROVISION" ;
	                }
	                
	                vValDatos[0] = ""+oFiso.getMvarlNumFiso() ;
	                vValDatos[1] = ""+oSFiso.getMvariNumero() ; //'* DCTB-FII
	                vValDatos[2] = ""+lNumPersona ;
	                vValDatos[3] = "" ; //'En Comisiones no hay Sugerencias
	                vValDatos[4] = "" ; //'En Comisiones no hay Sugerencias
	                vValDatos[5] = Format(dImporteHon, "###############0.00") ;
	                vValDatos[6] = Format(dImporteIVA, "###############0.00") ;
	                vValDatos[7] = Format(dImporteMorat, "###############0.00") ;
	                vValDatos[8] = Format(dImporteTotal, "###############0.00") ;
	                if (iMoneda != m_iMonedaNac) {
	                    vValDatos[9] = Format(dTipoCambio, "############0.00000000") ;
	                    vValDatos[10] = Format(dTipoCambioOrig, "############0.00000000") ;
	                }
			    }
		    }
		    
		    //'USO CONTABLE GENERAL
		    GeneraDatoval(iNumDatos) ;
		//'    oSFiso.iNumero = 0
		//'    oSFiso.sNombre = vbNullString
	
		    oDetMov.setMvardtFechaMovimiento(dtFechaCont); 
		    oDetMov.setMvarsNumOperacion(sNumOperacion); 
		    oDetMov.setMvarsStatus("ACTIVO");
		    oDetMov.setMvardImporte(dImporteTotal); 
		    oDetMov.setMvarlNumPoliza(0);
		    oDetMov.setMvariNumDatos(iNumDatos + 1);
		    
		    if (oContable.GenAplicaOperacionSrv(oFiso, oSFiso, oDetMov, oDatosMov, lNumFolioContab, oCliente, sMsgServ, sMsgErr, bSugerencias)) {
	        	resultado = true ;
		    }
		    else {
				//Erase oDatosMov
			    for(int i=0; i<oDatosMov.length; i++) {
			    	oDatosMov[i] = null ;
			    }
			    if (oDetMov != null){ oDetMov = null ; }
	    		if (oFiso != null){ oFiso = null ; }
			    if (oSFiso != null){ oSFiso = null ; }
			    if (oContable != null){ oContable = null ; }
			    if (oServGnrls != null){ oServGnrls = null ; }
			    if (oFechas != null){ oFechas = null ; }
			    sMsgErr = sMsgErr + "\n" +
			             "Fuente : " + m_sObjectName + ".ContabilizaHonorarios" ;
			    return resultado ;
		    }
		    
		    //Erase oDatosMov
		    for(int i=0; i<oDatosMov.length; i++) {
		    	oDatosMov[i] = null ;
		    }
		    if (oDetMov != null){ oDetMov = null ; }
		    if (oFiso != null){ oFiso = null ; }
		    if (oSFiso != null){ oSFiso = null ; }
		    if (oContable != null){ oContable = null ; }
		    
		}catch(Exception ex) {
			
			//Erase oDatosMov
		    for(int i=0; i<oDatosMov.length; i++) {
		    	oDatosMov[i] = null ;
		    }
		    if (oDetMov != null){ oDetMov = null ; }
    		if (oFiso != null){ oFiso = null ; }
		    if (oSFiso != null){ oSFiso = null ; }
		    if (oContable != null){ oContable = null ; }
		    if (oServGnrls != null){ oServGnrls = null ; }
		    if (oFechas != null){ oFechas = null ; }
		    sMsgErr = sMsgErr + "\n" + ex + "\n" +
		             "Fuente : " + m_sObjectName + ".ContabilizaHonorarios" ;
		    return resultado ;
		}
		
		return resultado;
	}

	@Override
	public boolean ContabilizaDevoluciones(int iFuncion, String sNumFiso, int iTipoPersona, long lNumPersona,
			String sConcepto, String sFormaDeposito, double dImporteDev, int iMoneda, Cliente oCliente,
			long lNumFolioContab, String sMsgServ, String sMsgErr) {
		// TODO Auto-generated method stub
		
		boolean resultado = false ;
		
		IfiduGenerales.IGenerales oServGnrls ;
		IfiduFechas.IFechas oFechas ;
		fiduEstrucContables.DatosFiso oFiso ;
		fiduEstrucContables.SubFiso oSFiso ;
		fiduEstrucContables.DetMovimiento oDetMov ;
		IfiduContabilidad.IContabilizador oContable ;
		String sNumOperacion ;
		String sPersona ;
		int iContador ;
		int iNumDatos ;
		boolean bSugerencias ;
		boolean bEjecuto ;
		String sSql ;
		IfiduBaseDatos.IBaseDatos oAccesoBD ;
		IfiduFolio.clsFolio oFolio ;
		long lFolioRecep ;
		ADODB.Recordset rsCuenta = new ADODB.Recordset() ;
		String sCuenta ;
		String sPlaza ;
		
		try {
			
			//'USO CONTABLE GENERAL
		    //'Creacion de Objetos
		    oFiso = new fiduEstrucContables.DatosFiso() ;
		    oSFiso = new fiduEstrucContables.SubFiso() ;
		    oDetMov = new fiduEstrucContables.DetMovimiento() ;
		    oFechas = new fiduFechas.Fechas() ;
		    oServGnrls = new IfiduGenerales.Generales() ;
		    oContable = new IfiduContabilidad.Contabilizador() ;
		    
		    dtFechaCont = oFechas.dtContable ;
    	    if (!oServGnrls.VerificaContratoSrv(sNumFiso, true, oFiso, sMsgErr, false, null)) {
    	    	//Erase oDatosMov
    		    for(int i=0; i<oDatosMov.length; i++) {
    		    	oDatosMov[i] = null ;
    		    }
    		    if (oDetMov != null){ oDetMov = null ; }
    		    if (oFiso != null){ oFiso = null ; }
    		    if (oSFiso != null){ oSFiso = null ; }
    		    if (oContable != null){ oContable = null ; }
    		    if (oServGnrls != null){ oServGnrls = null ; }
    		    if (oFechas != null){ oFechas = null ; }
    		    if (rsCuenta != null){ rsCuenta = null ; }
    		    sMsgErr = sMsgErr + "\n" + 
    		             "Fuente : " + m_sObjectName + ".ContabilizaDevoluciones" ;
    		    return resultado ;
    	    }
    	    dTipoCambio = oServGnrls.TipoCambio(iMoneda, 0, dtFechaCont) ;
    	    if (oServGnrls != null){ oServGnrls = null ; }
    	    if (oFechas != null) { oFechas = null ; }
			
    	    //'USO CONTABLE VARIABLE dependiendo del Tipo de Funcion
    	    
    	    switch(iFuncion) {
	    	    case m_iDEVOLUCIONPAGOS : {
	    	    	bSugerencias = true ;
	                sPersona = ObtenStrPersona(iTipoPersona) ;
	                sNumOperacion = ArmaOpeDevolPagos(sConcepto, sFormaDeposito, iMoneda, sMsgServ, sMsgErr) ;
	                if (iMoneda == m_iMonedaNac) {
	                    iNumDatos = m_iDEVOLUCIONES_MN ;
	                }
	                else {
	                    if (dTipoCambio == 0) {
	                        sMsgServ = "No existe tipo de cambio para el dia." ;
	                      //Erase oDatosMov
	            		    for(int i=0; i<oDatosMov.length; i++) {
	            		    	oDatosMov[i] = null ;
	            		    }
	            		    if (oDetMov != null){ oDetMov = null ; }
	            		    if (oFiso != null){ oFiso = null ; }
	            		    if (oSFiso != null){ oSFiso = null ; }
	            		    if (oContable != null){ oContable = null ; }
	            		    if (oServGnrls != null){ oServGnrls = null ; }
	            		    if (oFechas != null){ oFechas = null ; }
	            		    if (rsCuenta != null){ rsCuenta = null ; }
	            		    sMsgErr = sMsgErr + "\n" + 
	            		             "Fuente : " + m_sObjectName + ".ContabilizaDevoluciones" ;
	            		    return resultado ;
	                    }
	                    iNumDatos = m_iDEVOLUCIONES_DL ;
	                    dImporteDev = dImporteDev * dTipoCambio ;
	                }
	                
	                sTipDatos = new String[iNumDatos];
	                vValDatos = new String[iNumDatos];
	                
	                sTipDatos[0] = "CONTRATO" ;
                    sTipDatos[1] = sPersona ;
                    sTipDatos[2] = "PROVEEDOR" ;
                    sTipDatos[3] = "CTOINVERSION" ;
                    sTipDatos[4] = Format(iMoneda, "00") + "IMPORTE" ;
                    if (iMoneda != m_iMonedaNac){
                        sTipDatos[5] = Format(iMoneda, "00") + "TIPO DE CAMBIO" ;
                    }
	                
                    vValDatos[0] = ""+oFiso.lNumero ;
                    vValDatos[1] = ""+lNumPersona ;
                    vValDatos[2] = "" ;//' Sugerencias autom�ticas
                    vValDatos[3] = "" ; //' Sugerencias autom�ticas
                    vValDatos[4] = Format(dImporteDev, "###############0.00") ;
                    if (iMoneda != m_iMonedaNac) {
                        vValDatos[5] = Format(dTipoCambio, "############0.00000000") ;
                    }
	    	    }
    	    }
    	    
    	    //'USO CONTABLE GENERAL
    	    GeneraDatoval(iNumDatos) ;
    	    oSFiso.setMvariNumero(0);
    	    oSFiso.setMvarsNombre("");

    	    oDetMov.setMvardtFechaMovimiento(dtFechaCont);
    	    oDetMov.setMvarsNumOperacion(sNumOperacion);
    	    oDetMov.setMvarsStatus("ACTIVO"); 
    	    oDetMov.setMvardImporte(dImporteDev);
    	    oDetMov.setMvarlNumPoliza(0);
    	    oDetMov.setMvariNumDatos(iNumDatos + 1); 
    	    
    	    if (oContable.GenAplicaOperacionSrv(oFiso, oSFiso, oDetMov, oDatosMov, lNumFolioContab, oCliente, sMsgServ, sMsgErr, bSugerencias)) {
    	    	//Linea 354 en Visual Basic v6 
    	    	oFolio = new fiduFolio.Folio() ;
    	        lFolioRecep = oFolio.setFolio(22) ;
    	        if (oFolio != null) { oFolio = null ; }
    	    	
    	        sSql = " SELECT CVE_FORMA_EMP_CVE" ;
	                sSql = sSql + " FROM   CLAVES " ;
	                sSql = sSql + " WHERE  CVE_LIMINF_CLAVE  = 44 " ;
	                sSql = sSql + " AND    CVE_LIMSUP_CLAVE  = " + iMoneda ;
	                sSql = sSql + " AND    CVE_NUM_SEC_CLAVE = " + oFiso.lNumero ;//Esto seria un get al objeto folio
	                sSql = sSql + " AND    CVE_NUM_CLAVE     = " + 299  ;//'Concentradoras Especiales
	                sSql = sSql + " AND    CVE_CVE_ST_CLAVE  = 'ACTIVO'" ;
    	    	
                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
                //		DoEvents
                rsCuenta = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgErr) ;
                //		DoEvents
                if (sMsgErr.length() > 0) { 
                	Err.Raise -1, , "Error: Al buscar la cuenta concentradora del Fiso " 
                		+ oFiso.getMvarlNumFiso() + "\n" + " Ya se ejecut� la contabilizaci�n de la devoluci�n pero no se registr� el dep�sito." ;
    	    	}
                if (oAccesoBD != null ) { oAccesoBD = null ; }
                
                if (rsCuenta.EOF) {
	                sSql = " SELECT CVE_FORMA_EMP_CVE" ;
			         sSql = sSql + " FROM   CLAVES " ;
			         sSql = sSql + " WHERE  CVE_LIMINF_CLAVE  = 44 " ;
			         sSql = sSql + " AND    CVE_LIMSUP_CLAVE  = " + iMoneda ;
			         sSql = sSql + " AND    CVE_NUM_CLAVE     = " + 300 ; //         'Concentradoras Generales
			         sSql = sSql + " AND    CVE_CVE_ST_CLAVE  = 'ACTIVO'" ;
			
			         oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
			         // 	DoEvents
			         rsCuenta = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgErr) ;
			         //		DoEvents
			         if (sMsgErr.length() > 0){ 
			        	 Err.Raise -1, , "Error: Al buscar la cuenta concentradora del Fiso " + oFiso.getMvarlNumFiso() + "\n" + 
			        	 " Ya se ejecut� la contabilizaci�n de la devoluci�n pero no se registr� el dep�sito." ;
			         }
			                                                  
			         if (oAccesoBD != null ) { oAccesoBD = null ; }
			 
			         if (rsCuenta.EOF) {
			           sMsgServ = "Error: No se encontr� la cuenta concentradora del Fiso " + oFiso.getMvarlNumFiso() + "\n" + 
			                      " Ya se ejecut� la contabilizaci�n de la devoluci�n pero no se registr� el dep�sito." ;
			           //Erase oDatosMov
		    		    for(int i=0; i<oDatosMov.length; i++) {
		    		    	oDatosMov[i] = null ;
		    		    }
		    		    if (oDetMov != null){ oDetMov = null ; }
		    		    if (oFiso != null){ oFiso = null ; }
		    		    if (oSFiso != null){ oSFiso = null ; }
		    		    if (oContable != null){ oContable = null ; }
		    		    if (oServGnrls != null){ oServGnrls = null ; }
		    		    if (oFechas != null){ oFechas = null ; }
		    		    if (rsCuenta != null){ rsCuenta = null ; }
		    		    sMsgErr = sMsgErr + "\n" + 
		    		             "Fuente : " + m_sObjectName + ".ContabilizaDevoluciones" ;
		    		    return resultado ;
			         }
                }
                
                sCuenta = Right(rsCuenta!CVE_FORMA_EMP_CVE, 8) ;
                sPlaza = Left(rsCuenta!CVE_FORMA_EMP_CVE, 3) ;
                
                if (rsCuenta != null) { rsCuenta = null ; }
                
                sSql = " INSERT INTO DEPOSIT"  + " VALUES(" + lFolioRecep + "," + lNumFolioContab + "," ;
	                sSql = sSql + sNumOperacion + "," ;
	                sSql = sSql + "0," ; //   'Sec Operacion
	                sSql = sSql + "0," ; //   'Modulo
	                sSql = sSql + "0," ; //   'Num Transaccion
	                sSql = sSql + oFiso.getMvarlNumFiso() + "," ;
	                sSql = sSql + "0," ; //   'SubContrato
	                sSql = sSql + sNumOperacion.substring(2, 2) + "," ; // 'Concepto
	                sSql = sSql + lNumPersona + "," ; //   'Num. Persona
	                sSql = sSql + "0," ; //   'Folio Oper Sis
	                sSql = sSql + "0," ; //   'Benef. x Tercero
	                sSql = sSql + "1," ; //   'Cotizaci�n
	                sSql = sSql + "''," ; // 'Nombre de Persona
	                sSql = sSql + "''," ; // 'Tipo de Persona
	                sSql = sSql +sNumOperacion.substring(4, 2) + "," ; // 'Tipo de Dep�sito
	                sSql = sSql + dImporteDev + "," + iMoneda + "," + "0," + "'CONCENTRADORA'," ;
	                sSql = sSql + "44," ; //  'Num. Banco
	                sSql = sSql + "'SCOTIABANK INVERLAT, S.A.'" + sPlaza + "," + "''," + "0," + "0," + "''," ;
	                sSql = sSql + "''," + "''," + sCuenta + "," + dtFechaCont + "," + "''," + "0," + "0," + "''," ;
	                sSql = sSql + "0," + "0," + "0," + "0," + "0," + "0," + "''," + "0," + "0," + "1," ;
	                sSql = sSql + dtFechaCont.getYear() + "," + dtFechaCont.getMonth() + "," + dtFechaCont.getDay() + "," ;
	                sSql = sSql + dtFechaCont.getYear() + "," + dtFechaCont.getMonth() + "," + dtFechaCont.getDay() + "," ;
	                sSql = sSql + dtFechaCont.getYear() + "," + dtFechaCont.getMonth() + "," ;+ dtFechaCont.getDay() + "," ;
	                sSql = sSql + "'CONTABILIZADO'," + "0," + "0," + "0," + "28)" ;
                
                oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
                //		DoEvents
                bEjecuto = oAccesoBD.EjecutaTransaccion(sSql, sMsgErr) ;
                //		DoEvents
                
                if (!bEjecuto) { Err.Raise -1, , "Error: Al tratar de insertar el deposito del contrato : " 
                + oFiso.getMvarlNumFiso() + "\n" +
                " Ya se ejecut� la contabilizaci�n de la devoluci�n pero no se registr� el dep�sito."
                }

                if (oAccesoBD != null) { oAccesoBD = null ; }
                
    	    	resultado = true ;
    	    }
    	    else {
    	    	//Erase oDatosMov
    		    for(int i=0; i<oDatosMov.length; i++) {
    		    	oDatosMov[i] = null ;
    		    }
    		    if (oDetMov != null){ oDetMov = null ; }
    		    if (oFiso != null){ oFiso = null ; }
    		    if (oSFiso != null){ oSFiso = null ; }
    		    if (oContable != null){ oContable = null ; }
    		    if (oServGnrls != null){ oServGnrls = null ; }
    		    if (oFechas != null){ oFechas = null ; }
    		    if (rsCuenta != null){ rsCuenta = null ; }
    		    sMsgErr = sMsgErr + "\n" + 
    		             "Fuente : " + m_sObjectName + ".ContabilizaDevoluciones" ;
    		    return resultado ;
    	    }
    	    
    	    //Erase oDatosMov
		    for(int i=0; i<oDatosMov.length; i++) {
		    	oDatosMov[i] = null ;
		    }
		    
    	    if (oDetMov != null){ oDetMov = null ; }
    	    if (oFiso != null){ oFiso = null ; }
    	    if (oSFiso != null){ oSFiso = null ; }
    	    if (oContable != null){ oContable = null ; }
    	    if (rsCuenta != null){ rsCuenta = null ; }
    	    
		}catch(Exception ex) {
			
			//Erase oDatosMov
		    for(int i=0; i<oDatosMov.length; i++) {
		    	oDatosMov[i] = null ;
		    }
		    if (oDetMov != null){ oDetMov = null ; }
		    if (oFiso != null){ oFiso = null ; }
		    if (oSFiso != null){ oSFiso = null ; }
		    if (oContable != null){ oContable = null ; }
		    if (oServGnrls != null){ oServGnrls = null ; }
		    if (oFechas != null){ oFechas = null ; }
		    if (rsCuenta != null){ rsCuenta = null ; }
		    sMsgErr = sMsgErr + "\n" + ex + "\n" + 
		             "Fuente : " + m_sObjectName + ".ContabilizaDevoluciones" ;
            return resultado ;
		}
		
		return resultado;
	}
	
	//'************* Implementaciones Sin Interfaz ******************'
	//'Obtiene el tipo de la Persona en Cadena
	
	private String ObtenStrPersona(int iTipo) {
		
		String resultado = "" ;
		
		switch(iTipo) {
			case m_iFIDEICOMITENTE:{
				resultado = "FIDEICOMITENTE" ;
			}
			case m_iFIDEICOMISARIO:{
				resultado = "FIDEICOMISARIO" ;
			}
			case m_iTERCERO:{
				resultado = "TERCERO" ;
			}
			case m_iCARGOFONDO:{
				resultado = "CARGO AL FONDO" ;
			}
		}
		
		return resultado ;
	}
	
	//'Arma la Operaci�n de Cobro de Comisiones
	
	private String ArmaOpeCobroComisiones(fiduEstrucContables.DatosFiso oFiso, String sPersona, String sFormaPago,
			String sConcepto, int iMoneda, int iIntermed, long lCtoInver, String sMsgServ, String sMsgErr){
		
		String resultado = "" ;
		
		IfiduGenerales.IGenerales oServGnrls ;
		String sFuncion ;
	    int iFuncion ;
	    int iTipoIVA ;
	    int iConcepto ;
	    String sAnticipado ;
	    int iTipoProducto ;
	    int iFormaPago ;
	    String sSql ;
	    ADODB.Recordset rsAnticipado = new ADODB.Recordset() ;
		ADODB.Recordset rsProducto = new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos oAccesoBD ;
		try {
			//'Inicializacion de Mensajes de Error y de Servicio
		    sMsgServ = "" ;
		    sMsgErr = "" ;
		    
		    //'' rac Cambios en el select 05/08/2003
	        sSql = " SELECT PAC_CVE_PERIOD_COB,PAC_INTERMED,PAC_CTO_INVER " + " FROM   PACAHON" ;
			sSql = sSql + " WHERE  PAC_NUM_CONTRATO   = " + oFiso.getMvarlNumFiso() ;
			sSql = sSql + " AND    PAC_CVE_ST_PACAHON = 'ACTIVO'" ;
			
			oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
		    //		DoEvents
		    rsAnticipado = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgErr) ;
		    //		DoEvents
		    if (sMsgErr.length() > 0) {
			    if (rsAnticipado != null) { rsAnticipado = null ; }
			    if (rsProducto != null) { rsProducto = null ; }
			    if (oAccesoBD != null) { oAccesoBD = null ; }
			    if (oServGnrls != null) { oServGnrls = null ; }
			    sMsgErr = sMsgErr + "\n" + /*ex + "\n" +*/ "Fuente : " + m_sObjectName + ".ArmaOpeCobroComisiones" ;
	            return resultado ;
		    }
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    
		    if (!rsAnticipado.EOF) {
		    //'' rac se carga Intermed y Ctoinver 05/08/2003
	            iIntermed = 0 ;
	            lCtoInver = 0 ;
	            if (rsAnticipado("PAC_INTERMED") + "" != "") { iIntermed = rsAnticipado("PAC_INTERMED") ; }
	            if (rsAnticipado("PAC_CTO_INVER") + "" != "") { lCtoInver = rsAnticipado("PAC_CTO_INVER") ; }
		    //''
	            if ((rsAnticipado("PAC_CVE_PERIOD_COB").indexOf("ADELANTADA") > 0) || 
	               (rsAnticipado("PAC_CVE_PERIOD_COB").indexOf("ANTICIPADA") > 0)) {   //'Ojo Robert: ADELANTADA o ANTICIPADA
	                sAnticipado = " ANT" ;
	            }
	            else {
	                sAnticipado = "" ;
	            }
		    }
	        else {
	        	sMsgServ = "No se pudo armar el N�mero de Operaci�n." ;
			    if (rsAnticipado != null) { rsAnticipado = null ; }
			    if (rsProducto != null) { rsProducto = null ; }
			    if (oAccesoBD != null) { oAccesoBD = null ; }
			    if (oServGnrls != null) { oServGnrls = null ; }
			    sMsgErr = sMsgErr + "\n" + /*ex + "\n" +*/ "Fuente : " + m_sObjectName + ".ArmaOpeCobroComisiones" ;
	            return resultado ;
	        }
		    
		    //Linea 575 VB archivo Contabilizador.cls
		    if (rsAnticipado != null) { rsAnticipado = null ; }
		    
		    sSql = " SELECT PRO_NUM_PROD_ESTA" + " FROM   CONTRATO, PRODUCTO, PRODESTA" ;
	    	sSql = sSql + " WHERE  CTO_NUM_PRODUCTO  = PRL_NUM_PRODUCTO" ;
    	    sSql = sSql + " AND    PRL_NUM_PROD_ESTA = PRO_NUM_PROD_ESTA" ;
    	    sSql = sSql + " AND    CTO_NUM_CONTRATO  = " + oFiso.getMvarlNumFiso() ;
		    
    	    oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
    	    //		DoEvents
    	    rsProducto = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgErr) ;
    	    //		DoEvents
		    
    	    if (sMsgErr.length() > 0) {
			    if (rsAnticipado != null) { rsAnticipado = null ; }
			    if (rsProducto != null) { rsProducto = null ; }
			    if (oAccesoBD != null) { oAccesoBD = null ; }
			    if (oServGnrls != null) { oServGnrls = null ; }
			    sMsgErr = sMsgErr + "\n" + /*ex + "\n" +*/ "Fuente : " + m_sObjectName + ".ArmaOpeCobroComisiones" ;
	            return resultado ;
    	    }
    	    if (oAccesoBD != null) { oAccesoBD = null ; }
    	    
    	    if (!rsProducto.EOF) {
    	    	iTipoProducto = rsProducto("PRO_NUM_PROD_ESTA") ;
    	    }
	        else {
	            sMsgServ = "No se pudo armar el N�mero de Operaci�n." ;
	            //Tanto en este como en los anteriores de este tipo, nose si deberia hacer el return o guardar valor
	            //en una variable para que continue el codigo, ya que debajo se pone una variable a null.
			    if (rsAnticipado != null) { rsAnticipado = null ; }
			    if (rsProducto != null) { rsProducto = null ; }
			    if (oAccesoBD != null) { oAccesoBD = null ; }
			    if (oServGnrls != null) { oServGnrls = null ; }
			    sMsgErr = sMsgErr + "\n" + /*ex + "\n" +*/ "Fuente : " + m_sObjectName + ".ArmaOpeCobroComisiones" ;
	            return resultado ;
	        }
    	    
    	    if (rsProducto != null) { rsProducto = null ; }
    	    
    	    sFuncion = "COBRO COMISIONES" + sAnticipado + " " + sPersona ;
    	    iTipoIVA = oFiso.getMvariIVAEspecial() + 1 ;
    	    
    	    oServGnrls = new IfiduGenerales.Generales() ;
    	    //'Tipo de honorario
    	    oServGnrls.BuscaNumClaves("92", sFuncion, iFuncion) ;
    	    //'Concepto
    	    oServGnrls.BuscaNumClaves("93", sConcepto, iConcepto) ;
    	    //'Forma de pago
    	    oServGnrls.BuscaNumClaves("6", sFormaPago, iFormaPago) ;
    	    
    	    if (oServGnrls != null) { oServGnrls = null ; }
    	    
    	    //Estos format me los he ido encontrando a lo largo de la traduccion y estoy pensando en ponerlos
    	    //Los tipo de moneda como float y estos asecas como ah� abajo.
		    //resultado = "5" + Format(iFuncion, "00") + Format(iTipoIVA, "0") + Format(iConcepto, "0") +
            //        Format(iFormaPago, "0") + Format(iMoneda, "00") + Format(iTipoProducto, "00") ;
    	    resultado = "5" + iFuncion + iTipoIVA + iConcepto + iFormaPago + iMoneda + iTipoProducto ;
		    
		
		}catch(Exception ex) {
		    if (rsAnticipado != null) { rsAnticipado = null ; }
		    if (rsProducto != null) { rsProducto = null ; }
		    if (oAccesoBD != null) { oAccesoBD = null ; }
		    if (oServGnrls != null) { oServGnrls = null ; }
		    sMsgErr = sMsgErr + "\n" + ex + "\n" + "Fuente : " + m_sObjectName + ".ArmaOpeCobroComisiones" ;
            return resultado ;
		}
		return resultado ;
	}
	
	//'Arma la Operaci�n de Devoluci�n de Pagos
	private String ArmaOpeDevolPagos(String sConcepto, String sFormaDeposito, int iMoneda, 
			String sMsgServ, String sMsgErr) {
		String resultado = "" ;
		
		IfiduGenerales.IGenerales oServGnrls = null ;
		String sOrigen ;
	    int iConcepto = 0 ;
	    int iFormaDeposito = 0 ;
	    int iOrigen = 0 ;
	    
	    try {
	    	
	    	//'Inicializacion de Mensajes de Error y de Servicio
	        sMsgServ = "" ;
	        sMsgErr = "" ;
	        
	        sOrigen = "PROPIOS" ;      // 'Siempre fijo para estas devoluciones
	        
	        oServGnrls = new IfiduGenerales.Generales() ;
    	    //'Concepto de Aportaci�n
    	    oServGnrls.BuscaNumClaves("75", sConcepto, iConcepto) ;
    	    //'Forma del Dep�sito
    	    oServGnrls.BuscaNumClaves("74", sFormaDeposito, iFormaDeposito) ;
    	    //'Origen de los Recursos
    	    oServGnrls.BuscaNumClaves("82", sOrigen, iOrigen) ;
    	    if (oServGnrls != null) { oServGnrls = null ; }
	    	
    	    resultado = "1" + iConcepto + iFormaDeposito + iMoneda + iOrigen + "0" ;
    	    
	    }catch(Exception ex) {
	    	
		    if (oServGnrls != null) { oServGnrls = null ; }
		    sMsgErr = sMsgErr + "\n" + ex + "\n" +  "Fuente : " + m_sObjectName + ".ArmaOpeDevolPagos" ;
		    return resultado ;
	    }
		
		return resultado ;
	}
	
	private void GeneraDatoval(int iNumDatos) {
		
	    oDatosMov = new DatosMovimiento[iNumDatos] ;
		
	    for(int i = 0; i<iNumDatos; i++) {
	    	oDatosMov[i].setMvariNumDato(i); 
	        oDatosMov[i].setMvarsConcepto(sTipDatos[i]);
	        oDatosMov[i].setMvarvValor(vValDatos[i]);
	    }
	    
	}
	
	private int ObtenNumPersona(String sTipoPersona){
	   //' Nuevo servicio privado rac
		int resultado = 0 ;
		
		switch(sTipoPersona) {
			case "FIDEICOMITENTE":{
				resultado = m_iFIDEICOMITENTE ;
			}
			case "FIDEICOMISARIO":{
				resultado = m_iFIDEICOMISARIO ;
			}
			case "TERCERO":{
				resultado = m_iTERCERO ;
			}
			case "CARGO AL FONDO":{
				resultado = m_iCARGOFONDO ;
			}
			default:{
				resultado = m_iFIDEICOMITENTE ;
			}
		}
		
		return resultado ;
	}
	
	//'Arma la Operaci�n de Condonaciones y Cancelaciones RAC
	private String ArmaOpeCanCon(fiduEstrucContables.DatosFiso oFiso, String sFuncionContable, String sPersona, 
							String sConcepto, int iMoneda, String sStatusAdeudo, String sMsgServ, String sMsgErr) {
		
		String resultado = null ;
		
		//'' Nuevo Servicio rac
	    //'' Privado
		IfiduGenerales.IGenerales oServGnrls ;
	    int iFuncion ;
	    int iTipoIVA ;
	    int iConcepto ;
	    String sAnticipado ;
	    int iTipoProducto ;
	    String sSql ;
	    ADODB.Recordset rsProducto = new ADODB.Recordset() ;
		IfiduBaseDatos.IBaseDatos oAccesoBD ;
	    int iStatus ;
	    int iBanco ;
		
	    try {
	    	
	    	//'Inicializacion de Mensajes de Error y de Servicio
	        sMsgServ = "" ;
	        sMsgErr = "" ;

	        sSql = " SELECT PRO_NUM_PROD_ESTA" + " FROM   CONTRATO, PRODUCTO, PRODESTA" ;
	        sSql = sSql + " WHERE  CTO_NUM_PRODUCTO  = PRL_NUM_PRODUCTO" + " AND    PRL_NUM_PROD_ESTA = PRO_NUM_PROD_ESTA" ;
	        sSql = sSql + " AND    CTO_NUM_CONTRATO  = " + oFiso.getMvarlNumFiso() ;
	    	
	        oAccesoBD = new IfiduBaseDatos.BaseDatos() ;
    	    //		DoEvents
    	    rsProducto = oAccesoBD.EjecutaSeleccion(sSql, 0, sMsgErr) ;
    	    //		DoEvents
    	    if (sMsgErr.length() > 0) {
    	    	if (rsProducto != null) { rsProducto = null ; }
    		    if (oAccesoBD != null)  { oAccesoBD = null ; }
    		    if (oServGnrls != null) { oServGnrls = null ; }
    		    sMsgErr = sMsgErr + "\n" + /*ex + "\n" +*/ "Fuente : " + m_sObjectName + ".ArmaOpeCanCon" ;
    		    
                return resultado ;
    	    }
    	    if (oAccesoBD != null) { oAccesoBD = null ; }
    	    
    	    if (!rsProducto.EOF) {
    	    	iTipoProducto = rsProducto("PRO_NUM_PROD_ESTA") ;
    	    }
	        else {
	            sMsgServ = "No se pudo armar el N�mero de Operaci�n." ;
	            if (rsProducto != null) { rsProducto = null ; }
    		    if (oAccesoBD != null)  { oAccesoBD = null ; }
    		    if (oServGnrls != null) { oServGnrls = null ; }
    		    sMsgErr = sMsgErr + "\n" + /*ex + "\n" +*/ "Fuente : " + m_sObjectName + ".ArmaOpeCanCon" ;
    		    
                return resultado ;
	        }
    	    
    	    if (rsProducto != null) { rsProducto = null ; }
    	    	    
    	    iTipoIVA = oFiso.getMvariIVAEspecial() + 1 ;
    	    iBanco = oFiso.getMvariNivelEstructura1() ;
    	    
    	    oServGnrls = new IfiduGenerales.Generales() ;
    	    //'Tipo de honorario
    	    oServGnrls.BuscaNumClaves("92", sFuncionContable, iFuncion) ;
    	    //'Concepto
    	    oServGnrls.BuscaNumClaves("93", sConcepto, iConcepto) ;
    	    //'Status Adeudo
    	    oServGnrls.BuscaNumClaves("8", sStatusAdeudo, iStatus) ;
    	    if (oServGnrls != null) { oServGnrls = null ; }
    	    
    	    resultado = "5" +iFuncion + iTipoIVA + iConcepto + iMoneda + "0000" +iStatus + iBanco ;
	        
	    }catch(Exception ex) {
	    	
		    if (rsProducto != null) { rsProducto = null ; }
		    if (oAccesoBD != null)  { oAccesoBD = null ; }
		    if (oServGnrls != null) { oServGnrls = null ; }
		    sMsgErr = sMsgErr + "\n" + ex + "\n" + "Fuente : " + m_sObjectName + ".ArmaOpeCanCon" ;
		    
            return resultado ;
	    	
	    }
	    
		return resultado ;
	}
	
	
	
	
}
