package poleador;

public class TduDetalleDeposito {

	String sFolio;                    //'1  N�MERO DE FOLIO CONTABLE
    String sContrato;                 //'2  N�MERO DE CONTRATO
    String sSubFiso;                  //'3  N�MERO DE SUBCONTRATO
    String sSubPrograma;     	      //'4  N�MERO DE SUBPROGRAMA
    String sFolioOperativo;           //'9  FOLIO OPERATIVO, OBTENERLO CON GETFOLIO
    String sImporte;                  //'11 IMPORTE DE LA LIQUIDACION
    String sParidad;                  //'12 TIPO DE CAMBIO
    String sNumeroMoneda;             //'14 N�MERO DE MONEDA
    String sNumeroBanco;              //'17 N�MERO DE BANCO
    String sNombreBanco;              //'18 NOMBRE DEL BANCO
    String sNumeroCuenta;             //'19 N�MERO DE CUENTA
    String sNumeroAbono;        
    String sProveedor;                //'23 INTERMEDIARIO
    String sCtoInversion;             //'24 CONTRATO DE INVERSION
    String sCodigoTrans;              //'28 CODIGO TRANS
    String sNumeroOperacion;          //'33 N�MERO DE OPERACI�N
    String sUsuario;                  //'34 USUARIO
    String sPassword;                 //'35 PASSWORD
    String sFuncionario;              //'37 FUNCIONARIO
    Boolean bEmergente;               //'38 EMERGENTE
    String sError;                    //'39 DESCRIPCION DE ERROR
    
	public String getsFolio() {
		return sFolio;
	}
	public void setsFolio(String sFolio) {
		this.sFolio = sFolio;
	}
	public String getsContrato() {
		return sContrato;
	}
	public void setsContrato(String sContrato) {
		this.sContrato = sContrato;
	}
	public String getsSubFiso() {
		return sSubFiso;
	}
	public void setsSubFiso(String sSubFiso) {
		this.sSubFiso = sSubFiso;
	}
	public String getsSubPrograma() {
		return sSubPrograma;
	}
	public void setsSubPrograma(String sSubPrograma) {
		this.sSubPrograma = sSubPrograma;
	}
	public String getsFolioOperativo() {
		return sFolioOperativo;
	}
	public void setsFolioOperativo(String sFolioOperativo) {
		this.sFolioOperativo = sFolioOperativo;
	}
	public String getsImporte() {
		return sImporte;
	}
	public void setsImporte(String sImporte) {
		this.sImporte = sImporte;
	}
	public String getsParidad() {
		return sParidad;
	}
	public void setsParidad(String sParidad) {
		this.sParidad = sParidad;
	}
	public String getsNumeroMoneda() {
		return sNumeroMoneda;
	}
	public void setsNumeroMoneda(String sNumeroMoneda) {
		this.sNumeroMoneda = sNumeroMoneda;
	}
	public String getsNumeroBanco() {
		return sNumeroBanco;
	}
	public void setsNumeroBanco(String sNumeroBanco) {
		this.sNumeroBanco = sNumeroBanco;
	}
	public String getsNombreBanco() {
		return sNombreBanco;
	}
	public void setsNombreBanco(String sNombreBanco) {
		this.sNombreBanco = sNombreBanco;
	}
	public String getsNumeroCuenta() {
		return sNumeroCuenta;
	}
	public void setsNumeroCuenta(String sNumeroCuenta) {
		this.sNumeroCuenta = sNumeroCuenta;
	}
	public String getsNumeroAbono() {
		return sNumeroAbono;
	}
	public void setsNumeroAbono(String sNumeroAbono) {
		this.sNumeroAbono = sNumeroAbono;
	}
	public String getsProveedor() {
		return sProveedor;
	}
	public void setsProveedor(String sProveedor) {
		this.sProveedor = sProveedor;
	}
	public String getsCtoInversion() {
		return sCtoInversion;
	}
	public void setsCtoInversion(String sCtoInversion) {
		this.sCtoInversion = sCtoInversion;
	}
	public String getsCodigoTrans() {
		return sCodigoTrans;
	}
	public void setsCodigoTrans(String sCodigoTrans) {
		this.sCodigoTrans = sCodigoTrans;
	}
	public String getsNumeroOperacion() {
		return sNumeroOperacion;
	}
	public void setsNumeroOperacion(String sNumeroOperacion) {
		this.sNumeroOperacion = sNumeroOperacion;
	}
	public String getsUsuario() {
		return sUsuario;
	}
	public void setsUsuario(String sUsuario) {
		this.sUsuario = sUsuario;
	}
	public String getsPassword() {
		return sPassword;
	}
	public void setsPassword(String sPassword) {
		this.sPassword = sPassword;
	}
	public String getsFuncionario() {
		return sFuncionario;
	}
	public void setsFuncionario(String sFuncionario) {
		this.sFuncionario = sFuncionario;
	}
	public Boolean getbEmergente() {
		return bEmergente;
	}
	public void setbEmergente(Boolean bEmergente) {
		this.bEmergente = bEmergente;
	}
	public String getsError() {
		return sError;
	}
	public void setsError(String sError) {
		this.sError = sError;
	}
}
