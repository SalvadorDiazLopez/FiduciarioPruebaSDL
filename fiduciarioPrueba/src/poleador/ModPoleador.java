package poleador;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import IfiduBaseDatos.BaseDatos;
import IfiduBitacora.Bitacora;
import Modulos.ADODB;
import Modulos.IfiduBaseDatos;
import PoleadorTF.IfiduBitacora;


public class ModPoleador {

	//DECLARACI�N DE finalANTES
	//CONSTANTES PARA CONTABILIZAR
	public final String sconstUSUARIO = "999";
	public final int iMONEDANACIONAL = 1 ;
	public final int iCTASPERSONALES = 1 ;
	public final int iIDCTAJUMBO = 1 ;
	public final String sInterfaz = "2" ; //SE VA A UTILIZAR LA DE CUENTAS PERSONALES
	public String sSiguienteDescarga ;
	public Date dtFechaUltimaDescarga ;
	public boolean bHayDescargasPrevias ;
	public boolean bDiaHabil ;
	public final String sRECHAZADO = "R" ;
	public final String sfinalUSUARIO = "999" ;
	public final String BI_TERMINAL = "MAESTRA" ;
	public final String sCOMPONENTE = "fiduPoleadorCtaEje" ;
	public final String sERRGRAL = "EG" ;
	public final String sERRINTER = "EI" ;
	public final String sERRCONTA = "EC" ;
	public final String sSINERROR = "DI" ;

	//finalANTES PARA DESCARGA DE MOVIMIENTOS
	public final String gcsOrdenacionDesc = "D" ;
	public final String gcsOrdenacionAsc = "A" ;
	public final String gcsCargo = "D" ;
	public final String gcsAbono = "H" ;
	public final String gcsAnulado = "A" ;
	public final String gcsSignoPositivo = " " ;
	public final String gcsSignoNegativo = "-" ;
	public final String gcsMasPaginas = "S" ;
	public final String gcsSinPaginas = "N" ;
	public final String gcsConError = "E" ;
	public String sgNumeroUsuario ;
	
	TduDetalleDeposito tduDetalleDeposito ;
	
	public String ObtenerFechaActual (String sMsgError) {
		try {
			Date sFecha = new Date();
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");			
			return formateador.format(sFecha);
		}
		catch(Exception e) {
			sMsgError = e.getMessage();
			return "";
		}
	}
	
	public boolean InsertaMovimientos (String sIdCuenta, ADODB.Recordset orsRksetMovtos, String sMsgError) {
		
		//private NamedParameterJdbcTemplate jdbcTemplate;
		
		boolean bExito ;
		
		try {
			sMsgError = "" ;
			//orsRksetMovtos.Filter = adFilterNone ; // 0	No filter. This value removes the current filter and restores all records to view.
			if(orsRksetMovtos.BOF && orsRksetMovtos.EOF) {
				return true ;
			}
			
			/*Suppose a Recordset contains three fields named firstName, middleInitial, and lastName. 
			 * Set the Sort property to the string, "lastName DESC, firstName ASC", 
			 * which will order the Recordset by last name in descending order, then by first name in ascending order. 
			 * The middle initial is ignored.*/
			orsRksetMovtos.Sort = "NumerMov ASC" ;
	        //orsRksetMovtos.MoveFirst ; // Use the MoveFirst method to move the current record position to the first record in the Recordset.
			        
			while(!orsRksetMovtos.EOF) {
				bExito = GrabaMovto(sIdCuenta, orsRksetMovtos, sMsgError) ;
				if(sMsgError != "") {
					sMsgError = "Error al grabar movto. :" + orsRksetMovtos("NumerMov") + " " + sMsgError ;
					GuardaBitacora("InsertaMovtosYReferencias", "frmCargaMovtos", sMsgError) ;
					//Err.Raise 1, , sMsgError Aqui saltaremos al catch.						

			        return false ;
				}
				//orsRksetMovtos.MoveNext ; Con esto lo que estamos haciendo es recorrer la tabla hash recuperada de la base de datos.
			}
			return true ; 
			
		}catch(Exception e) {
			sMsgError = e.getMessage() ;
	        GuardaBitacora("InsertaMovtosYReferencias", "frmCargaMovtos", sMsgError) ;

	        return false ;
		}
	}
	
	public boolean GrabaMovto ( String sIdCuenta, ADODB.Recordset orsRksetMovtos, String sMsgError) {
		
		String sSql ;
		IfiduBaseDatos.IBaseDatos obase ;
	    boolean bExito ;
	    String sFuncionario = "" ;
	    String sContrato ;
	    String sNumerodeOperacion ;
	    String sReferenciaEje ;
	    String sEstatus = "" ;
	    String sSubContrato ;
	    String sCuentaEje = "" ;
	    String sErrorAGrabar = "" ;
	    
	    String sFechaOper ;
	    String sFechaValor ;
	    String sHoraOper ;
	    String sTipoMovto ;
	    String sImporte ;
	    String sCodigo ;
	    String sNumCheque ;
	    String sReferencia ;
	    String sConcepto ;
	    String sReferAmpliada ;
	    String sProducto ;
	    String sSubProducto ;
	    String sAutoManual ;
	    String sContrapartida ;
	    String sAnulado ;
	    String sEntidadOrigen ;
	    String sCentroOrigen ;
	    String sUsuarioOrigen ;
	    String sTerminal ;
	    String sTransaccion ;
	    int iLongImporte ;
	    
		String sContratoConc="";
		int iContratoCAD;
		
	    try {
	    	sMsgError = "" ;
	    	
	    	int flag=0;
	    	//sFechaOper = Replace(orsRksetMovtos("FechaOper"), Chr(0), " ")
	    			//El caracter 0 de lo que devuelva orsRksetMovtos("XX") sera reemplazado por un espacio.
            //sFechaValor = Replace(orsRksetMovtos("FechaValor"), Chr(0), " ")
            //sHoraOper = Replace(orsRksetMovtos("HoraOper"), Chr(0), " ")
	        String sSubCadena = sHoraOper.substring(0,2);//LEFT
	        String sSubCadena2 = sHoraOper.substring(sHoraOper.length()-2,sHoraOper.length());//RIGHT
	        sHoraOper = sSubCadena+":"+sSubCadena2;//CONCATENA AMBOS
	        System.out.println(sHoraOper);
	        if (sTipoMovto=="D"){
	        	return true; //PORQUE LOS CARGOS NO SE GRABAN
	        }
	        else {
	        	sTipoMovto="A";
	        }
	        /*
	        //El caracter 0 de lo que devuelva orsRksetMovtos("XX") sera reemplazado por un espacio.
	        sImporte = Replace(orsRksetMovtos("Importe"), Chr(0), " ")
            sCodigo = Replace(orsRksetMovtos("Codigo"), Chr(0), " ")
            sNumCheque = Replace(orsRksetMovtos("Cheque"), Chr(0), " ")
            
            sReferencia = Replace(orsRksetMovtos("RefInterna"), Chr(0), " ") ;
            Left(
            sReferencia = sReferencia.substring(0, 14) ;
            
            sConcepto = Replace(orsRksetMovtos("RefInterna"), Chr(0), " ") ;
            Right(
            sConcepto = sConcepto.substring(sConcepto.length()-24, sConcepto.length()) ;
            
            //El caracter 0 de lo que devuelva orsRksetMovtos("XX") sera reemplazado por un espacio.
            sReferAmpliada = Replace(orsRksetMovtos("Observaciones"), Chr(0), " ")
            sProducto = Replace(orsRksetMovtos("Producto"), Chr(0), " ")
            sSubProducto = Replace(orsRksetMovtos("SubProdu"), Chr(0), " ")
            sAutoManual = Replace(orsRksetMovtos("IndAOM"), Chr(0), " ")
            sContrapartida = Replace(orsRksetMovtos("Contrapartida"), Chr(0), " ")
            sAnulado = Replace(orsRksetMovtos("IndAnu"), Chr(0), " ")
            sEntidadOrigen = Replace(orsRksetMovtos("EntidadAct"), Chr(0), " ")
            sCentroOrigen = Replace(orsRksetMovtos("CentroAct"), Chr(0), " ")
            sUsuarioOrigen = Replace(orsRksetMovtos("UserIDAct"), Chr(0), " ")
            
            sTerminal = Replace(orsRksetMovtos("NetNameAct"), Chr(0), " ") ;
            sTransaccion = Replace(orsRksetMovtos("NetNameAct"), Chr(0), " ") ;
            Left(
            sTerminal = sTerminal.substring(0, 3) ;
            Right(
            sTransaccion = sTransaccion.substring(sTransaccion.length()-4, sTransaccion.length() ) ;
            
            //El caracter 0 de lo que devuelva orsRksetMovtos("XX") sera reemplazado por un espacio.
            orsRksetMovtos("Importe") = Replace(orsRksetMovtos("Importe"), Chr(0), " ")
            
            iLongImporte = Len(orsRksetMovtos("Importe"))
	        */
	        if (iLongImporte > 2){
	        	//sSubCadena = orsRksetMovtos("Importe").substring(0,iLongImporte - 2);//LEFT
		        //sSubCadena2 = orsRksetMovtos("Importe").substring(orsRksetMovtos("Importe").length()-2,orsRksetMovtos("Importe").length());//RIGHT
		        sImporte = sSubCadena+"."+sSubCadena2;//CONCATENA AMBOS
	        }
	        else {
	        	sImporte = "0."+sImporte;
	        }
	        sReferenciaEje = "I" ;
	        
	        
	        /*'''' INI GCLD 2008-01-25 Permitir que entren transacciones al NSF desde cualquier otra forma
	        ''''                 diferente a YMF1
	                    
	        '''''            'INI GCLD 15/06/2007 Cambio Temporal para identificar la transacci�n de Origen, ya que BG env�a vac�os
	        '''''            '                    los datos cuando son movimientos del d�a.
	        '''''            If Trim(sTransaccion) = Empty Then sTransaccion = "YMF1"
	        '''''            'FIN GCLD 15/06/2007
	        '''' FIN GCLD 2008-01-25
	        */
	        
	        ValidaReferencia(sReferAmpliada, sTransaccion, sContrato, sSubContrato, sErrorAGrabar) ;
	        
	        if (sErrorAGrabar!=""){
	        	sEstatus="EG";
	        }
	        if (sEstatus=="") {
	        	if (Integer.parseInt(Val(sContrato))>0) {
	        		sFuncionario = ObtenFuncionario(sContrato, sErrorAGrabar) ;
	                if (sErrorAGrabar != "") { sEstatus = "EG" ; }
	        	}
	        	else {
	                sFuncionario = "" ;
	                sEstatus = "EG" ;
		        	System.out.println(sContratoConc);
	        	}
	        	
	        	sSubCadena = sHoraOper.substring(18,28);//LEFT
	        	
	        	if (!ExisteCuentaEje(sContrato, sSubContrato, sCuentaEje)) {
	                sReferenciaEje = "I" ;
	                sEstatus = "EG";
	        	}
	        	else {
	        		sReferenciaEje = "V" ;
	        	}	
        	}
	        
	        sSql = "INSERT INTO FID_EJE_DEPOSITOS_JUMBO";
	        sSql = sSql + "(JBO_ID" ;
	        sSql = sSql + " ,DEJ_MOVTOA_BGW";
	        sSql = sSql + " ,DEJ_MOVTOC_BGW";
	        sSql = sSql + " ,DEJ_NUM_CONTRATO";
	        sSql = sSql + " ,DEJ_SUB_CONTRATO";
	        sSql = sSql + " ,DEJ_COD_FUNCIONARIO";
	        sSql = sSql + " ,DEJ_IMPORTE";
	        sSql = sSql + " ,DEJ_SALDO_DISPERSION";
	        sSql = sSql + " ,DEJ_CUENTA_EJE";
	        sSql = sSql + " ,DEJ_FECHR_DESCARGA";
	        sSql = sSql + " ,DEJ_FOLIO_OPERA";
	        sSql = sSql + " ,DEJ_ID_OPERACION";
	        sSql = sSql + " ,DEJ_FOLIO_AUX_FID";
	        sSql = sSql + " ,DEJ_REFERENCIA_EJE";
	        sSql = sSql + " ,DEJ_ESTATUS";
	        sSql = sSql + " ,DEJ_DESC_ERROR";
	        sSql = sSql + " ,DEJ_TIPO_MOVTO";
	        sSql = sSql + " ,DEJ_FECHA_OPER";
	        sSql = sSql + " ,DEJ_FECHA_VALOR";
	        sSql = sSql + " ,DEJ_HORA_OPER";
	        sSql = sSql + " ,DEJ_CLAVE_OPER";
	        sSql = sSql + " ,DEJ_NUMCHQ_FOLFICHA";
	        sSql = sSql + " ,DEJ_REFERENCIA";
	        sSql = sSql + " ,DEJ_CONCEPTO";
	        sSql = sSql + " ,DEJ_REFER_AMPLIADA";
	        sSql = sSql + " ,DEJ_ANULADO";
	        sSql = sSql + " ,DEJ_ENTIDAD_ORIG";
	        sSql = sSql + " ,DEJ_CENTRO_ORIG";
	        sSql = sSql + " ,DEJ_USUARIO_ORIG";
	        sSql = sSql + " ,DEJ_TERMINAL";
	        sSql = sSql + " ,DEJ_TRANSACCION)";

	        sSql = sSql + " VALUES(";
	        sSql = sSql + sIdCuenta; //'ID DE LA CUENTA
	        sSql = sSql + ", " + orsRksetMovtos("NumerMov"); //' N�MERO DE ABONO
	        sSql = sSql + ", NULL"; //'NUMERO DE CARGO
	        sSql = sSql + ", " + Val(sContrato) ; //'CONTRATO
	        sSql = sSql + ", " + Val(sSubContrato);// 'SUBCONTRATO
	        
            if (sFuncionario == "") {
            	sSql = sSql + ", NULL " ); // ' FUNCIONARIO 
            }
            else{
            	sSql = sSql + ", " + Val(sFuncionario) ); // ' FUNCIONARIO
            }
            
            sSql = sSql + ", " + sImporte ;   //' IMPORTE
            sSql = sSql + ", " + sImporte ;  //' SALDO DE DISPERSI�N
            sSql = sSql + ", '" + sCuentaEje + "'" ; //'CUENTA EJE
            sSql = sSql + ", CURRENT TIMESTAMP " ; //' FECHA DE DESCARGA
            sSql = sSql + ", NULL" ; //'FOLIO DE OPERACI�N
            sSql = sSql + ", NULL" ; // 'N�MERO DE OPERACI�N
            sSql = sSql + ", NULL" ; //'FOLIO AUX_FID
            sSql = sSql + ", '" + sReferenciaEje + "'" ; //'REFERENCIA EJE
            
            if (sEstatus != ""){
            	sSql = sSql + ", '" + sEstatus.toUpperCase() + "'" ; //'STATUS
            }
            else{
            	sSql = sSql + ",NULL" ; // 'STATUS
            }
            
            sSql = sSql + ", '" + sTipoMovto + "'" ;
            sSql = sSql + ", DATE('" + sFechaOper + "')" ;
            sSql = sSql + ", DATE('" + sFechaValor + "')" ;
            sSql = sSql + ", '" + sHoraOper + "'" ;
            sSql = sSql + ", '" + sCodigo + "'" ;
            sSql = sSql + ", '" + sNumCheque + "'" ;
            sSql = sSql + ", '" + sReferencia + "'" ;
            sSql = sSql + ", '" + sConcepto + "'" ;
            sSql = sSql + ", '" + sReferAmpliada + "'" ;
            sSql = sSql + ", '" + sAnulado + "'" ;
            sSql = sSql + ", '" + sEntidadOrigen + "'" ;
            sSql = sSql + ", '" + sCentroOrigen + "'" ;
            sSql = sSql + ", '" + sUsuarioOrigen + "'" ;
            sSql = sSql + ", '" + sTerminal + "'" ;
            sSql = sSql + ", '" + sTransaccion + "')" ;
	        
            obase = new BaseDatos() ;
            
            bExito = obase.EjecutaTransaccion(sSql, sMsgError) ;
            
            obase = null ;
            
            if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
            
            //orsRksetMovtos("MovtoCopiado") = "S" ;
            //orsRksetMovtos.Update ;
            
            return true ;
	        
		}catch(Exception e) {
			
			sMsgError = "Error al grabar mvto: " + orsRksetMovtos("NumerMov").trim() + ", de cta: " + orsRksetMovtos("Cuenta").trim() + ". " + e.getMessage() ;
            
            GuardaBitacora("GrabaMovto", "frmCargaMovtos", sMsgError) ;
            obase = null ;
            return false;
			
		}
	}
	
	public Boolean ExisteCuentaEje(String sContrato, String sSubContrato, String sCuentaEje ) {
		
		IfiduBaseDatos.IBaseDatos oDb = new IfiduBaseDatos.BaseDatos(); 
		//IBaseDatos oDb = new IBaseDatos(); 
		
		StringBuffer sSql = new StringBuffer();
		//ADOB.Recordset rsConsulta = new ADOB.Recordset();
		String sCuentaPU;
		String sMsgError;
		
		try {
			
			sSql.append( "SELECT CUE_NUM_CUENTA_PU AS CUENTA FROM FID_EJE_CUENTAS WHERE "
					+ "CTO_NUM_CONTRATO =" + sContrato + "AND CUE_SUB_CONTRATO =" 
					+ sSubContrato + "AND CUE_ESTATUS  = 'A'");
			
			//Set oDb = CreateObject("fiduBaseDatos.BaseDatos");
            //Set rsConsulta = oDb.EjecutaSeleccion(sSql, 0, sMsgError);
			
			if(sMsgError!=null) {
				//Err.Raise -1, sMsgError
			}
			oDb = null;
			if(rsConsulta.BOF && rsConsulta.EOF) {
				return true;
			}
			//A la espera de la base d e datos.................
			//sCuentaPU = trim(rsConsulta!CUENTA);
			String sSubCadena = sCuentaPU.substring(sCuentaPU.length(), 10);
			if (Val(sSubCadena) == Val(sCuentaEje)) {
				return true;
			}
		}catch(Exception t) {
			
			sMsgError = "Error al obtener cuenta eje:"+t.getMessage();
			oDb = null;
			GuardaBitacora("ExisteCuentaEje", sCOMPONENTE, sMsgError);
			return false;
		}
	}

	public String ObtenFuncionario(String sContrato, String sMsgError) {//356
	
		IfiduBaseDatos.IBaseDatos oDb;
		StringBuffer sSql = new StringBuffer();
		ADODB.Recordset rsConsulta;
		
		try {
			sSql.append(" SELECT  B.ATE_NUM_USUARIO AS FUNCIONARIO ");
	        sSql.append(" FROM CONTRATO A FULL OUTER JOIN FDATENCION B ");
	        sSql.append(" ON A.CTO_NUM_CONTRATO = B.ATE_NUM_CONTRATO  ");
	        sSql.append(" WHERE A.CTO_NUM_CONTRATO =  " + sContrato);
	        sSql.append(" AND (A.CTO_CVE_ST_CONTRAT = 'ACTIVO' OR  A.CTO_CVE_ST_CONTRAT = 'ANTEPROYECTO')");
	        sSql.append(" AND B.ATE_CVE_ST_ATENCION = 'AGENDA TITULAR'");
	        
	        oDb = new IfiduBaseDatos.BaseDatos() ;
	        
	        rsConsulta = oDb.EjecutaSeleccion(sSql, 0, sMsgError);
	        
	        if (sMsgError!="") {
	        	//Err.Raise 1, , sMsgError;
	        }
	        oDb = null;
	        
	        if (rsConsulta.BOF && rsConsulta.EOF) {
	        	//Err.Raise 1, , "No Existe funcionario para el contrato";
	        }
	        //A la espera de la base d e datos.................
			//ObtenFuncionario = rsConsulta!FUNCIONARIO;
	        
	        rsConsulta = null;
	                
		}catch(Exception t) {
			sMsgError = "Error al obtener el Funcionario"+t.getMessage();
			return "";
			oDb = null;
			GuardaBitacora("ObtenFuncionario",sCOMPONENTE, sMsgError);
		}
	}
	
	void GuardaBitacora (String sFuncion, String sForma, String sMensajeaGrabar) {
		Bitacora oBitacora;
		
		long lUsuario;
		boolean bError;
		
		try {
			lUsuario = Long.parseLong(sconstUSUARIO);
			oBitacora = new Bitacora();
			//Completar
			
			String sSubCadena = sFuncion.trim().substring(0,24);//LEFT
			String sSubCadena2 = sForma.trim().substring(0,49);//LEFT
			String sSubCadena3 = sMensajeaGrabar.trim().substring(0,199);//LEFT
			bError = oBitacora.GeneraBitacora(sSubCadena, sSubCadena2, sSubCadena3, BI_TERMINAL, lUsuario);
			oBitacora = null;			
		}catch(Exception e) {
			sMensajeaGrabar = e.getMessage();
			oBitacora = null;			
		}
		
	}	

	public boolean RealizaContabilidad(TduDetalleDeposito tduDetalleDepos, fiduEstrucGlobales.Cliente oCliente, String sErrorBitacora, String sEstatus) {
		
		IfiduGenerales.IGenerales oGener ;        
		fiduEstrucContables.DatosFiso oFiso ;           
		fiduEstrucContables.SubFiso oSubFiso ;        
		fiduEstrucContables.DatosMovimiento oDatMov[] ;
		fiduEstrucContables.DetMovimiento oDetMov ;      
		IfiduContabilidad.IContabilizador oConta ;          
		IfiduContabilidad.ICancelacion oCancela ;      
		IfiduFechas.IFechas oFecha ;
		
	    int iNumerodeDatos ;
	    long lFolio ;  
	    String sMsgServ ;
	    String sMsgErrCan ;
	    String sMsgError ;
	    boolean bEjecuto ;
	    String sMsgErrorBit ;
	    long lCodError ;
	    Date dtFechaCont ;
	    String sNumeroOperacion ;
	          
	    //'Ini Variable para la interfaz
	    int iInterfase ;
	    String sFolioSalInterf ;
	    boolean bExito ;
	    
	    IfiduInterfEntSal.ClsInterfEntSal oInterfEntSal ;
	    IfiduInterfEntSal.tduProcesaInterfaz tduLlenaValores ;
	    IfiduInterfEntSal.tduConfInterFid tduConfInterFid ;
	    
	    //'Fin Variable para la interfaz
		
	    try {
	    	
	    	sEstatus = "" ;
			sErrorBitacora = "" ;
			sMsgError = "" ;
			   
			oFecha = new IfiduFechas.Fechas() ;
			dtFechaCont = oFecha.dtContable ;
			oFecha = null ;
	
			//'1 Valida contrato y subcontrato
			
			oFiso = new fiduEstrucContables.DatosFiso() ;
			oSubFiso = new fiduEstrucContables.SubFiso() ;
			oGener = new IfiduGenerales.Generales() ;
	    	
			if (!oGener.VerificaContratoSrv(tduDetalleDepos.sContrato, true, oFiso, sMsgError, false)) {
				sEstatus = sERRGRAL ;
				//Err.Raise -777, , sMsgError
			}
			if (!oGener.VerificaSubContratoSrv(oFiso, Integer.parseInt(tduDetalleDepos.sSubFiso), true, oSubFiso, sMsgError, false)) {
				sEstatus = sERRGRAL ;
		        //Err.Raise -777, , sMsgError
			}
			
			//'2 OBTIENE N�MERO DE OPERACION
		    ObtenNumerodeOperacion(tduDetalleDepos.sContrato, sNumeroOperacion, sMsgError) ;
		    if (sMsgError != "") {
		    	sEstatus = sERRGRAL ;
				//Err.Raise -777, , sMsgError
		    }
		      
		    tduDetalleDepos.setsNumeroOperacion(sNumeroOperacion) ;
		    
		    //'3 Arma Datoval
	        ArmaDatosDatoval(oDatMov, tduDetalleDepos.sContrato, 
	        		tduDetalleDepos.sSubFiso, Integer.parseInt(tduDetalleDepos.sNumeroMoneda), 
	        		tduDetalleDepos.sImporte, tduDetalleDepos.sNumeroCuenta, iNumerodeDatos, sMsgError) ;
	        if (sMsgError != "") {
	        	sEstatus = sERRGRAL  ;
				//Err.Raise -777, , sMsgError
	        }
	        
	        //'4 Arma DetMov
	        
	        oDetMov = new fiduEstrucContables.DetMovimiento() ;
	        oDetMov.sStatus = "ACTIVO" ;
	        oDetMov.dImporte = tduDetalleDepos.sImporte ;
	        oDetMov.dtFechaMovimiento = dtFechaCont ;
	        oDetMov.iNumDatos = iNumerodeDatos ;
	        oDetMov.sNumOperacion = tduDetalleDepos.sNumeroOperacion ;
	        
	        //'5 LLENADO DE DATOS GENERALES PARA PROCESAR INTERFAZ
	        
	         //'SIEMPRE VA A SER A CUENTAS PERSONALES. UN CARGO
	         tduLlenaValores.tduEntradaCte.bCancelacion = false ;
	         tduLlenaValores.tduEntradaCte.sBanco = "" ;//vbNullString ;//'Banco va en nulo
	         tduLlenaValores.tduEntradaCte.sModalidad = "M" ;//'Siempre va a ser Masiva
	         tduLlenaValores.tduEntradaCte.tduDatosGrales.sDivisa = tduDetalleDepos.sNumeroMoneda ;
	         tduLlenaValores.tduEntradaCte.tduDatosGrales.sUsuario = tduDetalleDepos.sUsuario ;
	         tduLlenaValores.tduEntradaCte.sUsoFuturo3 = tduDetalleDepos.sPassword ;
	         tduLlenaValores.tduEntradaCte.sNumOperacion = tduDetalleDepos.sNumeroOperacion ;
	         tduLlenaValores.tduEntradaCte.tduDatosContrato.sContrato = tduDetalleDepos.sContrato ;
	         tduLlenaValores.tduEntradaCte.tduDatosContrato.sSubContrato = tduDetalleDepos.sSubFiso ;
	         tduLlenaValores.tduEntradaCte.tduDatosGrales.sImporte = tduDetalleDepos.sImporte ;
	         tduLlenaValores.tduEntradaCte.tduDatosGrales.sCuenta = tduDetalleDepos.sNumeroCuenta ;
	         //'El dato de abajo nunca se llena porque jam�s se concilia
	         //'tduLlenaValores.tduEntradaCte.sUsoFuturo = Trim(oDeposit.sNumAbonoConcilia) 'No. de Movimiento correspondiente al Abono para la Conciliaci�n Autom�tica.
	         //'5B) Procesamiento de la Interfaz
	         oInterfEntSal = new fiduInterfEntSal.InterfEntSal() ;
	         bExito = oInterfEntSal.ProcesaInterfaz(tduLlenaValores, tduConfInterFid, sMsgError) ;
	         if (!bExito && sMsgError.length() != 0) {
	        	 sEstatus = sERRINTER ;
				 //Err.Raise -666, , "No se pudo procesar la interfaz: " & sMsgError
	         }
	            
	         tduDetalleDepos.setbEmergente(tduLlenaValores.btxEmergente) ;
	                      
	        //'5C) En caso de que se haya ejecutado correctamente la Interfaz se asigna el folio, n�m. movto o cve. de rastreo
	         if (bExito) {
	        	 sFolioSalInterf = tduLlenaValores.tduSalColector.sNumMovto ;
				 tduDetalleDepos.setsCodigoTrans(sFolioSalInterf) ;
	         }
	         
	         //'6) Procesamiento contable
	         oConta = new fiduContabHonorarios.Contabilizador() ;
	         bEjecuto = oConta.GenAplicaOperacionSrv(oFiso, oSubFiso, oDetMov, oDatMov, lFolio, oCliente, sMsgServ, sMsgError) ;
	         if (!bEjecuto) {
	        	 sEstatus = sERRCONTA ;
	        	 //Err.Raise -777, , "GenAplicaOperacionSrv: " & sMsgServ & " " & sMsgError
	         }
	         tduDetalleDepos.setsFolio(lFolio) ;
	         
	         //'7) Confirma la Tx de Interfase con el folio contable fiduciario.
	         if (bExito) {
	               tduConfInterFid.sFolioOperacion = tduDetalleDepos.sFolio ;
	               if (!(oInterfEntSal.ConfirmacionInterFiduciaria(tduConfInterFid, sMsgError))) {
	                 GrabaBitacora("RealizaContabilidad", tduDetalleDepos.sContrato + "-" + tduDetalleDepos.sSubFiso, 
	                		 "Movto: " + tduDetalleDepos.sNumeroAbono + " " + "ConfirmacionInterFiduciaria: " + sMsgError, 999) ;
	               }
	               oInterfEntSal = null ;
	         }
	       
	         oGener = null ;
	         oFiso = null ;
	         oSubFiso = null ;
	         oDatMov = null ;
	         oDetMov = null ;
	         oConta = null ;
	         oInterfEntSal = null ;
	         
	         return true ;
			
	    }catch(Exception t) {
	    	//sMsgError = Err.Description ;
	    		      
			oFecha = null ;
			oGener = null ;
			oFiso = null ;
			oSubFiso = null ;
			oDatMov = null ;
			oDetMov = null ;
			oConta = null ;
			oInterfEntSal = null ;
			GrabaBitacora("RealizaContabilidad", tduDetalleDepos.sContrato + "-" + tduDetalleDepos.sSubFiso,
					"Movto: " + tduDetalleDepos.sNumeroAbono + " " + sMsgError, 999) ;
			if (sEstatus == "") {
				sEstatus = sERRGRAL ;
			}
			sErrorBitacora = sMsgError ;
			return false ;
	    }
	    
	}
	
	public static void CargaDatoSrvLoc(fiduEstrucContables.DatosMovimiento opDatosMov, 
			String pConcepto, String pValor, int pNumDato) {
		
		//ReDim Preserve opDatosMov(pNumDato)
		fiduEstrucContables.DatosMovimiento aux[]= new fiduEstrucContables.DatosMovimiento[opDatosMov.length()]; 
		System.arraycopy(opDatosMov, 0, aux, 0, opDatosMov.length());
	
		opDatosMov=null;
		opDatosMov=new fiduEstrucContables.DatosMovimiento[pNumDato] ;
		System.arraycopy(aux, 0, opDatosMov, 0, aux.length());
	
		for(int x=0;x<opDatosMov.length();x++) {
			opDatosMov[x] = new fiduEstrucContables.DatosMovimiento() ;
			opDatosMov[x].sConcepto = pConcepto ;
			opDatosMov[x].iNumDato = pNumDato ;
			opDatosMov[x].vValor = pValor ;
		}
		// NO entiendo la siguiente linea, en el archivo de VB6 pNumDato no tiene ningun valor.
		pNumDato = pNumDato + 1;
	}

	public boolean EjecutaTransaccion(String sSql, String sMsgError) {
		
		IfiduBaseDatos.clsBaseDatos obase;
		
		try {
			fiduBaseDatos.BaseDatos obase = new fiduBaseDatos.BaseDatos();
			
			obase.EjecutaTransaccion(sSql, sMsgError);
			
			obase = null;
			
			if (sMsgError!="") {
				//Err.Raise 1, , sMsgError;				
			}
			
		}catch(Exception e) {
			sMsgError = e.getMessage() + "\n" + " Fuente: EjecutaTransaccion."; //+ sCOMPONENTE REM VERIFICAR ANALIZAR
			obase = null;
			return false ;
			
		}
		return true ;
	}
	
	public void GrabaBitacora(String sFuncion, String sForma,
			String sRedaccion, int sUsuario) {
		try {
			
			IfiduBitacora.clsBitacora oBitacora ;
			fiduBitacora.Bitacora oBitacora = new fiduBitacora.Bitacora() ;
		    oBitacora.GeneraBitacora(sFuncion.substring(0,24), 
		    		sForma.substring(0,50), sRedaccion.substring(0,200), 
		    		"MAESTRA",sUsuario) ;
		    oBitacora = null ;
			
		}catch(Exception e) {
			oBitacora = null ;
		}
	}
	
	//l�nea 627
	private boolean ArmaDatosDatoval(fiduEstrucContables.DatosMovimiento oDatMov,
            String sContrato, String sSubContrato, int iMoneda, String sImporte,
            String sCuenta, int iNumeroDatos, String sMsgError) {
		
		int iNumDatos ;
		double dImporteNacional ;
		
		try {
			
			CargaDatoSrvLoc (oDatMov, "CONTRATO", sContrato, iNumDatos) ; //'0
			CargaDatoSrvLoc (oDatMov, "SUBCONTRATO", sSubContrato, iNumDatos) ; //'1
			CargaDatoSrvLoc (oDatMov, "CUENTA", sCuenta, iNumDatos) ; //'2  PONER UN DATO PARA LA CUENTA
			
			NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
			
			CargaDatoSrvLoc (oDatMov, nf.format(iMoneda) + "IMPORTE", 
					 nf.format(sImporte), iNumDatos) ;//  '3
			iNumeroDatos = iNumDatos ;
			return true ;

		}catch(Exception e) {
			sMsgError = e.getMessage() + "\n" + " Fuente: ArmaDatosvalLiqui." + sCOMPONENTE ;
			GrabaBitacora("ArmaDatosDatoval", sContrato + "-" + sSubContrato, sMsgError, 999) ;
			return false ;
		}
	}

	fiduEstrucGlobales.Cliente oCreaCliente (boolean bConComplem, String sMsgError){
			
		fiduBaseDatos.clsBaseDatos obase; 
		StringBuffer sSqlB = new StringBuffer(); 
		String sSql; 
		ADODB.Recordset orsRkset;
		String gnivel1;
		String gnivel2;
		String gnivel3;
		String gnivel4;
		String gnivel5;
		Long gusuario;
		String sPwd;
		fiduEstrucGlobales.Cliente oClienteDumm;
		String sUsuario;
		
		try {
			ObtenDatosUsuario(sPwd, sUsuario, sMsgError);
			if (sMsgError!="") {
				//Err.Raise 1, , sMsgError
			}
			
			fiduEstrucGlobales.Cliente oClienteDumm;
			oClienteDumm.lUsuario = 999;
			oClienteDumm.sTerminal = BI_TERMINAL;
			
			if (bConComplem) {
				oClienteDumm.sDirWin = "";
			}
			if (oClienteDumm!=null) {
				oClienteDumm = null;
			}
			
			return oClienteDumm;
			
		}catch(Exception e) {
			sMsgError = e.getMessage();
			GuardaBitacora("oCreaCliente", sCOMPONENTE, sMsgError);
		}
	}
	
	public static Boolean ObtenDatosUsuario(String sPassword, String sUsuario, String sMsgError ) {
		/*
		FUNCION PARA OBTENER USUARIO Y PWD DE BD
		REM  EL NUMERO DE USUARIO ESTA DEFINIDO POR LA CONSTANTE sconstUSUARIO
		*/
		IfiduBaseDatos.clsBaseDatos obase;
		ADODB.Recordset orsRkset;
		StringBuffer sSql = new StringBuffer();
		String sMsgError;
		
		try{
			sSql.append(" SELECT USU.USU_CVE_BBVA, CVE.CVE_DESC_CLAVE, USU.USU_NUM_USUARIO FROM"
					+ "FDUSUARIOS USU INNER JOIN CLAVES CVE ON USU.USU_NUM_USUARIO =  "
					+ "CVE.CVE_LIMINF_CLAVE WHERE CVE.CVE_NUM_CLAVE = 680 AND "
					+ "CVE.CVE_NUM_SEC_CLAVE = 98 AND CVE.CVE_LIMINF_CLAVE = USU.USU_NUM_USUARIO"
					+ " AND CVE.CVE_CVE_ST_CLAVE = 'ACTIVO' AND USU.USU_CVE_ST_USUARIO = 'ACTIVO'");
			
			//Set obase = CreateObject("fiduBaseDatos.BaseDatos")
			//Set orsRkset = obase.EjecutaSeleccion(sSql, 0, sMsgError)
			//Set obase = Nothing
			
			if (sMsgError !="") {
				//Err.Raise 1, , sMsgError
			}
			if (orsRkset.BOF & orsRkset.EOF) {
				//Err.Raise 1, , "No existe los datos del usuario"
			}
			sUsuario = orsRkset("USU_CVE_BBVA");
			sPassword = orsRkset("CVE_DESC_CLAVE");
			sgNumeroUsuario = orsRkset("USU_NUM_USUARIO");
			return true;
			
		}catch(Exception e) {
			sMsgError = e.getMessage();
			GuardaBitacor("ObtendatosUsuario", sCOMPONENTE, sMsgError);
			//Set obase=null;
			return false;
		}
	}

	public boolean EjecutaTransacci�n(String sSql, String sMsgError) {
		
		IfiduBaseDatos.clsBaseDatos obase;
		
		try {
			fiduBaseDatos.BaseDatos obase = new fiduBaseDatos.BaseDatos();
			
			obase.EjecutaTransaccion(sSql, sMsgError);
			
			obase = null;
			
			if (sMsgError!="") {
				//Err.Raise 1, , sMsgError;				
			}
			
		}catch(Exception e) {
			sMsgError = e.getMessage() + "\n" + " Fuente: EjecutaTransaccion."; //+ sCOMPONENTE REM VERIFICAR ANALIZAR
			obase = null;
			return false;
		}
		return true;
	}

	//l�nea788
	public boolean InvocaCargaMovtos (String sIdCuenta, String sCuenta, String sTotalDescargados, 
			String sMovInicialDesc,String sMovFinalDesc,String sMsgError) {
		
		IfiduBaseDatos.clsBaseDatos oBaseDatos;
		ADODB.Recordset orsMovtoLog; 
		ADODB.Recordset orsRksetMovtos; 
		Boolean bExitoEnCarga; 
		StringBuffer sSql = new StringBuffer(); 
		String sFechaInicial; 
		String sFechaActual; 
		String sFechaFinal; 
		String sMovtoUltimaCarga; 
		String sMovtoInicial; 
		String sMovimientoFinal; 
		String sObservaciones; 
		String sEstadoCarga; 
		Boolean bExitoEnGrabar; 
		String sMsgErrCarga; 
		String sOrdenamiento;
		String sTotalDeMovtos;
		
		try {
			//sFechaActual = Format(ObtenerFechaActual(sMsgError), "yyyy-mm-dd");
			if (sMsgError != "") {
				//Err.Raise 1, , "Error al obtener la Fecha" + " " + sMsgError;
			}
			sSql.append(" SELECT DEJ_MOVTOA_BGW   ULTIMOMOVTO        ");
	        sSql.append("        ,DEJ_FECHA_OPER  FECHAULTIMOMOVTO   ");
	        sSql.append(" FROM FID_EJE_DEPOSITOS_JUMBO               ");
	        sSql.append(" WHERE JBO_ID = " + sIdCuenta);
	        sSql.append(" ORDER BY DEJ_MOVTOA_BGW DESC ");
			sSql.append(" FETCH FIRST 1 ROW ONLY ");
			
			fiduBaseDatos.BaseDatos oBaseDatos = new fiduBaseDatos.BaseDatos();
			orsMovtoLog = oBaseDatos.EjecutaSeleccion(sSql, 0, sMsgError);
			
			oBaseDatos = null;
			if (sMsgError!="" ) {
				//Err.Raise 1, , "Error al accesar a bit�cora de movimientos" + vbCrLf + sMsgError;
			}
			
			if (orsMovtoLog.BOF && orsMovtoLog.EOF) {
				sFechaInicial = sFechaActual;
	            sFechaFinal = sFechaActual;
	            sMovtoUltimaCarga = "0";
	            sMovimientoFinal = "0";
	            sMovtoInicial = "0";
	            sOrdenamiento = gcsOrdenacionDesc;
			}
			else {
				sOrdenamiento = gcsOrdenacionDesc;
	            //orsMovtoLog.MoveFirst;
	            //sFechaInicial = Format(orsMovtoLog("FECHAULTIMOMOVTO"), "yyyy-mm-dd");
	            sFechaFinal = sFechaActual;
	            
	             if((orsMovtoLog("ULTIMOMOVTO")==null)){
	            	 sMovtoUltimaCarga = "0"; 
	             }
	             else {
	            	 orsMovtoLog("ULTIMOMOVTO");
	             }              		 
	            	
	            sMovtoInicial = sMovtoUltimaCarga + 1;
	            if (sOrdenamiento == gcsOrdenacionDesc) {
	            	sMovimientoFinal = 0;
	            }                    
	            else {
	            	sMovimientoFinal = Integer.parseInt(Val(sMovtoInicial))= + 499;
	            }       
			}
				/*Set orsRksetMovtos = New ADODB.Recordset;
		        orsRksetMovtos.Fields.Append "NoLinea", adInteger;
		        orsRksetMovtos.Fields.Append "Entidad", adVarChar, 4;
		        orsRksetMovtos.Fields.Append "CentroAlta", adVarChar, 4;
		        orsRksetMovtos.Fields.Append "Cuenta", adVarChar, 10;
		        orsRksetMovtos.Fields.Append "SecreNov", adVarChar, 5;
		        orsRksetMovtos.Fields.Append "NumerMov", adVarChar, 9;
		        orsRksetMovtos.Fields.Append "Codigo", adVarChar, 3;
		        orsRksetMovtos.Fields.Append "Signo1", adVarChar, 1;
		        orsRksetMovtos.Fields.Append "Importe", adVarChar, 22;
		        orsRksetMovtos.Fields.Append "FechaConta", adVarChar, 10;
		        orsRksetMovtos.Fields.Append "FechaOper", adVarChar, 10;
		        orsRksetMovtos.Fields.Append "HoraOper", adVarChar, 4;
		        orsRksetMovtos.Fields.Append "FechaValor", adVarChar, 10;
		        orsRksetMovtos.Fields.Append "Divisa", adVarChar, 4;
		        orsRksetMovtos.Fields.Append "Cheque", adVarChar, 10;
		        orsRksetMovtos.Fields.Append "RefInterna", adVarChar, 44;
		        orsRksetMovtos.Fields.Append "NumcRefDesc", adVarChar, 3;
		        orsRksetMovtos.Fields.Append "TipoContab", adVarChar, 4;
		        orsRksetMovtos.Fields.Append "Observaciones", adVarChar, 44;
		        orsRksetMovtos.Fields.Append "Signo2", adVarChar, 1;
		        orsRksetMovtos.Fields.Append "SaldoAut", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "EntidadCont", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "CentroCont", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Producto", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "SubProdu", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "IndAOM", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "IndACT", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Contrapartida", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "IndMultiple", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "IndMod", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "IndAnu", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "IndCarAbo", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "TipProcOri", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "AplicacionOri", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "TransaccionOri", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "CorASU1", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "CorASU2", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Plazo1", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Plazo2", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "IndIVA", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Morosidad", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Tipo", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Concepto", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "SubConcepto", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "Filler", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "AplicacionAct", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "EntidadAct", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "CentroAct", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "UserIDAct", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "NetNameAct", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "TimestAct", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "AuxAlfaGrande2", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "AuxAlfaGrande3", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "AuxAlfaGrande", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "AuxAlfaChica", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "AuxAlfaChica2", adVarChar, 100;
		        orsRksetMovtos.Fields.Append "EsReferenciaValida", adVarChar, 2;
		        orsRksetMovtos.Fields.Append "ReferenciaValida", adVarChar, 20;
		        orsRksetMovtos.Fields.Append "Editable", adVarChar, 2;
		        orsRksetMovtos.Fields.Append "Estado", adVarChar, 2;
		        orsRksetMovtos.Fields.Append "CopiaraReferecia", adVarChar, 2;
		        orsRksetMovtos.Fields.Append "MovtoCopiado", adVarChar, 2;
		        orsRksetMovtos.Fields.Append "ReferenciaCopiada", adVarChar, 2;
		        orsRksetMovtos.Open;*/
			
			bExitoEnCarga = CargaMovtos(sCuenta, sFechaInicial, sFechaFinal, sMovtoInicial, 
					sMovimientoFinal, orsRksetMovtos, sOrdenamiento, sMsgErrCarga);
			if (!bExitoEnCarga) {
				//Err.Raise 1, , "Error en carga de movimientos :" + sMsgErrCarga;
			}
			 bExitoEnGrabar = InsertaMovimientos(sIdCuenta, orsRksetMovtos, sMsgError);
			if (!bExitoEnGrabar){
				//Err.Raise 1, , "Error al grabar movimientos :" + sMsgError;
			}
			
			if (orsRksetMovtos.BOF && orsRksetMovtos.EOF) {
				sTotalDescargados = "0";
				sMovInicialDesc = "0";
				sMovFinalDesc = "0";
			}	           
	        else {
	        	//orsRksetMovtos.Filter = adFilterNone;
				//orsRksetMovtos.Filter = "MovtoCopiado = 'S' ";
				//sTotalDescargados = orsRksetMovtos.RecordCount;
	        }
			orsRksetMovtos.Filter = adFilterNone;
	        return true;
	        
		}catch(Exception e) {
			sMsgError = e.getMessage();
	        GuardaBitacora("InvocaCargaMovtos", sCOMPONENTE, sMsgError);
	        return false;
		}		
	}

	//l�nea 951
	public Boolean CargaMovtos(String sCuenta, String sFechaInicial, 
			String sFechaFinal, String sMovtoInicial, String sMovtoFinal, 
			ADODB.Recordset orsRksetMovto, String sOrdenamiento, String sMsgError) {
		/*
		IfiduCtasPersonales.tduDatosEntradaMOVTOS tduEntradaMovtos;
		IfiduCtasPersonales.tduDatosSalidaMOVTOS tduSalidaMovtos;
		IfiduCtasPersonales.ClsConsultaMovtos oConsultaMovtos;
		*/
		int iContador;
		Boolean bContinuar;
		String sUltimoNumMovto;
		String sMovimiento;
		String sMovInicialPivote;
		String sMovFinalPivote;
		int iPosicionErrorVacio;
		long lGranContador;
		Boolean bGrabar;
		Boolean bPosiblesMasMovtos;
		String GL_sUSR_BBVA;
		String GL_sPWD_BBVA;
		
		try {
			/*
			Valores Iniciales de Par�metros
      	Estos valores siempre van fijos en la primer invocaci�n demoniaca al colector
      	*/
			ObtenDatosUsuario(GL_sPWD_BBVA, GL_sUSR_BBVA, sMsgError);
			
			if(sMsgError!="") {
				//Err.Raise 1, , sMsgError
			}
			sMovInicialPivote = sMovtoInicial;
			sMovFinalPivote = sMovtoFinal;
			
			if(Integer.parseInt(Val(sMovFinalPivote))==0 && 
					Integer.parseInt(Val(sMovInicialPivote))!=0 && Integer.parseInt(Val(sMovtoInicial))>1){
				sMovtoInicial = Integer.parseInt(sMovtoInicial) -1;
			}
			
			
			//NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
			//CargaDatoSrvLoc (oDatMov, nf.format(iMoneda) + "IMPORTE", 
			
			
			tduEntradaMovtos.sNumMovDevueltos = "00000";
	        tduEntradaMovtos.sAreaPagNum = "00000";
	        tduEntradaMovtos.sAreaNumTar = "00000";
	        tduEntradaMovtos.sNumCta = sCuenta;     
	        tduEntradaMovtos.sUsuario = GL_sUSR_BBVA;
	        tduEntradaMovtos.sApliOrigen = "FD ";

			/*SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");			
			formateador.format(fechaaux);
	        sFechaInicial = fechaaux;*/
	        
	        DateFormat format = new SimpleDateFormat("MM dd, yyyy");
	        Date date = format.parse(sFechaInicial);
	        
	        tduEntradaMovtos.sFechaIni = formateador.format(sFechaInicial);
	        		
	        
	        
	        
	        	/*	
	        tduEntradaMovtos.sFechaFin = Format(sFechaFinal, "yyyy-mm-dd")
	        tduEntradaMovtos.sTs = Space$(16)
	        tduEntradaMovtos.sSigno = gcsSignoPositivo
	        tduEntradaMovtos.sNumIni = Format(sMovtoInicial, "000000000")
	        tduEntradaMovtos.sSigno2 = gcsSignoPositivo
	        tduEntradaMovtos.sNumFin = Format(sMovtoFinal, "000000000")
	        tduEntradaMovtos.sSigno3 = gcsSignoPositivo
	        tduEntradaMovtos.sNumPend = String$(3, "0")
	        tduEntradaMovtos.sOrdTs = sOrdenamiento
	        tduEntradaMovtos.sFiller = Space$(19)
	        tduEntradaMovtos.sPaginacMovtos = gcsSinPaginas
	        tduEntradaMovtos.sNumMovDevueltos = String$(5, "0")
	        tduEntradaMovtos.sAreaPagNum = String$(5, "0")
	        tduEntradaMovtos.sAreaNumTar = String$(5, "0")
	        tduEntradaMovtos.sAstaPaginacion = GL_sPWD_BBVA
	        */
	        bContinuar = true;
	        lGranContador = 0;	
	        
	        do{
	        	//Set oConsultaMovtos = CreateObject("fiduCtasPersonales.ConsultaMovtos")
	        	oConsultaMovtos.ConsultaMovimientos(tduEntradaMovtos, tduSalidaMovtos, sMsgError);
	        	//Set oConsultaMovtos = null
	        	
	        	if(sMsgError!="") {
	        		//iPosicionErrorVacio = InStr(1, sMsgError, "SIN MOVIMIENTOS");
	        		if(iPosicionErrorVacio > 0) {
	        			GuardaBitacora("CargaMovtos", sCOMPONENTE, sMsgError);
	        			return true;
	        		}else {
	        			//Err.Raise 1, , sMsgError
	        		}	
	        	}
	        	for(iContador = 0; iContador<7; iContador++) {
	        		if (trim(tduSalidaMovtos.tduMovtoIndividual(iContador).sNumerMov)!="") {
	        			/*
	        			ESTO SE PONE PORQUE A VECES LA CONSULTA TRAE REGISTROS REPETIDOS,PERO NO SIEMPRE,
	                    ESTO PORQUE CUANDO LE METES UN MOVIMIENTO FINAL CON CERO EN LA CONSULTA TE DEJA DE TRAER EN LAS PRUEBAS EL MOVIMIENTO
	                    PERO EN PRODUCCI�N NO SE SI PASAR�A ESTO, ENTON�S POR SI LAS FLY, NO SE VAYA A REPETIR EL MOVIMIENTO
	                    ES POR LO DE LOS 500, SI LA CONSULTA TRAE MENOS DE 500 MOVTOS SE REPITA, PERO SI NO NO SE REPITE
	                    */
	        		}
	        		if (Val(tduSalidaMovtos.tduMovtoIndividual(iContador).sNumerMov) > 0) {
	        			//orsRksetMovtos.Filter = "NumerMov = '" & Trim(tduSalidaMovtos.tduMovtoIndividual(iContador).sNumerMov) & "'"
	        			if (orsRksetMovtos.RecordCount > 0) {
	        				bGrabar = false; //NO SE GRABA PORQUE ESTA REPETIDO
	        			}else {
	        				if (Val(tduSalidaMovtos.tduMovtoIndividual(iContador).sNumerMov) >= 
	        						(Val(sMovInicialPivote))) {
	        					bGrabar = true;
	        				}else {
	        					bGrabar = false;
	        				}
	        				//SE GRABA SI NO ESTA REPETIDO Y ES MAYOR AL MOVIMIENTO INICIAL SOLICITADO
	        			}
	        			//orsRksetMovtos.Filter = adFilterNone
	        		}else {
	        			bGrabar = false; //NO ES UN DATO NUM�RICO AS� QUE NO SE GRABA
	        		}
	        		if(bGrabar) {
	        			/*
	        			orsRksetMovtos.AddNew
                      orsRksetMovtos("NoLinea") = lGranContador
                      orsRksetMovtos("Entidad") = tduSalidaMovtos.tduMovtoIndividual(iContador).sEntidad
                      orsRksetMovtos("CentroAlta") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCentroAlta
                      orsRksetMovtos("Cuenta") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCuenta
                      orsRksetMovtos("SecreNov") = tduSalidaMovtos.tduMovtoIndividual(iContador).sSecreNov
                      orsRksetMovtos("NumerMov") = Trim(tduSalidaMovtos.tduMovtoIndividual(iContador).sNumerMov)
                      orsRksetMovtos("Codigo") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCodigo
                      orsRksetMovtos("Signo1") = tduSalidaMovtos.tduMovtoIndividual(iContador).sSigno
                      orsRksetMovtos("Importe") = tduSalidaMovtos.tduMovtoIndividual(iContador).sImporte
                      orsRksetMovtos("FechaConta") = tduSalidaMovtos.tduMovtoIndividual(iContador).sFechaConta
                      orsRksetMovtos("FechaOper") = tduSalidaMovtos.tduMovtoIndividual(iContador).sFechaOper
                      orsRksetMovtos("HoraOper") = tduSalidaMovtos.tduMovtoIndividual(iContador).sHoraOper
                      orsRksetMovtos("FechaValor") = tduSalidaMovtos.tduMovtoIndividual(iContador).sFechaValor
                      orsRksetMovtos("Divisa") = tduSalidaMovtos.tduMovtoIndividual(iContador).sDivisa
                      orsRksetMovtos("Cheque") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCheque
                      orsRksetMovtos("RefInterna") = tduSalidaMovtos.tduMovtoIndividual(iContador).sRefInterna
                      orsRksetMovtos("NumcRefDesc") = tduSalidaMovtos.tduMovtoIndividual(iContador).sNumcRefDesc
                      orsRksetMovtos("TipoContab") = tduSalidaMovtos.tduMovtoIndividual(iContador).sTipoContab
                      orsRksetMovtos("Observaciones") = tduSalidaMovtos.tduMovtoIndividual(iContador).sObservaciones
                      orsRksetMovtos("Signo2") = tduSalidaMovtos.tduMovtoIndividual(iContador).sSigno2
                      orsRksetMovtos("SaldoAut") = tduSalidaMovtos.tduMovtoIndividual(iContador).sSaldoAut
                      orsRksetMovtos("EntidadCont") = tduSalidaMovtos.tduMovtoIndividual(iContador).sEntidadCont
                      orsRksetMovtos("CentroCont") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCentroCont
                      orsRksetMovtos("Producto") = tduSalidaMovtos.tduMovtoIndividual(iContador).sProducto
                      orsRksetMovtos("SubProdu") = tduSalidaMovtos.tduMovtoIndividual(iContador).sSubProdu
                      orsRksetMovtos("IndAOM") = tduSalidaMovtos.tduMovtoIndividual(iContador).sIndAom
                      orsRksetMovtos("IndACT") = tduSalidaMovtos.tduMovtoIndividual(iContador).sIndAct
                      orsRksetMovtos("Contrapartida") = tduSalidaMovtos.tduMovtoIndividual(iContador).sContrapartida
                      orsRksetMovtos("IndMultiple") = tduSalidaMovtos.tduMovtoIndividual(iContador).sIndMultiple
                      orsRksetMovtos("IndMod") = tduSalidaMovtos.tduMovtoIndividual(iContador).sIndMod
                      orsRksetMovtos("IndAnu") = tduSalidaMovtos.tduMovtoIndividual(iContador).sIndAnu
                      orsRksetMovtos("IndCarAbo") = tduSalidaMovtos.tduMovtoIndividual(iContador).sIndCarAbo
                      orsRksetMovtos("TipProcOri") = tduSalidaMovtos.tduMovtoIndividual(iContador).sTipProcOri
                      orsRksetMovtos("AplicacionOri") = tduSalidaMovtos.tduMovtoIndividual(iContador).sAplicacionOri
                      orsRksetMovtos("TransaccionOri") = tduSalidaMovtos.tduMovtoIndividual(iContador).sTransaccionOri
                      orsRksetMovtos("CorASU1") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCorAsu1
                      orsRksetMovtos("CorASU2") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCorAsu2
                      orsRksetMovtos("Plazo1") = tduSalidaMovtos.tduMovtoIndividual(iContador).sPlazo1
                      orsRksetMovtos("Plazo2") = tduSalidaMovtos.tduMovtoIndividual(iContador).sPlazo2
                      orsRksetMovtos("IndIVA") = tduSalidaMovtos.tduMovtoIndividual(iContador).sIndIva
                      orsRksetMovtos("Morosidad") = tduSalidaMovtos.tduMovtoIndividual(iContador).sMorosidad
                      orsRksetMovtos("Tipo") = tduSalidaMovtos.tduMovtoIndividual(iContador).sTipo
                      orsRksetMovtos("Concepto") = tduSalidaMovtos.tduMovtoIndividual(iContador).sConcepto
                      orsRksetMovtos("SubConcepto") = tduSalidaMovtos.tduMovtoIndividual(iContador).sSubConcepto
                      orsRksetMovtos("Filler") = tduSalidaMovtos.tduMovtoIndividual(iContador).sFiller
                      orsRksetMovtos("AplicacionAct") = tduSalidaMovtos.tduMovtoIndividual(iContador).sAplicacionAct
                      orsRksetMovtos("EntidadAct") = tduSalidaMovtos.tduMovtoIndividual(iContador).sEntidadAct
                      orsRksetMovtos("CentroAct") = tduSalidaMovtos.tduMovtoIndividual(iContador).sCentroAct
                      orsRksetMovtos("UserIDAct") = tduSalidaMovtos.tduMovtoIndividual(iContador).sUserIdAct
                      orsRksetMovtos("NetNameAct") = tduSalidaMovtos.tduMovtoIndividual(iContador).sNetNameAct
                      orsRksetMovtos("TimestAct") = tduSalidaMovtos.tduMovtoIndividual(iContador).sTimestAct
                      orsRksetMovtos("AuxAlfaGrande2") = tduSalidaMovtos.tduMovtoIndividual(iContador).sAuxAlfaGrande2
                      orsRksetMovtos("AuxAlfaGrande3") = tduSalidaMovtos.tduMovtoIndividual(iContador).sAuxAlfaGrande3
                      orsRksetMovtos("AuxAlfaGrande") = tduSalidaMovtos.tduMovtoIndividual(iContador).sAuxAlfaGrande
                      orsRksetMovtos("AuxAlfaChica") = tduSalidaMovtos.tduMovtoIndividual(iContador).sAuxAlfaChica
                      orsRksetMovtos("AuxAlfaChica2") = tduSalidaMovtos.tduMovtoIndividual(iContador).sAuxAlfaChica2
                      orsRksetMovtos.Update
                      sUltimoNumMovto = tduSalidaMovtos.tduMovtoIndividual(iContador).sNumerMov
	        			*/
	        			lGranContador = lGranContador + 1;
	        		}
	        	}
	        	/*
	        	tduEntradaMovtos.sPaginacMovtos = tduSalidaMovtos.sPaginacMovtos
	            tduEntradaMovtos.sNumMovDevueltos = tduSalidaMovtos.sNumMovDevueltos
	            tduEntradaMovtos.sAreaPagNum = tduSalidaMovtos.sAreaPagNum
	            tduEntradaMovtos.sAreaNumTar = tduSalidaMovtos.sAreaNumTar
	        	*/
	        	if(tduSalidaMovtos.sPaginacMovtos=="S") {
	        		bContinuar=true;
	        	}else {
	        		bContinuar=false;
	        	}
	        	
	        	switch(tduEntradaMovtos.sOrdTs) {
	        		
	        		case gcsOrdenacionDesc:
	        			if(Val(tduEntradaMovtos.sNumMovDevueltos >= 10 & tduSalidaMovtos.sPaginacMovtos == "N")) {
	        				bPosiblesMasMovtos=true;
	        			}else {
	        				bPosiblesMasMovtos=false;
	        			}
	        			break;
	        		
	        		case gcsOrdenacionAsc:
	        			if(Val(tduEntradaMovtos.sNumMovDevueltos >= 10 & tduSalidaMovtos.sPaginacMovtos == "N")) {
	        				bPosiblesMasMovtos=true;
	        			}else {
	        				bPosiblesMasMovtos=false;
	        			}
	        			break;
	        			//ah, creo que es igual
	        	}
	        	
	        	if(bPosiblesMasMovtos) {
	        		/*
	        		tduEntradaMovtos.sNumMovDevueltos = String$(5, "0")
	        		tduEntradaMovtos.sAreaPagNum = String$(5, "0")
	                tduEntradaMovtos.sAreaNumTar = String$(5, "0")
	                tduEntradaMovtos.sNumCta = sCuenta
	                */
	        		
	        		switch(tduEntradaMovtos.sOrdTs) {
	        			
	        			case gcsOrdenacionDesc:
	        					if(Val(sMovInicialPivote)>0 && Val(sMovFinalPivote)==0) {
	        						/*
	        						orsRksetMovtos.Sort = "NumerMov DESC"
                                  orsRksetMovtos.MoveFirst
                                  sUltimoNumMovto = orsRksetMovtos("NumerMov")
	        						*/
	        						sMovtoFinal = 0;
	        						sMovtoInicial = sUltimoNumMovto;
	        						//NO LE PONGO 1 NI MENOS UNO PORQUE EN LAS PRUEBAS SIEMPRE TRAJO A PARTIR DE LA CONSULTA + 1, caso en m�s de 500
	        						bContinuar = true;
	        					}else {
	        						if(sUltimoNumMovto > 1) {
	        							sMovtoFinal = Val(sUltimoNumMovto) - 1;
	        							if(Val(sMovInicialPivote)==0) {
	        								sMovtoInicial = 0;
	        							}
	        							bContinuar = true;
	        							if(Val(sMovtoInicial)>Val(sMovtoFinal)) {
	        								bContinuar = false;
	        							}
	        						}else {
	        							bContinuar = false; 
	        							//SI EL �LTIMO N�MERO DE MOVIMIENTO ES 1 ENTONCES YA NO SE CONTINUAR� BUSCANDO PORQUE YA NO HAY M�S
	        						}
	        					}
	        				break;
	        			
	        			case gcsOrdenacionAsc:
	        					
	        				sMovtoInicial = sUltimoNumMovto + 1;
                          sMovtoFinal = sMovtoInicial + 499;
                          bContinuar = true;
	        				break;
	        		}
	        		/*
	        		tduEntradaMovtos.sNumIni = Format(sMovtoInicial, "000000000")
	        		tduEntradaMovtos.sNumFin = Format(sMovtoFinal, "000000000")
	                tduEntradaMovtos.sNumPend = String$(3, "0")
	                tduEntradaMovtos.sPaginacMovtos = gcsSinPaginas;
	                */
	        	}	
	        }while(bContinuar);
	        
	        return true;
			
		}catch(Exception e) {
			
			sMsgError = e;
			GuardaBitacora("CargaMovtos", sCOMPONENTE, sMsgError);
			//Set oConsultaMovtos = Nothing
			return false;
		}
	}

	public boolean ValidaHorarioInterfChqCaja( String sInterfaz, String  sHora, String sMsgError ) {

		StringBuffer sSql = new StringBuffer();
		IfiduBaseDatos.clsBaseDatos obase ;
		ADODB.Recordset orsRecordset ;
		
		try {
			sSql.append(" SELECT  HOI_HORA_INI_OPERACION AS HINICIO, ");
			sSql.append("HOI_HORA_FIN_OPERACION AS HFIN FROM FID_INT_HORARIO_INTERFASE ");
			sSql.append("WHERE HOI_STATUS= 'ACTIVO' AND HOI_ID_INTERFASE = "+ sInterfaz);
			
			//obase = new fiduBaseDatos.BaseDatos();
			//orsRecordset = new obase.EjecutaSeleccion(sSql, 0, sMsgError);
		    obase = null ;
		    
		    if (sMsgError != "") { 
		    	//Err.Raise 1, , "Error al realizar la consulta de Horario de la interfaz " + sInterfaz + " " + sMsgError ;
		    }
		    if (orsRecordset.BOF && orsRecordset.EOF) {
		    	//Then Err.Raise 1, , "No existe horario para la interfaz " + sInterfaz ;
		    }
		    orsRecordset = null ;
		    
		    sSql.append( " SELECT  *  " );
	        sSql.append(" FROM    FID_INT_HORARIO_INTERFASE") ;
	        sSql.append(" WHERE   HOI_ID_INTERFASE = " + sInterfaz );
	        sSql.append(" AND     TIME('" + sHora + "')" );
	        sSql.append(" BETWEEN HOI_HORA_INI_OPERACION" );
	        sSql.append(" AND     HOI_HORA_FIN_OPERACION" );
	        sSql.append(" AND HOI_STATUS = 'ACTIVO'" );
			
            obase = new fiduBaseDatos.BaseDatos(); 
            orsRecordset = obase.EjecutaSeleccion(sSql, 0, sMsgError) ;
            obase = null ;
            if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
            if (orsRecordset.BOF && orsRecordset.EOF) {
            	return false;
            }
            orsRecordset = null ;
		            
            return true;
            
		}catch(Exception ex) {
			//sMsgError = ex.getMessage() ;
	        return false ;
	        obase = null ;
	        orsRecordset = null ;
		}	
	}

	//l�nea 1187
	public boolean RealizaDescargaContabilidad (String sMsgError) {
		
		//FUNCION QUE REALIZA PRIMERO LAS DESCARGAS DE LOS MOVIMIENTOS DE LA MEGA CUENTA
		//Y DESPU�S CONTABILIZA ESTOS MOVIMIENTOS DESCARGADOS

		//ADODB.Recordset orsMovtos;
		Boolean bHayMovimientos;
		Boolean bExito;
		String sMoneda;
		//TduDetalleDeposito tduDetalleDeposit; 
		//IfiduFechas.clsFechas oFechaCont;
		String sFechaCont;
		//fiduEstrucGlobales.Cliente oCliente;
		String sCuenta; 
		Long lContabilizados;
		String sNumMovDesc;
		String sMovIniDesc;
		String sMovFinDesc;
		String sPwd;
		String sUsuario;
		String sErrorBitacora;
		String sEstatus;
	
		try {
			ObtenCuentaJumbo(sCuenta, sMoneda, sMsgError);
			if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
			
			String sIdCuenta = Integer.toString(iIDCTAJUMBO)  ;
			
			InvocaCargaMovtos(sIdCuenta, sCuenta, sNumMovDesc, sMovIniDesc, sMovFinDesc, sMsgError);
			if (sMsgError != "") {
				GuardaBitacora("RealizaDescargaContabilidad", sCOMPONENTE, sMsgError);
			}
			GuardaBitacora("RealizaDescargaContabilidad", sCOMPONENTE, "Movtos descargados:" + sNumMovDesc + ". Mov Inicial:" + sMovIniDesc + ", Mov final:" + sMovFinDesc);
			orsMovtos = RegPorContab(bHayMovimientos, sMsgError);
			
			if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
			
			if (!bHayMovimientos) {
				return true;
			}
			/*if ((orsMovtos.BOF) && (orsMovtos)) {
				return true;
			}*/
			//fiduFechas.Fechas oFechaCont = new fiduFechas.Fechas;
			//sFechaCont = oFechas;
			oFechaCont = null;
			//orsMovtos.MoveFirst
			oCliente = oCreaCliente(False, sMsgError);
			
			if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
			
			lContabilizados = (long) 0;
			
			if (!orsMovtos.BOF && !orsMovtos.EOF){
				//orsMovtos.MoveFirst;
				ObtenDatosUsuario(sPwd, sUsuario, sMsgError);
				
				if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
				
				while (!orsMovtos.EOF){
					
					tduDetalleDeposit.setsContrato(orsMovtos("CONTRATO"));
					tduDetalleDeposit.setsSubFiso(orsMovtos("SUBCONTRATO"));
		            tduDetalleDeposit.setsNumeroMoneda(Integer.parseInt(sMoneda));
		            tduDetalleDeposit.setsNumeroCuenta(sCuenta);
		            tduDetalleDeposit.setsImporte(orsMovtos("IMPORTE"));
		            tduDetalleDeposit.setsNumeroAbono(orsMovtos("MOVIMIENTO"));
		            tduDetalleDeposit.setsPassword(sPwd);
		            tduDetalleDeposit.setsUsuario(sgNumeroUsuario);
		            tduDetalleDeposit.setsCodigoTrans("");
		            tduDetalleDeposit.setsFolio("");
		            tduDetalleDeposit.setsNumeroOperacion("");
		            
		            sErrorBitacora = "";
		            sEstatus = "";
		            bExito = RealizaContabilidad(tduDetalleDeposit, oCliente, sErrorBitacora, sEstatus);
		            
		            if (bExito) {
		            	lContabilizados = lContabilizados + 1;
		            }
		            
		            ActualizaEstatusMovto(tduDetalleDeposit.sNumeroAbono, tduDetalleDeposit.sCodigoTrans, tduDetalleDeposit.sFolio, tduDetalleDeposit.sNumeroOperacion, sErrorBitacora, sEstatus);
		            //orsMovtos.MoveNext
		            
				}
		}
		GuardaBitacora("RealizaDescargaContabilidad", sCOMPONENTE, "Registros Contabilizados: " + lContabilizados);
		
		oFechaCont = null;
		oCliente = null;
		
		return true;
	
		}catch (Exception e) {
			sMsgError = e.getMessage();
			GuardaBitacora("RealizaDescargaContabilidad", sCOMPONENTE, sMsgError);
			oFechaCont = null;
			oCliente = null;
			return false;
		}	
	}
	
	//l�nea 1270
	public boolean ObtenCuentaJumbo(String sCuenta, String sMoneda, String sMsgError) {
		
		IfiduBaseDatos.clsBaseDatos obase ; 
		StringBuffer sSql = new StringBuffer();
		ADODB.Recordset orsRkset ; 
		
		try {
			
			sSql.append(" SELECT JBO_NUM_CUENTA AS CUENTA, JBO_NUM_MONEDA AS MONEDA ") ;
		    sSql.append(" FROM FID_EJE_JUMBOS " );
		    sSql.append(" WHERE JBO_ID = " + iIDCTAJUMBO + " AND JBO_ESTATUS = 'A'" );
		    
		    obase = new fiduBaseDatos.BaseDatos() ;
		    orsRkset = obase.EjecutaSeleccion(sSql, 0, sMsgError) ;
		    obase = null ;
		    
		    if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
		    
		    if (orsRkset.BOF && orsRkset.EOF) {
		    	//Err.Raise 1, , "No existe cuenta jumbo configuarada" ;
		    }
		    
		    sCuenta = orsRkset("CUENTA") ;
		    sMoneda = orsRkset("MONEDA") ;
		    return true ;
			
		}catch(Exception ex) {
			GuardaBitacora("ObtenCuentaJumbo", sCOMPONENTE, Err.Description) ;
		    obase = null ;
		    return false ;
		}
		
	}
	
	//l�nea 1298
	public ADODB.Recordset RegPorContab(Boolean bHayMovimientos, String sMsgError) {
		
		IfiduBaseDatos.clsBaseDatos obase ;
		ADODB.Recordset orsRkset ;
		StringBuffer sSql = new StringBuffer();
		
		try {
			
			sSql.append(" SELECT DEJ_MOVTOA_BGW     AS MOVIMIENTO") ;
		    sSql.append("        ,DEJ_IMPORTE       AS IMPORTE") ;
		    sSql.append("        ,DEJ_NUM_CONTRATO  AS CONTRATO") ;
		    sSql.append("        ,DEJ_SUB_CONTRATO  AS SUBCONTRATO ") ;
		    sSql.append(" FROM FID_EJE_DEPOSITOS_JUMBO") ;
		    sSql.append(" WHERE (DEJ_ESTATUS = 'EI' OR DEJ_ESTATUS IS NULL) ") ;
		    sSql.append("       AND DEJ_TIPO_MOVTO = 'A'") ;
		    sSql.append("       AND DEJ_ANULADO = 'N' ") ;
		    sSql.append("       AND DEJ_REFERENCIA_EJE = 'V'") ;
			
		    obase = new fiduBaseDatos.BaseDatos () ;
		    orsRkset = obase.EjecutaSeleccion(sSql, 0, sMsgError) ;
		    obase = null ;
		    
		    if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
		    
    	    if (orsRkset.BOF && orsRkset.EOF) {
    	    	//Exit Function
    	    }
    	    bHayMovimientos = true ;
    	    return orsRkset ;
		    
		}catch(Exception ex) {
			sMsgError = Err.Description ;
		    bHayMovimientos = false ;
		    GuardaBitacora("RegPorContab", sCOMPONENTE, sMsgError) ;
		    obase = null ;
		}
		
	}
	
	//l�nea 1335
	public static Boolean LeeFrecuenciaDescargas(int iFrecuencia) {
		
		StringBuffer sSql = new StringBuffer();
		String sMsgError=null;
		
		try {
			
			sSql.append("SELECT  CVE_LIMINF_CLAVE AS FRECUENCIA FROM CLAVES "
					+ "WHERE CVE_NUM_CLAVE = 670 AND CVE_NUM_SEC_CLAVE = 5 ");
			
			
			//fiduBaseDatos.clsBaseDatos oDb = new fiduBaseDatos.clsBaseDatos();
			//oDb.EjecutaSeleccion rsConsulta = new oDb.EjecutaSeleccion(sSql, 0, sMsgError);
			
			if (sMsgError != null) {
				//Err.Raise -1, , sMsgError
				oDb = null;
			}
			/*if rsConsulta.BOF And rsConsulta.EOF {
			 
				 Err.Raise -1, , "Error al intentar consultar el intervalo de descargas"
				 //iFrecuencia = rsConsulta("FRECUENCIA");
				 rsConsulta = null;
				 
			 }
			*/
			return true;
			
		}
		catch(Exception t){
			
			//sMsgError = Err.Description;
			iFrecuencia = 0
					;
			oDb = null;
			//GuardaBitacora("LeeFrecuenciaDescargas", sCOMPONENTE, sMsgError);
			return false;
		}
	
	}
	
	//l�nea 1373
	public boolean ActualizaEstatusMovto(String sNumMovto, String sNumCargo, String sFolioOperacion, 
			String sIdOperacion, String sErrorBitacora, String sEstatus) {
		
		IfiduBaseDatos.clsBaseDatos obase ; 
		StringBuffer sSql = new StringBuffer();
		String sMsgError ;
		
		try {
			
			if (sEstatus == "") {
				sEstatus = sSINERROR ;
			}
			sSql.append(" UPDATE FID_EJE_DEPOSITOS_JUMBO " );
		    sSql.append(" SET DEJ_ESTATUS = '" + sEstatus + "'" );
			    
			    if (Val(sNumCargo) > 0) {
			    	sSql.append(" ,DEJ_MOVTOC_BGW = " + sNumCargo );
			    }
			    if (Val(sFolioOperacion) > 0) {
			    	sSql.append(" ,DEJ_FOLIO_OPERA = " + sFolioOperacion) ;
			    }
			    if (sIdOperacion.length() > 0){
			    	sSql.append(" ,DEJ_ID_OPERACION = '" + sIdOperacion + "'" );
			    }
			    else{
			    	sSql.append(" ,DEJ_ID_OPERACION = NULL ") ;
			    }
			    
			    if (sErrorBitacora.length() > 0){
			    	sSql.append(" ,DEJ_DESC_ERROR = '" + sErrorBitacora.trim().substring(0,79) + "'" );
			    }
			    else{
			    	sSql.append(" ,DEJ_DESC_ERROR = NULL " );
			    }
			        
			    sSql.append(" WHERE DEJ_MOVTOA_BGW = " + sNumMovto );
			    sSql.append(" AND JBO_ID = " + iIDCTAJUMBO );  // 'rem cuenta
			    
			    obase = new fiduBaseDatos.BaseDatos() ;
	    	    obase.EjecutaTransaccion(sSql, sMsgError) ;
	    	    obase = null ;
	    	    if (sMsgError != "") {
	    	    	//Err.Raise 1, , "Error al Actualizar el movto: " + sNumMovto + " " + sMsgError ;
	    	    }
	    	    return true ;
			
		}catch(Exception ex) {
			GuardaBitacora("ActualizaEstatusMovto", sCOMPONENTE, Err.Description) ;
		    
		    obase = null ;
    		return false ;
		}
		
	}
	
	public static String ObtenNumerodeBanco(String sContrato, String sMsgError){
		
		//IfiduBaseDatos.clsBaseDatos obase;
		StringBuffer sSql = new StringBuffer();
		//ADODB.Recordset orsRkset;
		
		try {
			
			sSql.append(" SELECT CTO_NUM_NIVEL1 AS BANCO FROM CONTRATO WHERE "
					+ "CTO_NUM_CONTRATO =  " + sContrato +
					" AND CTO_CVE_ST_CONTRAT =  'ACTIVO' ");
			//Set obase = CreateObject("fiduBaseDatos.BaseDatos")
		    //Set orsRkset = obase.EjecutaSeleccion(sSql, 0, sMsgError)
		    //Set obase = null;
			
			if(sMsgError != "") {
				//Err.Raise 1, , sMsgError
			}
			if (orsRkset.BOF && orsRkset.EOF) {
				//Err.Raise 1, , "No existe el contrato" + sContrato;
				return Trim(orsRkset("BANCO"));
			}
		}catch(Exception e) {
			GuardaBitacora("ObtenCuentaJumbo", sCOMPONENTE, e);
			//Set obase = null;
			return "";
		}
	}
	
	public boolean ObtenNumerodeOperacion(String sContrato, 
			String sNumeroOperacion, String sMsgError) {
		
		String sBanco ; 
		
		try {
			
			sBanco = ObtenNumerodeBanco(sContrato, sMsgError).trim() ;
			
			if (sMsgError != "") { /* Err.Raise 1, , sMsgError*/ }
			
		    switch (sBanco) {
			    case "1": {
			    	sNumeroOperacion = "70700000071700" ;
			    }break ;
				case "2":{
					sNumeroOperacion = "70700000071800" ;
				}break ;
				case "3":{
					sNumeroOperacion = "70700000084200" ; //   '* EHH-PBAN302/374-0002
				}break ;
		    }
		    return true ;
			
		}catch(Exception ex) {
			//sMsgError = Err.Description
		    sNumeroOperacion = "" ;
		    return false ;
		}
	}
	
	boolean ValidaReferencia(String sReferenciaAmpliada, String sTransaccion,
			String sContrato, String sSubContrato, String sMsgError) {
			
			String aux;
		
		try {
			sContrato = "";
		    sSubContrato = "";
		    
		    /*INI GCLD 2008-02-05 Eliminar validaci�n de que el campo se encuentre vac�o,  ya que ha nosotros nos sigue llegando vac�o
			                    de Cuentas Personales*/
			if (sTransaccion.trim() == "") {
				//Err.Raise 1, , "No se pudo obtener contrato y subcontrato"
			}
/*
			FIN GCLD 2008-02-05

			INI GCLD 2008-01-25*/
		    
		    aux = sReferenciaAmpliada.substring(8,15);
		    sContrato = Val(aux);
		    aux = sReferenciaAmpliada.substring(15,18);
    	    sSubContrato = Val(aux);
			
			if (Integer.parseInt(Val(sContrato))<1) {
				//Err.Raise 1, , "El n�mero de contrato debe ser mayor que cero";
			}
			
			return true;
			
		}catch(Exception e) {
			sMsgError = e.getMessage();
		    sContrato = "";
		    sSubContrato = "";			
			return false;
		}
	}
	
	public String Val(String s) {	
		
		int flag = 0, i = 0;
		String st = "";
		
		while(i<s.length() && flag == 0) {
			if (s.charAt(i)>=48 && s.charAt(i)<=57) { st = st + s.charAt(i) ; }
			else if (s.charAt(i) == 32) { }
			else if (s.charAt(i) == 60) { return "0" ; }
			else if (s.charAt(i) == 62) { return "0" ; }
			else  { 
				flag = 1;
				 i++; 
			}
			
			i++ ;
		}
		if (i == 2) { st = "0" ; }
		if (i == 1) { st = s ; }
		
		return st ; 
	}	
}

