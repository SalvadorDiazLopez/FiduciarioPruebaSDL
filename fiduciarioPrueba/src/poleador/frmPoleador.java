package poleador;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class frmPoleador {

	private static void FormLoad(){		
		try{
			//if(){				
				//GuardaBitacora("Load", sCOMPONENTE, "Una instancia de fiduPoleadorCtaEje ya se encontraba activa");
			//}
				//GuardaBitacora("Load", sCOMPONENTE, "Inicio de procesamiento de poleador:"+ now();
			
		}catch(Throwable t){
				//GuardaBitacora("Load", sCOMPONENTE, Err.Description);
				//Temporizador.Interval = 1;
		}		
	}

	private static void Form_UnLoad(int Cancel){
		GuardaBitacora("Load", sCOMPONENTE, "Termino de procesamiento de poleador:" + now());
	}
	
	public void Temporizador_Timer(){		
		try {
			System.out.println("Hora: 1");
			Ejecucion();
		}catch(Throwable e) {
			GuardaBitacora("Temporizador", sComponente, e.getMessage());
		}	
	}
	
	@SuppressWarnings("deprecation")
	public void Ejecucion () {
		
		//IfiduFechas.clsFechas oFecha ;
		String sMsgError, sHoraActual, sProximaDesc, sSiguienteDescarga ;
		Date dFechaActual = new Date() ;
		Boolean bEnHorario ;
		Integer iRango ;
		//Time temporizador ;
		
		try {
			//temporizador.in .Interval = 60000 ;//' 60 MIL MILISEGUNDOS QUE EQUIVALEN A 60 SEGUNDOS
			
			//Date date = new Date();
			
			DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String strFecha = dateFormat.format(dFechaActual) ;
			sHoraActual = hourFormat.format(dFechaActual) ;
			
			System.out.println("Hora: "+ sHoraActual + "\n \t Fecha: " + strFecha);
			//oFecha = new IfiduFechas.clsFechas() ;
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(dFechaActual);
			int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
			
			if ( !bHayDescargasPrevias ) {
				
				if ( !oFecha.HayDiaFestivo(dFechaActual) || diaSemana == 1 || diaSemana == 7) {
					
					bEnHorario = ValidaHorarioInterfChqCaja(sInterfaz, sHoraActual, sMsgError) ;
					if(sMsgError != "") { /*Err.Raise 1, , sMsgError */}
					if(!bEnHorario) { 
						
						RealizaDescargaContabilidad(sMsgError) ;
						if(sMsgError != "") { /*Err.Raise 1, , sMsgError */}
						if (!LeeFrecuenciaDescargas(iRango)) {
							iRango = 1 ;							
						}
						sHoraActual = hourFormat.format(dFechaActual) ;
						
						//COMPLETAR
						dFechaActual.setMinutes(dFechaActual.getMinutes()+iRango);
						sSiguienteDescarga = hourFormat.format(dFechaActual) ;
						
				        dtFechaUltimaDescarga = dFechaActual;
				        bHayDescargasPrevias = true;
				        bDiaHabil = true;
				        //COMPLETAR
				        
					}
				}else {
					bDiaHabil = false;
				}
			}else {
				if (dFechaActual==dtFechaUltimaDescarga) {
					
					if (bDiaHabil) {
						
						if (sHoraActual>=sSiguienteDescarga) {
							
							bEnHorario=ValidaHorarioInterfChqCaja(sInterfaz, sHoraActual, sMsgError);
							if(sMsgError != "") { /*Err.Raise 1, , sMsgError */}
							if (bEnHorario) {
								
								RealizaDescargaContabilidad(sMsgError);
								if(sMsgError != "") {
									 /*Err.Raise 1, , sMsgError */								
								}
								if (!LeeFrecuenciaDescargas(iRango)){
									iRango=1;									
								}
								sHoraActual = hourFormat.format(date) ;
								//COMPLETAR
								dFechaActual.setMinutes(dFechaActual.getMinutes()+iRango);
								sSiguienteDescarga = hourFormat.format(dFechaActual) ;
				                bHayDescargasPrevias = true ;
				              //COMPLETAR
							}							
						}
					}
				}else {
					bHayDescargasPrevias = false;
				}
			}
			oFecha.set(null) ;
		
		}catch(ParseException ex) {
			System.out.println("Hora: 3");
			ex.printStackTrace();
			GuardaBitacora("EjecutorProcesos", sCOMPONENTE, ex.getMessage()) ;
			oFecha.set(null) ;
		}
	}
}
