package fiduEstrucGlobales;

public class Cliente {

	/*VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'*************************************************************'
'*           Derechos Reservados EFISOFT S.A. 2002           *'
'*************************************************************'
'*  ARCHIVO     : fiduContabilidad.vbp                       *'
'*  CLASES      : Operacion.cls                              *'
'*                                                           *'
'*  DESCRIPCION : Implementacion de Clases VALUE Generales.  *'
'*                                                           *'
'*  AUTOR       : OAP - Septiembre 2002                      *'
'*************************************************************'
Option Explicit*/
	
	private String mvarsDirTrabajo ;
	private String mvarsDirWin ;
	private int[] mvariEstructura = new int [4] ;
	private String mvarsDirDoc ;
	private String mvarsTerminal ;
	private long mvarlUsuario ;
	
	public String getMvarsDirTrabajo() { return mvarsDirTrabajo; }
	
	public void setMvarsDirTrabajo(String mvarsDirTrabajo) { this.mvarsDirTrabajo = mvarsDirTrabajo; }
	
	public String getMvarsDirWin() { return mvarsDirWin; }
	
	public void setMvarsDirWin(String mvarsDirWin) { this.mvarsDirWin = mvarsDirWin; }
	
	public int[] getMvariEstructura() { return mvariEstructura; }
	
	public void setMvariEstructura(int[] mvariEstructura) { 
		for(int i = 0; i<this.mvariEstructura.length ; i++ ) {
			this.mvariEstructura[i] = mvariEstructura[i]; 
		}
	}
	
	public String getMvarsDirDoc() { return mvarsDirDoc; }
	
	public void setMvarsDirDoc(String mvarsDirDoc) { this.mvarsDirDoc = mvarsDirDoc; }
	
	public String getMvarsTerminal() { return mvarsTerminal; }
	
	public void setMvarsTerminal(String mvarsTerminal) { this.mvarsTerminal = mvarsTerminal; }
	
	public long getMvarlUsuario() { return mvarlUsuario; }
	
	public void setMvarlUsuario(long mvarlUsuario) { this.mvarlUsuario = mvarlUsuario; }

}
