package IfiduBaseDatos;

import java.text.SimpleDateFormat;
import java.util.Date;

import ADODB.Recordset;

public class BaseDatos implements IBaseDatos {

	/*VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 3  'UsesTransaction
	END
	Attribute VB_Name = "BaseDatos"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : fiduBaseDatos.vbp                          *'
	'*  CLASE       : BaseDatos.cls                              *'
	'*  DESCRIPCION : Implementacion para Ejecutar sentencias SQL*'
	'*                y objetos ADO (Consultas o Transacciones). *'
	'*                                                           *'
	'*  AUTOR       : OAP - Septiembre 2002                      *'
	'*  ACTUALIZACION MRHC  Octubre    2004                      *'
	'*************************************************************'
	Option Explicit*/
	
	private final String m_sObjectName  = "fiduBaseDatos" ;
	private final int iREMOTECONN  = 2 ;

	private final int cntPBaseOne = 1 ; //En el codigo de visual basic estos numeros estaban en hexadecimal &H1
	private final int cntPBaseNine = 9 ; //En el codigo de visual basic estos numeros estaban en hexadecimal &H9
	private final int cntPBaseNinety = 90 ; //En el codigo de visual basic estos numeros estaban en hexadecimal &H5A
	private final int cntPBaseSixtyFive  = 65 ; //En el codigo de visual basic estos numeros estaban en hexadecimal &H41

	private final String cntPStateINICIA = "Inicia" ;
	private final String cntPStateTERMINA = "Termina" ;

	private final String cntPNoBloqueodeTabla = " WITH UR " ;        //'* DSA-FIII/652-00001

	fiduConstantes.Constantes oConstantes ;

	//private sub en VB6 es igual que un void, es decir no retrona ningun valor.
	private void Class_Initialize(){
		oConstantes = new fiduConstantes.Constantes() ;
	}

	private void Class_Terminate(){
		oConstantes = null ;
	}
	
	@Override
	public boolean EjecutaTransaccion(String sSQL, String sMsgError) {
		// TODO Auto-generated method stub
		
		ADODB.Connection oConnection = new ADODB.Connection() ;
		IfiduAdmonErrores.clsErrores oManejoErr ;
	    ADODB.Errors oErrorDB ;
	    long lNumRegs ;
	    
	    /*float*/String sinLInitialTime  ; // En VB6 es de tipo single 
		String strLIDProcedureEXCLUSIVE ;
		
		try {
			//##############################################################
			Date ahora = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
		    //EN VB6 se utiliza la funcion Timer, para obtener la hora actual, en java he pensado utilizar esto.
		    //Solo que dicha hora se quedaria en String y la variable era en VB6 single.
			sinLInitialTime = formateador.format(ahora);
			//##############################################################
			strLIDProcedureEXCLUSIVE = funCreateIDPetition(sinLInitialTime) ;
			
			//Hasta que no escriba la siguiente funcion no sabre que debo pasarle.
			subPetitionsLog(strLIDProcedureEXCLUSIVE, cntPStateINICIA, "clsBaseDatos_EjecutaTransaccion", sSQL) ;
			
			sMsgError = "" ;

		    oConnection.CursorLocation = adUseClient ; //https://msdn.microsoft.com/en-us/library/ee252442(v=bts.10).aspx
		    oConnection.Mode = adModeReadWrite ; //https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/connectmodeenum?view=sql-server-2017
		    oConnection.ConnectionString = oConstantes.GlCadenaConec(iREMOTECONN) ;
		    oConnection.CommandTimeout = 3000 ;
		    oConnection.Open ;
		    //Esto de abajo supongo que ejecutara la consulta sSQL sobre oConnection
//		    oConnection.Execute sSQL, lNumRegs, adCmdText ;
		    
		    if(oConnection != null) {
	        	if (oConnection.State == adStateOpen) { oConnection.Close ; }
	        	oConnection = null ;
		    }
		    //Hasta que no escriba la siguiente funcion no sabre que debo pasarle.
		    subPetitionsLog(strLIDProcedureEXCLUSIVE, cntPStateTERMINA, , , Abs(Timer - sinLInitialTime)) ;
			
		}catch(Exception ex) {
			if (oConnection.Errors.Count > 0) { 
				oManejoErr = CreateObject("fiduAdmonErrores.Errores") ;
		        sMsgError = oManejoErr.ManejoBD(oConnection.Errors) ;
		        if (oManejoErr != null) { oManejoErr = null ; }
			}
			else{
				sMsgError = Err.Description + "(" + Err.Number + ")" ;
			}
		    if (oConnection != null) {
		        if (oConnection.State == adStateOpen) { oConnection.Close ; }
		        oConnection = null ;
		    }
		    sMsgError = sMsgError + " Fuente: " + m_sObjectName + ".EjecutaTransaccion" ;
		    
		    return false ; 
		}
	    
		return true ;
	}

	@Override
	public ADODB.Recordset EjecutaSeleccion(String sSQL, Long lNRows, String sMsgError) {
		// TODO Auto-generated method stub
		
		ADODB.Recordset rsGetResult = new ADODB.Recordset() ;
		ADODB.Errors oErrorDB ;
		ADODB.Connection oConnection = new ADODB.Connection ;
		IfiduAdmonErrores.clsErrores oManejoErr ;
		
		/*float*/String sinLInitialTime  ; // En VB6 es de tipo single 
		String strLIDProcedureEXCLUSIVE ;
		
		try {
			//##############################################################
			Date ahora = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
		    //EN VB6 se utiliza la funcion Timer, para obtener la hora actual, en java he pensado utilizar esto.
		    //Solo que dicha hora se quedaria en String y la variable era en VB6 single.
			sinLInitialTime = formateador.format(ahora);
			//##############################################################
			strLIDProcedureEXCLUSIVE = funCreateIDPetition(sinLInitialTime) ;

			//Hasta que no escriba la siguiente funcion no sabre que debo pasarle.
			subPetitionsLog(strLIDProcedureEXCLUSIVE, cntPStateINICIA, "clsBaseDatos_EjecutaSeleccion", sSQL) ;
			
			sMsgError = "" ;
			//############################################################## 
			//Conexion a la base de datos.
		    oConnection.ConnectionString = oConstantes.GlCadenaConec(iREMOTECONN) ;
		    oConnection.CommandTimeout = 3000 ;
		    oConnection.Open ;
		    //##############################################################
		    
		    //############################################################## 
		    //Definicion de los cursores 
		    rsGetResult.CursorLocation = adUseClient ;//https://msdn.microsoft.com/en-us/library/ee252442(v=bts.10).aspx
    		rsGetResult.CursorType = adOpenStatic ; //https://www.w3schools.com/asp/prop_rs_cursortype.asp
			rsGetResult.LockType = adLockBatchOptimistic ; // https://www.w3schools.com/asp/prop_rs_locktype.asp
			//############################################################## 
			
			sSQL = AnalizaQuery(sSQL) ;      //'* DSA-FIII/652-00002
			//Esto de abajo supongo que ejecutara la consulta sSQL sobre oConnection
//		    rsGetResult.Open sSQL, oConnection ;
			lNRows = rsGetResult.RecordCount ;
//			Set clsBaseDatos_EjecutaSeleccion = rsGetResult ; // En VB6 entiendo que esto seria como un return. 
			
			if (oConnection != null) {
		        if (oConnection.State == adStateOpen) {
		            rsGetResult.ActiveConnection = null ;
		            oConnection.Close ;
		        }
		        oConnection = null ;
			}
			
//			subPetitionsLog(strLIDProcedureEXCLUSIVE, cntPStateTERMINA, , , Abs(Timer - sinLInitialTime)) ;
			
		}catch (Exception ex) {
			if (oConnection.Errors.Count > 0) {
//  	        oManejoErr = CreateObject("fiduAdmonErrores.Errores") ;
		        sMsgError = oManejoErr.ManejoBD(oConnection.Errors) ;
		        if (oManejoErr != null) { oManejoErr = null ; } // En VB6 esto era un If Not
			}
		    else{
//		    	sMsgError = Err.Description + "(" + Err.Number + ")" ;
		    }
	    
		    if (rsGetResult != null) { // En VB6 esto era un If Not
		        if (rsGetResult.State == adStateOpen){ rsGetResult.Close ; }
		        rsGetResult = null ;
		    }
		    if (oConnection != null) { // En VB6 esto era un If Not
		        if (oConnection.State == adStateOpen){ oConnection.Close ; }
		        oConnection = null ;
		    }
		    sMsgError = sMsgError + " Fuente: " + m_sObjectName + ".EjecutaSeleccion" ;
		}
		
		return rsGetResult;
	}

	@Override
	public boolean EjecutaTransacADO(MSAdodcLib.IAdodc adoControl, String sAccion, String sMsgError) {
		// TODO Auto-generated method stub
		
		/*float*/String sinLInitialTime  ; // En VB6 es de tipo single 
		String strLIDProcedureEXCLUSIVE ;
		
		try {
			
			//##############################################################
			Date ahora = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
		    //EN VB6 se utiliza la funcion Timer, para obtener la hora actual, en java he pensado utilizar esto.
		    //Solo que dicha hora se quedaria en String y la variable era en VB6 single.
			sinLInitialTime = formateador.format(ahora);
			//##############################################################
			strLIDProcedureEXCLUSIVE = funCreateIDPetition(sinLInitialTime) ;
			//Hasta que no escriba la siguiente funcion no sabre que debo pasarle.
			subPetitionsLog(strLIDProcedureEXCLUSIVE, cntPStateINICIA, "clsBaseDatos_EjecutaTransacADO", "ByVal adoControl As MSAdodcLib.IAdodc") ;
			
			sMsgError = "" ;
			//Hasta que no escriba la clase oConstante no sabre .   
		    adoControl.ConnectionString = oConstantes.GlCadenaConec(iREMOTECONN) ;
		    
		    switch (sAccion.toUpperCase()) {
			    case "REFRESH" : {
			    	adoControl.Refresh ;
			    }
			    case "UPDATEBATCH" : {
			    	adoControl.Recordset.UpdateBatch ;
			    }
			    case "ADDNEW" : {
			    	adoControl.Recordset.AddNew ;
			    }
			    case "DELETE" : {
			    	adoControl.Recordset.Delete ;
			    }
		    }
		    
		    if (adoControl != null) { adoControl = null ; }
			
		}catch(Exception ex) {
			
			if (adoControl != null) { adoControl = null ; }
		    sMsgError = sMsgError + " Fuente: " + m_sObjectName + ".EjecutaTransacADO" ;
			
			return false ;
		}
		
		return true;
	}

	@Override
	public boolean EjecutaTransacLong(String sSQL, String sColLong, String sValLong, String sMsgError) {
		// TODO Auto-generated method stub
		
		ADODB.Recordset rsLong = new ADODB.Recordset ;
		ADODB.Connection oConnection = new ADODB.Connection ;
	    IfiduAdmonErrores.clsErrores oManejoErr ;
	    ADODB.Errors oErrorDB ;
	    
	    /*float*/String sinLInitialTime  ; // En VB6 es de tipo single 
		String strLIDProcedureEXCLUSIVE ;
		
		try {
			
			//##############################################################
			Date ahora = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
		    //EN VB6 se utiliza la funcion Timer, para obtener la hora actual, en java he pensado utilizar esto.
		    //Solo que dicha hora se quedaria en String y la variable era en VB6 single.
			sinLInitialTime = formateador.format(ahora);
			//##############################################################
			strLIDProcedureEXCLUSIVE = funCreateIDPetition(sinLInitialTime) ;
			//Hasta que no escriba la siguiente funcion no sabre que debo pasarle.
			subPetitionsLog(strLIDProcedureEXCLUSIVE, cntPStateINICIA, "clsBaseDatos_EjecutaTransacLong", sSQL) ;
			
			sMsgError = "" ;
				    
		    oConnection.ConnectionString = oConstantes.GlCadenaConec(iREMOTECONN) ;
		    
		    rsLong.CursorLocation = adUseClient ; //https://msdn.microsoft.com/en-us/library/ee252442(v=bts.10).aspx
		    rsLong.CursorType = adOpenStatic ; //https://www.w3schools.com/asp/prop_rs_cursortype.asp
		    rsLong.LockType = adLockBatchOptimistic ; // https://www.w3schools.com/asp/prop_rs_locktype.asp
		    //############################################
//		    rsLong.Open sSQL, oConnection.ConnectionString ;
//		    rsLong.Fields(sColLong).AppendChunk (sValLong) ;
//		    rsLong.UpdateBatch ;
		    //############################################
		    
		    if (rsLong != null ) {
		        if (rsLong.State == adStateOpen) { rsLong.Close ; } // https://www.w3schools.com/asp/prop_conn_state.asp
		        rsLong = null ;
		    }
		    if (oConnection != null ) {
		        if (oConnection.State == adStateOpen) { oConnection.Close ; }
		        oConnection = null ;
		    }
		    //Hasta que no escriba la siguiente funcion no sabre que debo pasarle.
		    subPetitionsLog(strLIDProcedureEXCLUSIVE, cntPStateTERMINA, , , Abs(Timer - sinLInitialTime)) ;
			
		}catch (Exception ex) {
			
			if (oConnection.Errors.Count > 0) {
	        	oManejoErr = CreateObject("fiduAdmonErrores.Errores") ;
	        	sMsgError = oManejoErr.ManejoBD(oConnection.Errors) ;
	        	if (oManejoErr != null) { oManejoErr = null ; }
			}
	        else{
	        	sMsgError = Err.Description + "(" + Err.Number + ")" ;
	        }
		    if(rsLong != null ){
		        if (rsLong.State == adStateOpen) { rsLong.Close ; }
		        rsLong = null ;
		    }
		    if(oConnection!= null ) {
		        if (oConnection.State == adStateOpen){ oConnection.Close ; }
		        oConnection = null ;
		    }
		    sMsgError = sMsgError + " Fuente: " + m_sObjectName + ".EjecutaTransacLong" ;
			return false ;
		}
		
		return true;
	}
	
	//La siguiente funcion lo que hara sera generar numeros aleatorios para crear un id de peticion 
	public String funCreateIDPetition(String sinLTimer) {
		
		String idPetition ;
		idPetition = String.valueOf(((int)( cntPBaseNine * Math.random())+ cntPBaseOne)) ;
		idPetition = idPetition + ((char)((int) (cntPBaseNinety - cntPBaseSixtyFive + cntPBaseOne) * Math.random() + cntPBaseSixtyFive ));
		idPetition = idPetition + sinLTimer ;
		
		return idPetition ;
	}
	/*
	public void subPetitionsLog(String strLIDPetitio, String strLState, Optional ByVal strLProcedure As String = Empty, Optional ByVal strLSQL As String = Empty, Optional ByVal sinLDurationInSeconds As Single) {
		
		Dim intLIDFile As Integer

		Let intLIDFile = FreeFile
		Open App.Path & "\EventLogFBD" & Format(Date, "DDMMYYYY") & ".Log" For Append As #intLIDFile

		Select Case strLState
		    Case cntPStateINICIA
		        Print #intLIDFile, vbCrLf & Now & vbTab & strLIDPetition & vbTab & strLProcedure & vbTab & strLState
		        Print #intLIDFile, strLSQL
		    Case cntPStateTERMINA
		        Print #intLIDFile, vbCrLf & Now & vbTab & strLIDPetition & vbTab & vbTab & strLState & vbTab & sinLDurationInSeconds
		End Select

		Close #intLIDFile
	
	}
	*/
	public String AnalizaQuery(String strPSQL) {
		
		String strSQL_AUX ;                                              //'* DSA-FIII/652-00004
		//'strPSQL = UCase(strPSQL)                                              '* DSA-FIII/652-00005 '* DSA-FIII/669-00001
		if (strPSQL.length() == 0) {                                               //'* DSA-FIII/652-00006
		    return strPSQL ;                                            //'* DSA-FIII/652-00007
		}                                                                 //'* DSA-FIII/652-00009
		if ("WITH UR".indexOf(strPSQL.toUpperCase()) > 0) {                     //'* DSA-FIII/652-00010    '* DSA-FIII/669-00002
		    return strPSQL ;                                             //'* DSA-FIII/652-00011
		}
		
		if ("SELECT".indexOf(strPSQL.toUpperCase()) > 0) {                              //'* DSA-FIII/652-00014
		    if (";".indexOf(strPSQL.toUpperCase()) > 0) {                               //'* DSA-FIII/652-00015
		        strSQL_AUX = strPSQL.substring(1, ";".indexOf(strPSQL.toUpperCase()) - 1) ;     //'* DSA-FIII/652-00016
		        strSQL_AUX = strSQL_AUX + cntPNoBloqueodeTabla + " ; " + strPSQL.substring(";".indexOf(strPSQL.toUpperCase()) + 1,strPSQL.length());  //'* DSA-FIII/652-00017
		        return strSQL_AUX ;                                    //'* DSA-FIII/652-00018
		    }
		    if ("".indexOf(strPSQL.toUpperCase()) > 0) {                                //'* DSA-FIII/652-00021
		        strSQL_AUX = strPSQL.substring(1, strPSQL.length()) ;                     //'* DSA-FIII/652-00022
		        strSQL_AUX = strSQL_AUX + cntPNoBloqueodeTabla ;                  //'* DSA-FIII/652-00023
		        return strSQL_AUX ;                                      //'* DSA-FIII/652-00024
		    }                                                              //'* DSA-FIII/652-00026
		}
	}
}
