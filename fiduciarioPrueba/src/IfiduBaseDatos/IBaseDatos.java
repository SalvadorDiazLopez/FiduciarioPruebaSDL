package IfiduBaseDatos;

public interface IBaseDatos {

	/*VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 3  'UsesTransaction
	END
	Attribute VB_Name = "clsBaseDatos"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : IfiduBaseDatos.vbp                         *'
	'*  CLASE       : clsBaseDatos.cls                           *'
	'*  DESCRIPCION : Interfase del COM fiduBaseDatos            *'
	'*  AUTOR       : OAP - Septiembre 2002                      *'
	'*************************************************************'
	Option Explicit*/

	public boolean EjecutaTransaccion(String sSQL, String sMsgError) ;

	public ADODB.Recordset EjecutaSeleccion(String sSQL, Long lNRows, String sMsgError) ;

	public boolean EjecutaTransacADO(Adodc adoControl, String sAccion, String sMsgError) ;

	public boolean EjecutaTransacLong(String sSQL, String sColLong, String sValLong, String sMsgError) ;
	
}
