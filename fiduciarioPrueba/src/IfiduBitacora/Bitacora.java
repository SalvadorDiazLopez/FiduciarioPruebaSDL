package IfiduBitacora;

import IfiduBaseDatos.BaseDatos;

public class Bitacora implements IBitacora {
/*
	VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "Bitacora"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : fiduBitacora.vbp                           *'
	'*  CLASES      : Bitacora.cls                               *'
	'*  DESCRIPCION : Implementacion para Administrar tabla      *'
	'*                Bitacora                                   *'
	'*                                                           *'
	'*  AUTOR       : MRHC - Octubre   2002                      *'
	'*************************************************************'

	Option Explicit
	*/
	@Override
	public boolean GeneraBitacora(String sFuncion, String sForma, String sRedaccion, String sTerminal, Long lnUsuario) {
		// TODO Auto-generated method stub
		
		//boolean bEjecuto ;
		String sSql ;
		BaseDatos oBaseDat ;
		
		if (sFuncion == "") { sFuncion = "." ; }
		if (sForma   == "") { sForma   = "." ; }
		
		sSql = " INSERT INTO SAF.BITACORA (" ;
		  sSql = sSql + " BIT_FEC_TRANSAC," ;
		  sSql = sSql + " BIT_ID_TERMINAL," ;
		  sSql = sSql + " BIT_NUM_USUARIO," ;
		  sSql = sSql + " BIT_NOM_PGM," ;
		  sSql = sSql + " BIT_CVE_FUNCION," ;
		  sSql = sSql + " BIT_DET_BITACORA" ;
		  sSql = sSql + " ) VALUES (" ;
		  sSql = sSql + "   CURRENT TIMESTAMP," ;
		  sSql = sSql + "   '" + sTerminal.toUpperCase() + "'," ;
		  sSql = sSql + lnUsuario + "," ;
		  sSql = sSql + "   '" + sForma.toUpperCase().substring(0, 50) + "'," ;
		  sSql = sSql + "   '" + sFuncion.toUpperCase().substring(0, 25) + "'," ;
		  sSql = sSql + "   '" + sRedaccion.toUpperCase().substring(0, 200) + "'" ;
		  sSql = sSql + " )" ;
		  
		  oBaseDat = new BaseDatos() ;
		  boolean genBit = oBaseDat.EjecutaTransaccion(sSql, "") ;
		  oBaseDat = null ;
		  
		  return genBit ;
	}

}
