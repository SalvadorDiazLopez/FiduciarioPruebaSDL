package IfiduBitacora;

public interface IBitacora {

	/*VERSION 1.0 CLASS
	BEGIN
	  MultiUse = -1  'True
	  Persistable = 0  'NotPersistable
	  DataBindingBehavior = 0  'vbNone
	  DataSourceBehavior  = 0  'vbNone
	  MTSTransactionMode  = 0  'NotAnMTSObject
	END
	Attribute VB_Name = "clsBitacora"
	Attribute VB_GlobalNameSpace = False
	Attribute VB_Creatable = True
	Attribute VB_PredeclaredId = False
	Attribute VB_Exposed = True
	'*************************************************************'
	'*           Derechos Reservados EFISOFT S.A. 2002           *'
	'*************************************************************'
	'*  ARCHIVO     : IfiduBitacora.vbp                          *'
	'*  CLASE       : clsBitacora.cls                            *'
	'*  DESCRIPCION : Interfase del COM fiduBitacora             *'
	'*  AUTOR       : MRHC - Octubre 2002                        *'
	'*************************************************************'

	Option Explicit

	'********************** Metodos para Bitacora ********************************'*/
	public boolean GeneraBitacora(String sFuncion, String sForma, String sRedaccion, String sTerminal, Long lnUsuario) ;
	
}
